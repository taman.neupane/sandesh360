$(document).ready(function(){

    var name = $('#preferenceName');
    var lan = $('#language');
    var icon = $('#icon');
    var isA = $('#cbActive');
    var isD = $('#cbDelete');


    function createPreference(){
        var vname = name.val();
        var vlan = lan.val();
        var vactive = getStatusByCheckBoxId(isA);
        var vdelete = getStatusByCheckBoxId(isD);

        var pref = {
            preference_name:vname,
            language_id:vlan,
            is_active:vactive,
            is_delete:vdelete
        }
        return{
            preference: pref
        }

    }

    function getStatusByCheckBoxId(checkbox){
        if (checkbox.is(":checked")){
            return true;
        }else{
            return false;
        }
    }



    $("#btnSave").click(function(){
        var body = createPreference();
        var formData = new FormData();
        var files = icon[0].files;
        $.each(files, function(i, file){
            formData.append('image', file);
        });

        formData.append('body', JSON.stringify(body));

        $.ajax({
            url: "/api/preference/add",
            type:'POST',
            enctype: 'multipart/form-data',
            cache: false,
            contentType: false,
            processData: false,
            data: formData,
            dataType:"json",
            success: function(result) {
               alert(result.status)
            },
            error: function(xhr, ajaxOptions, thrownError){
                alert(xhr.status+"\n"+xhr.responseJSON.message);
            }
        });
    });


});

