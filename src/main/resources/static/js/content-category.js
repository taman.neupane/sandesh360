$(document).ready(function(){
    var data = null;
    $.get( "/api/categories/active", function(result, httpStatus){
        if(httpStatus == "success"){
            data = result;
            if(data.status=="OK"){
               console.log("status: "+data.status);
               getActiveCategoriesByLanguageId(data.categories);
            }else{
                console.log(result);
            }
        }

    });

    function getActiveCategoriesByLanguageId(categories){
         $.each(categories, function(i, category) {
                var opt = $('<option />').val(category.id).text(category.category_name);
                $('#selectCategories').append(opt);
         });

         $('#selectCategories').multiselect('rebuild');
    }

});