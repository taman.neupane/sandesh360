$(document).ready(function(){
    var data = null;
    $.get( "/api/preferences/active", function(result, httpStatus){
        if(httpStatus == "success"){
            data = result;
            if(data.status=="OK"){
               console.log("status: "+data.status);
               getActivePreferencesByLanguageId(data.preferences);
            }else{
                console.log(result);
            }
        }

    });

    function getActivePreferencesByLanguageId(preferences){
            $.each(preferences, function(i, preference) {
                var opt = $('<option />').val(preference.id).text(preference.preference_name);
                $('#selectPreferences').append(opt);
             });

            $('#selectPreferences').multiselect('rebuild');

    }

});