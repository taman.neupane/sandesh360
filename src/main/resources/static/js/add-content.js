$(document).ready(function(){

    var titleText = $('#title');
    var shortDes = $('#shortDes');
    var des = $('#des');
    var conUrl = $('#conUrl');
    var siteUrl = $('#siteUrl');
    var conPref = $('#selectPreferences');
    var conCate = $('#selectCategories');
    var lan = $('#language');
    var icon = $('#icon');
    var isA = $('#cbActive');
    var isD = $('#cbDelete');


    function createContent(){
        var vTitle = titleText.val();
        var vShortDes = shortDes.val();
        var vDes = des.val();
        var vConUrl = conUrl.val();
        var vSiteUrl = siteUrl.val();
        var vConPref = conPref.val();
        var vConCate = conCate.val();
        var vLan = lan.val();
        var vActive = getStatusByCheckBoxId(isA);
        var vDelete = getStatusByCheckBoxId(isD);

        if(vConPref.length<=0){
            console.log("Please select at least one preference");
        }
        var preferences = [];
        for(var i in vConPref){
            preferences.push({
                id:vConPref[i]
            });
        }

        if(vConCate.length<=0){
            console.log("Please select at least one preference");
        }
        var categories = [];
        for(var i in vConCate){
            categories.push({
                id:vConCate[i]
            });
        }

        var contentObj = {
            title:vTitle,
            short_description: vShortDes,
            description: vDes,
            content_url: vConUrl,
            site_url: vSiteUrl,
            preferences: preferences,
            categories: categories,
            language_id:vLan,
            is_active:vActive,
            is_delete:vDelete
        }
        return{
            content: contentObj
        }

    }

    function getStatusByCheckBoxId(checkbox){
        if (checkbox.is(":checked")){
            return true;
        }else{
            return false;
        }
    }



    $("#btnSave").click(function(){
        var formData = new FormData();
        var files = icon[0].files;
        if(files.length<=0){
            console.log("Upload at least one image");
        }

        $.each(files, function(i, file){
            formData.append('files', file);
        });

        var body = createContent();
        formData.append('body', JSON.stringify(body));

        $.ajax({
            url: "/api/content/add",
            type:'POST',
            enctype: 'multipart/form-data',
            cache: false,
            contentType: false,
            processData: false,
            data: formData,
            success: function(result) {
               alert(result.status)
            },
            error: function(xhr, ajaxOptions, thrownError){
                alert(xhr.status+"\n"+xhr.responseJSON.message);
            }
        });
    });


});

