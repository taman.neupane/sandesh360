$(document).ready(function(){

    var name = $('#categoryName');
    var des = $('#description');
    var lan = $('#language');
    var icon = $('#icon');
    var isA = $('#cbActive');
    var isD = $('#cbDelete');


    function createCategory(){
        var vname = name.val();
        var vdes = des.val();
        var vlan = lan.val();
        var vactive = getStatusByCheckBoxId(isA);
        var vdelete = getStatusByCheckBoxId(isD);

        return{
            category_name:vname,
            description:vdes,
            language_id:vlan,
            is_active:vactive,
            is_delete:vdelete
        }

    }

    function getStatusByCheckBoxId(checkbox){
        if (checkbox.is(":checked")){
            return true;
        }else{
            return false;
        }
    }

    $("#btnSave").click(function(){
        var body = createCategory();
        var c = {
            category: body
        }

        var formData = new FormData();
        var files = icon[0].files;
        $.each(files, function(i, file){
            formData.append('image', file);
        });
        formData.append('body', JSON.stringify(c));

        $.ajax({
            url: "/api/category/add",
            type:'POST',
            enctype: 'multipart/form-data',
            cache: false,
            contentType: false,
            processData: false,
            data: formData,
            dataType:"json",
            success: function( result ) {
               console.log("Successfully upload: "+result.status);

            },
            error: function(xhr, ajaxOptions, thrownError){
                console.log("upload failed: "+xhr.status+"\n"+xhr.responseJSON.message);
            }
        });
    });

});

