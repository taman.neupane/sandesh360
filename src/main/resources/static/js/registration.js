$(document).ready(function(){
    $("#btnRegister").click(function(){
        var uname = $("#userName").val();
        var fname = $("#fullName").val();
        var eId = $("#emailId").val();
        var phoneNo = $("#phoneNo").val();
        var addr = $("#address").val();
        var pass = $("#userPassword").val();
        var confirmPass = $("#confirmPass").val();

        if(uname==''){
            alert("User name is required");
        }else if(fname==''){
            alert('Full name is required')
        }else if(eId==''){
            alert('Email is required')
        }else if(phoneNo==''){
            alert('Phone number is required')
        }else if(addr==''){
            alert('Address is required')
        }else if(pass==''){
            alert('Password is required')
        }else if(confirmPass !== pass){
            alert('Password & confirm password does not match')
        }else{
//            $.post("/api/admin/add/", //Required URL of the page on server
//            { // Data Sending With Request To Server
//                  user_name:uname,
//                  password:pass,
//                  name:fname,
//                  email:eId,
//                  phone:phoneNo,
//                  address:addr
//            },
//            function(response,status){ // Required Callback Function
//                if(status=='success'){
//                    window.location.href ="/login";
//                }else{
//                    window.location.href ="/registration";
//                }
//
//            });
            var admin = {
                  user_name:uname,
                  password:pass,
                  name:fname,
                  email:eId,
                  phone:phoneNo,
                  address:addr
            }

            var body = {
                admin:admin
            }
            $.ajax({
                url: "/api/admin/add",
                type:'POST',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                data: JSON.stringify(body),
                success: function( result ) {
                   window.location.href ="/login";
                },
                error: function(xhr, ajaxOptions, thrownError){
                    alert(xhr.status+"\n"+xhr.responseJSON.message);
                }
             }
            )
        }
    });
});