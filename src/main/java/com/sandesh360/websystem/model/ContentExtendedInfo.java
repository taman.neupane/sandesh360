package com.sandesh360.websystem.model;

import com.sandesh360.websystem.entity.Category;
import com.sandesh360.websystem.entity.Content;
import com.sandesh360.websystem.entity.Image;
import com.sandesh360.websystem.entity.Preference;

import java.math.BigInteger;
import java.util.List;


public class ContentExtendedInfo extends Content {

    private Integer preferenceId;

    private Integer categoryId;

    private BigInteger userId;

    private List<Preference> preferences;

    private List<Category> categories;

    private List<Image> images;


    public Integer getPreferenceId() {
        return preferenceId;
    }

    public void setPreferenceId(Integer preferenceId) {
        this.preferenceId = preferenceId;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public BigInteger getUserId() {
        return userId;
    }

    public void setUserId(BigInteger userId) {
        this.userId = userId;
    }

    public List<Preference> getPreferences() {
        return preferences;
    }

    public ContentExtendedInfo setPreferences(List<Preference> preferences) {
        this.preferences = preferences;
        return this;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public ContentExtendedInfo setCategories(List<Category> categories) {
        this.categories = categories;
        return this;
    }

    public List<Image> getImages() {
        return images;
    }

    public ContentExtendedInfo setImages(List<Image> images) {
        this.images = images;
        return this;
    }
}