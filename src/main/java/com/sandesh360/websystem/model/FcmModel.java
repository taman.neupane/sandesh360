package com.sandesh360.websystem.model;

public class FcmModel {
	
	/**
	 * FCM Token
	 */
	private String fcmToken;
	
	/**
	 * Message Priority
	 */
	private String priority;
	
	/**
	 * Message Body Content
	 */
	private String messageBody;
	
	/**
	 * Message Title
	 */
	private String messageTitle;
	
	/**
	 * Notification on tap action
	 */
	private String clickAction;

	public String getFcmToken() {
		return fcmToken;
	}

	public void setFcmToken(String fcmToken) {
		this.fcmToken = fcmToken;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getMessageBody() {
		return messageBody;
	}

	public void setMessageBody(String messageBody) {
		this.messageBody = messageBody;
	}

	public String getMessageTitle() {
		return messageTitle;
	}

	public void setMessageTitle(String messageTitle) {
		this.messageTitle = messageTitle;
	}

	public String getClickAction() {
		return clickAction;
	}

	public void setClickAction(String clickAction) {
		this.clickAction = clickAction;
	}

}
