package com.sandesh360.websystem.model;

import java.math.BigInteger;

public class PollPercentage {

    private BigInteger questionId;

    private double yes;

    private double no;

    public BigInteger getQuestionId() {
        return questionId;
    }

    public void setQuestionId(BigInteger questionId) {
        this.questionId = questionId;
    }

    public double getYes() {
        return yes;
    }

    public void setYes(double yes) {
        this.yes = yes;
    }

    public double getNo() {
        return 100 - yes;
    }

    public void setNo(double no) {
        this.no = no;
    }
}
