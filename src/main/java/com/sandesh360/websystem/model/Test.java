package com.sandesh360.websystem.model;

public class Test {

    private int id;

    private String preferenceName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    private String locale;

    public String getLocale() {
        return locale;
    }

    public String getPreferenceName() {
        return preferenceName;
    }

    public void setPreferenceName(String preferenceName) {
        this.preferenceName = preferenceName;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }
}
