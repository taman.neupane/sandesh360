package com.sandesh360.websystem.security;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    protected final Log log = LogFactory.getLog(WebSecurityConfig.class);


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        http
                .authorizeRequests()
                .antMatchers("/admin*").authenticated()
                .anyRequest().permitAll()
                .and()
                .formLogin()
                .loginPage("/login")
                .permitAll();

//        http.csrf().disable();
//        http
//                .authorizeRequests()
//                .antMatchers("/", "/api").permitAll()
//                .antMatchers("/", "/index2").permitAll()
//                .anyRequest().authenticated()
//                .and()
//                .formLogin()
//                .loginPage("/login")
//                .permitAll()
//                .and()
//                .logout()
//                .permitAll();

//        log.info("Enabling Spring Security");
//
//        http.headers().frameOptions().sameOrigin();
//
//        http
//                .headers()
//                .xssProtection()
//                .block(true);
//        super.configure(http);
    }


}
