package com.sandesh360.websystem.entity;


import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigInteger;


/**
 * Admin Entity Class
 *
 * @author AtomAP Ltd.
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "User")
public class User extends BaseEntity<User> {

    /**
     * Admin ID
     */
    @Id
    @Column(name = "id")
    private BigInteger id;
    /**
     * Admin Password
     */
    @Column(name = "password")
    private String password;

    /**
     * Admin Username
     */
    @Column(name = "user_name")
    @JsonProperty("user_name")
    private String userName;

    /**
     * Admin Name
     */
    @Column(name = "name")
    private String name;

    /**
     * Admin phone
     */
    @Column(name = "phone")
    private String phone;

    /**
     * Admin Email
     */
    @Column(name = "email")
    private String email;

    /**
     * Admin address
     */
    @Column(name = "address")
    private String address;

    @Column(name = "profile_pic")
    private String profilePic;

    public BigInteger getId() {
        return id;
    }

    public User setId(BigInteger id) {
        this.id = id;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public User setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getUserName() {
        return userName;
    }

    public User setUserName(String userName) {
        this.userName = userName;
        return this;
    }

    public String getName() {
        return name;
    }

    public User setName(String name) {
        this.name = name;
        return this;
    }

    public String getPhone() {
        return phone;
    }

    public User setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public User setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getAddress() {
        return address;
    }

    public User setAddress(String address) {
        this.address = address;
        return this;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public User setProfilePic(String profilePic) {
        this.profilePic = profilePic;
        return this;
    }
}