package com.sandesh360.websystem.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigInteger;

@Entity
@Table(name = "ContentCategory")
public class ContentCategory extends BaseEntity<ContentCategory> {

    @Id
    @Column(name = "id")
    private BigInteger id;

    @Column(name = "category_id")
    private Integer categoryId;

    @Column(name = "content_id")
    private BigInteger contentId;


    public BigInteger getId() {
        return id;
    }

    public ContentCategory setId(BigInteger id) {
        this.id = id;
        return this;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public ContentCategory setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
        return this;
    }

    public BigInteger getContentId() {
        return contentId;
    }

    public ContentCategory setContentId(BigInteger contentId) {
        this.contentId = contentId;
        return this;
    }
}
