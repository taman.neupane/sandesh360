package com.sandesh360.websystem.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author mobarak
 *
 */
@Entity
@Table(name = "Language")
public class Language extends BaseEntity{

	@Id
	@Column(name = "id")
	private Integer id;
	
	@Column(name ="name")
	private String name;

	@Column(name ="locale")
	private String locale;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}
	
	
}
