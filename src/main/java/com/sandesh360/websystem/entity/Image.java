package com.sandesh360.websystem.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigInteger;
import java.sql.Timestamp;

@Entity
@Table(name = "Image")
public class Image {

    @Id
    @Column(name = "id")
    private BigInteger id;

    @Column(name = "image_url")
    private String imageUrl;

    @Column(name = "video_url")
    private String videoUrl;

    @Column(name = "content_id")
    private BigInteger contentId;

    @Column(name = "uploaded_at")
    private Timestamp uploadedAt;

    public BigInteger getId() {
        return id;
    }

    public Image setId(BigInteger id) {
        this.id = id;
        return this;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public Image setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
        return this;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public Image setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
        return this;
    }

    public BigInteger getContentId() {
        return contentId;
    }

    public Image setContentId(BigInteger contentId) {
        this.contentId = contentId;
        return this;
    }

    public Timestamp getUploadedAt() {
        return uploadedAt;
    }

    public Image setUploadedAt(Timestamp uploadedAt) {
        this.uploadedAt = uploadedAt;
        return this;
    }
}