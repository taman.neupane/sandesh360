package com.sandesh360.websystem.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author mobarak
 */
@Entity
@Table(name = "Preference")
public class Preference extends BaseEntity {

    @Id
    @Column(name = "id")
    private Integer id;

    @Column(name = "preference_name")
    @JsonProperty("preference_name")
    private String preferenceName;

    @Column(name = "icon")
    private String iconUrl;

    @Column(name = "language_id")
    @JsonProperty("language_id")
    private Integer languageId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPreferenceName() {
        return preferenceName;
    }

    public void setPreferenceName(String preferenceName) {
        this.preferenceName = preferenceName;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }


    public Integer getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Integer languageId) {
        this.languageId = languageId;
    }

}
