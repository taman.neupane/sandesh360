package com.sandesh360.websystem.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Timestamp;

@Entity
@Table(name = "Poll")
public class Poll extends BaseEntity {

    @Id
    @Column(name = "id")
    private BigInteger id;

    @Column(name = "qid")
    private BigInteger questionId;

    @Column(name = "voter")
    private String voter;

    @Column(name = "is_yes")
    @JsonProperty("is_yes")
    private Boolean isYes;

    @Column(name = "voted_at")
    private Timestamp votedAt;

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public BigInteger getQuestionId() {
        return questionId;
    }

    public void setQuestionId(BigInteger questionId) {
        this.questionId = questionId;
    }

    public String getVoter() {
        return voter;
    }

    public void setVoter(String voter) {
        this.voter = voter;
    }

    public Boolean isYes() {
        return isYes;
    }

    public void setYes(Boolean yes) {
        isYes = yes;
    }

    public Timestamp getVotedAt() {
        return votedAt;
    }

    public void setVotedAt(Timestamp votedAt) {
        this.votedAt = votedAt;
    }
}