package com.sandesh360.websystem.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.sandesh360.websystem.common.Constants;

import javax.persistence.Column;
import java.io.Serializable;
import java.sql.Timestamp;

public class BaseEntity<T> implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 5416416652281751780L;

    /**
     * Active Flag
     */
    @Column(name = "is_active")
    @JsonProperty("is_active")
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private Boolean isActive = Constants.ACTIVE_FLAG_YES;

    /**
     * Delete Flag
     */
    @Column(name = "is_delete")
    @JsonProperty("is_delete")
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private Boolean isDelete = Constants.DELETE_FLAG_NO;

    /**
     * The person who Created
     */
    @Column(name = "created_by")
    @JsonProperty("created_by")
    private String createdBy;

    /**
     * Creation Date Time
     */
    @Column(name = "created_at")
    private Timestamp createdAt;

    /**
     * Last Updated person
     */
    @Column(name = "updated_by")
    @JsonProperty("updated_by")
    private String updatedBy;

    /**
     * Update Date Time
     */
    @Column(name = "updated_at")
    private Timestamp updatedAt;

    /**
     * Device ID
     */
    @Column(name = "device_id")
    private String deviceID;

    /**
     * Auth token not related to database
     */
    @Column(name = "auth_token")
    private String authToken;


    public static long getSerialVersionUID() {
        return serialVersionUID;
    }


    public Boolean isActive() {
        return isActive;
    }

    public T setActive(Boolean isActive) {
        this.isActive = isActive;
        return (T) this;
    }

    public Boolean isDelete() {
        return isDelete;
    }

    public T setDelete(Boolean isDelete) {
        this.isDelete = isDelete;
        return (T) this;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public T setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
        return (T) this;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public T setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
        return (T) this;
    }

    public String getUpdatedBy() {
        return updatedBy;

    }

    public T setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
        return (T) this;
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;

    }

    public T setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
        return (T) this;
    }

    public String getDeviceID() {
        return deviceID;
    }

    public T setDeviceID(String deviceID) {
        this.deviceID = deviceID;
        return (T) this;
    }

    public String getAuthToken() {
        return authToken;
    }

    public T setAuthToken(String authToken) {
        this.authToken = authToken;
        return (T) this;
    }
}
