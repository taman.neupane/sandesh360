package com.sandesh360.websystem.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigInteger;

@Entity
@Table(name = "Feedback")
public class Feedback extends BaseEntity<Feedback> {

    @Id
    @Column(name = "id")
    private BigInteger id;

    @Column(name = "from")
    private String from;

    @Column(name = "comment")
    private String comment;


    public BigInteger getId() {
        return id;
    }

    public Feedback setId(BigInteger id) {
        this.id = id;
        return this;
    }

    public String getFrom() {
        return from;
    }

    public Feedback setFrom(String from) {
        this.from = from;
        return this;
    }

    public String getComment() {
        return comment;
    }

    public Feedback setComment(String comment) {
        this.comment = comment;
        return this;
    }


}