package com.sandesh360.websystem.entity;


import com.sandesh360.websystem.webapp.controller.AdminForm;
import com.sandesh360.websystem.webapp.controller.LoginForm;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * Admin Entity Class
 *
 * @author AtomAP Ltd.
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "Admin")
public class Admin extends BaseEntity<Admin> {

    /**
     * Admin ID
     */
    @Id
    @Column(name = "id")
    private Integer id;
    /**
     * Admin Password
     */
    @Column(name = "password")
    private String password;

    /**
     * Admin Username
     */
    @Column(name = "user_name")
    private String userName;

    /**
     * Admin Name
     */
    @Column(name = "name")
    private String name;

    /**
     * Admin phone
     */
    @Column(name = "phone")
    private String phone;

    /**
     * Admin Email
     */
    @Column(name = "email")
    private String email;

    /**
     * Admin address
     */
    @Column(name = "address")
    private String address;

    private String newPassword;

    private String confirmNewPassword;

    public Integer getId() {
        return id;
    }

    public Admin setId(Integer id) {
        this.id = id;
        return this;

    }

    public String getPassword() {
        return password;
    }

    public Admin setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getUserName() {
        return userName;
    }

    public Admin setUserName(String userName) {
        this.userName = userName;
        return this;
    }

    public String getName() {
        return name;
    }

    public Admin setName(String name) {
        this.name = name;
        return this;
    }

    public String getPhone() {
        return phone;
    }

    public Admin setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public Admin setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getAddress() {
        return address;

    }

    public Admin setAddress(String address) {
        this.address = address;
        return this;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public Admin setNewPassword(String newPassword) {
        this.newPassword = newPassword;
        return this;
    }

    public String getConfirmNewPassword() {
        return confirmNewPassword;
    }

    public Admin setConfirmNewPassword(String confirmNewPassword) {
        this.confirmNewPassword = confirmNewPassword;
        return this;
    }

    public boolean isConfirm() {
        return newPassword != null
                && confirmNewPassword != null
                && newPassword.equals(confirmNewPassword);
    }

//    public LoginForm getLoginForm(){
//        LoginForm loginForm = new LoginForm();
//        loginForm.setAdminId(this.getId());
//        loginForm.setUserName(this.getUserName());
//        loginForm.setAdminName(this.getName());
//        loginForm.setAdminEmail(this.getEmail());
//        loginForm.setAdminPhone(this.getPhone());
//        loginForm.setAdminAddress(this.getAddress());
//
//        return loginForm;
//    }
//
//    public AdminForm getAdminForm(){
//        AdminForm adminForm = new AdminForm();
//        adminForm.setId(this.getId());
//        adminForm.setName(this.getName());
//        adminForm.setUserName(this.getUserName());
//        adminForm.setPhone(this.getPhone());
//        adminForm.setEmail(this.getEmail());
//        adminForm.setAddress(this.getAddress());
//        return adminForm;
//    }
}