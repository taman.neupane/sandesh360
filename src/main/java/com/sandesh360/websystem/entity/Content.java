package com.sandesh360.websystem.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigInteger;
import java.util.List;

@Entity
@Table(name = "Content")
public class Content extends BaseEntity<Content> {

    @Id
    @Column(name = "id")
    private BigInteger id;

    @Column(name = "title")
    private String title;

    @Column(name = "short_description")
    @JsonProperty("short_description")
    private String shortDescription;

    @Column(name = "description")
    private String description;

    @Column(name = "content_url")
    @JsonProperty("content_url")
    private String contentUrl;

    @Column(name = "site_url")
    @JsonProperty("site_url")
    private String siteUrl;

    @Column(name = "language_id")
    @JsonProperty("language_id")
    private Integer languageId;

    public BigInteger getId() {
        return id;
    }

    public Content setId(BigInteger id) {
        this.id = id;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public Content setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public Content setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Content setDescription(String description) {
        this.description = description;
        return this;
    }

    public String getContentUrl() {
        return contentUrl;
    }

    public Content setContentUrl(String contentUrl) {
        this.contentUrl = contentUrl;
        return this;
    }

    public String getSiteUrl() {
        return siteUrl;
    }

    public Content setSiteUrl(String siteUrl) {
        this.siteUrl = siteUrl;
        return this;
    }

    public Integer getLanguageId() {
        return languageId;
    }

    public Content setLanguageId(Integer languageId) {
        this.languageId = languageId;
        return this;
    }
}