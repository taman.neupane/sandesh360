package com.sandesh360.websystem.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigInteger;

@Entity
@Table(name = "ContentCategory")
public class ContentPreference extends BaseEntity<ContentPreference> {

    @Id
    @Column(name = "id")
    private BigInteger id;

    @Column(name = "preference_id")
    private Integer preferenceId;

    @Column(name = "content_id")
    private BigInteger contentId;


    public BigInteger getId() {
        return id;
    }

    public ContentPreference setId(BigInteger id) {
        this.id = id;
        return this;
    }

    public Integer getPreferenceId() {
        return preferenceId;
    }

    public ContentPreference setPreferenceId(Integer preferenceId) {
        this.preferenceId = preferenceId;
        return this;
    }

    public BigInteger getContentId() {
        return contentId;
    }

    public ContentPreference setContentId(BigInteger contentId) {
        this.contentId = contentId;
        return this;
    }
}
