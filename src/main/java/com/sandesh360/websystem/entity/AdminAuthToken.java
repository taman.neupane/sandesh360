package com.sandesh360.websystem.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "AdminAuthToken")
public class AdminAuthToken {

    @Id
    @Column(name = "admin_id")
    private Integer adminId;
    @Column(name = "token")
    private String token;

    public int getAdminId() {
        return adminId;
    }

    public AdminAuthToken setAdminId(int adminId) {
        this.adminId = adminId;
        return this;
    }

    public String getToken() {
        return token;
    }

    public AdminAuthToken setToken(String token) {
        this.token = token;
        return this;
    }
}
