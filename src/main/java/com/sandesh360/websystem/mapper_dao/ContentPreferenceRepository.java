package com.sandesh360.websystem.mapper_dao;

import com.sandesh360.websystem.entity.ContentPreference;
import org.apache.ibatis.annotations.*;

import java.math.BigInteger;
import java.util.List;

@Mapper
public interface ContentPreferenceRepository {

    @Insert("INSERT INTO ContentPreference (id, preference_id, content_id," +
            " created_by, updated_by, created_at, updated_at)" +
            " VALUES (null, #{preferenceId}, #{contentId}," +
            " #{createdBy}, #{updatedBy}, #{createdAt}, #{updatedAt})")
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    int insert(ContentPreference contentPreference);

    @Update("UPDATE ContentPreference SET content_id = #{contentId}," +
            " updated_by = #{updatedBy}, updated_at = #{updatedAt} WHERE id = #{id}")
    @Options(keyProperty = "id", keyColumn = "id")
    int updateContentId(ContentPreference contentCategory);

    @Update("UPDATE ContentPreference SET preference_id = #{preferenceId}," +
            " updated_by = #{updatedBy}, updated_at = #{updatedAt} WHERE id = #{id}")
    @Options(keyProperty = "id", keyColumn = "id")
    int updatePreferenceId(ContentPreference contentCategory);

    @Delete("DELETE FROM ContentPreference WHERE id = #{id}")
    int deleteById(@Param("id") final long id);

    @Delete("DELETE FROM ContentPreference WHERE content_id = #{contentId}")
    int deleteByContentId(@Param("contentId") final BigInteger contentId);

    @Select("SELECT * FROM ContentPreference WHERE id = #{id}")
    ContentPreference findById(@Param("id") final long id);

    @Select("SELECT * FROM ContentPreference cp ORDER BY cp.id ASC")
    List<ContentPreference> findAll();

}
