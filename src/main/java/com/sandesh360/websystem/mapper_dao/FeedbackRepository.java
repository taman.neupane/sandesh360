package com.sandesh360.websystem.mapper_dao;

import com.sandesh360.websystem.entity.Feedback;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface FeedbackRepository {

    @Insert("INSERT INTO `Feedback`(`id`, `from`, `comment`, `is_delete`, `created_at`)" +
            " VALUES (NULL, #{from}, #{comment}, #{isDelete}, #{createdAt})")

    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    int insert(Feedback feedback);

    @Update("UPDATE Feedback SET is_delete = #{isDelete} WHERE id = #{id}")
    @Options(keyProperty = "id", keyColumn = "id")
    int update(Feedback feedback);

    @Delete("DELETE FROM Feedback WHERE id = #{id}")
    int deleteById(@Param("id") final long id);

    @Select("SELECT * FROM Feedback WHERE id = #{id}")
    Feedback findById(@Param("id") final long id);

    @Select("SELECT * FROM Feedback f ORDER BY f.id DESC")
    List<Feedback> findAll();

}
