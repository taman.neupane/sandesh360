package com.sandesh360.websystem.mapper_dao;

import com.sandesh360.websystem.entity.AdminAuthToken;
import org.apache.ibatis.annotations.*;

@Mapper
public interface AdminAuthRepository {

    @Insert("INSERT INTO AdminAuthToken (admin_id, token) VALUES (#{adminId}, #{token})")
    @Options(useGeneratedKeys = false, keyProperty = "id", keyColumn = "auth_token")
    int insert(AdminAuthToken authToken);

    @Update("UPDATE AdminAuthToken SET token = #{token} WHERE admin_id = #{adminId}")
    int update(AdminAuthToken authToken);
    @Delete("DELETE FROM AdminAuthToken WHERE admin_id = #{id}")
    int deleteById(@Param("id") final long id);

    @Select("SELECT * FROM AdminAuthToken WHERE admin_id = #{id}")
    AdminAuthToken findById(@Param("id") final long id);

    @Select("SELECT * FROM AdminAuthToken WHERE token = #{token}")
    AdminAuthToken findByToken(@Param("token") final String token);


}
