package com.sandesh360.websystem.mapper_dao;

import com.sandesh360.websystem.entity.Poll;
import com.sandesh360.websystem.model.PollPercentage;
import org.apache.ibatis.annotations.*;

import java.math.BigInteger;
import java.util.List;

@Mapper
public interface PollRepository {

    @Insert("INSERT INTO Poll (id, qid, voter, is_yes, voted_at)" +
            " VALUES (null, #{questionId}, #{voter}, #{isYes}, #{votedAt})")
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    int insert(Poll poll);

    @Update("UPDATE Poll SET is_yes = #{isYes} WHERE id = #{id}")
    int update(Poll poll);

    @Delete("DELETE FROM Poll WHERE id = #{id}")
    int deleteById(@Param("id") final long id);

    @Select("SELECT * FROM Poll WHERE id = #{id}")
    Poll findById(@Param("id") final long id);

    @Select("SELECT * FROM Poll p ORDER BY p.id ASC")
    List<Poll> findAll();

    @Select("SELECT p.qid AS questionId, (COUNT(p.is_yes)* 100 / (SELECT COUNT(*) FROM Poll pp WHERE pp.qid = #{questionId})) AS yes " +
            "FROM Poll p WHERE p.qid = #{questionId} AND p.is_yes = 1")
    PollPercentage getPercentageByQuestionId(BigInteger questionId);

}
