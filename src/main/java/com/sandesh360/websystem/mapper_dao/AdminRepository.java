package com.sandesh360.websystem.mapper_dao;

import com.sandesh360.websystem.entity.Admin;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface AdminRepository {

    @Insert("INSERT INTO Admin (id, user_name, password, name, phone, email, address, " +
            "is_active, is_delete, created_by, updated_by, created_at, updated_at) " +
            "VALUES (null, #{userName}, #{password}, #{name}, #{phone}, #{email}, #{address}, " +
            "#{isActive}, #{isDelete}, #{createdBy},  #{updatedBy}, #{createdAt}, #{updatedAt})")
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    int insert(Admin admin);

    @Update("UPDATE Admin SET name = #{name}, phone = #{phone}, email = #{email}, address = #{address}, " +
            "is_active = #{isActive}, is_delete = #{isDelete}, updated_by = #{updatedBy}, " +
            "updated_at = #{updatedAt} WHERE id = #{id}")
    int update(Admin admin);

    @Update("UPDATE Admin SET password = #{newpass} WHERE id = #{id} AND password = #{oldpass}")
    int updatePassword(@Param("id") long id, @Param("oldpass") String old, @Param("newpass") String newpass);

    @Update("UPDATE Admin SET password = #{password} WHERE email = #{email}")
    int resetPassword(@Param("email") String email);

    @Delete("DELETE FROM Admin WHERE id = #{id}")
    int deleteById(@Param("id") final long id);

    @Select("SELECT * FROM Admin WHERE id = #{id}")
    Admin findById(@Param("id") final long id);

    @Select("SELECT * FROM Admin ORDER BY Admin.name ASC")
    List<Admin> findAll();

    @Select("SELECT * FROM Admin WHERE user_name = #{userName} AND password = #{password}")
    Admin login(@Param("userName") final String userName, @Param("password") String password);


}
