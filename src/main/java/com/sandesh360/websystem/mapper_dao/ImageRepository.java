package com.sandesh360.websystem.mapper_dao;

import com.sandesh360.websystem.entity.Image;
import org.apache.ibatis.annotations.*;

import java.math.BigInteger;
import java.util.List;

@Mapper
public interface ImageRepository {

    @Insert("INSERT INTO Image (id, image_url, video_url, content_id, uploaded_at)" +
            " VALUES (null, #{imageUrl}, #{videoUrl}, #{contentId}, #{uploadedAt})")
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    int insert(Image image);

    @Update("UPDATE Image SET image_url = #{imageUrl}, video_url = #{videoUrl} WHERE id = #{id}")
    int update(Image image);

    @Update("UPDATE Image SET image_url = #{imageUrl} WHERE id = #{id}")
    int updateImageUrl(Image image);

    @Update("UPDATE Image SET video_url = #{videoUrl} WHERE id = #{id}")
    int updateVideoUrl(Image image);

    @Delete("DELETE FROM Image WHERE id = #{id}")
    int deleteById(@Param("id") final long id);

    @Delete("DELETE FROM Image WHERE content_id = #{contentId}")
    int deleteByContentId(@Param("contentId") final BigInteger contentId);

    @Select("SELECT * FROM Image WHERE id = #{id}")
    Image findById(@Param("id") final long id);

    @Select("SELECT * FROM Image f ORDER BY f.id ASC")
    List<Image> findAll();

}
