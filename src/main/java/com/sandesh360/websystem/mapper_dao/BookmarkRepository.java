package com.sandesh360.websystem.mapper_dao;

import com.sandesh360.websystem.entity.Bookmarks;
import org.apache.ibatis.annotations.*;

import java.math.BigInteger;
import java.util.List;

@Mapper
public interface BookmarkRepository {

    @Insert("INSERT INTO Bookmarks (id, user_id, content_id, status)" +
            " VALUES (null, #{userId}, #{contentId}, #{status})")
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    int insert(Bookmarks bookmarks);

    @Update("UPDATE Bookmarks SET status = #{status} WHERE id = #{id}")
    int update(Bookmarks bookmarks);

    @Delete("DELETE FROM Bookmarks WHERE id = #{id}")
    int deleteById(@Param("id") final long id);

    @Delete("DELETE FROM Bookmarks WHERE id = #{id}")
    int removeById(@Param("id") final BigInteger id);

    @Select("SELECT * FROM Bookmarks WHERE id = #{id}")
    Bookmarks findById(@Param("id") final long id);

    @Select("SELECT * FROM Bookmarks ORDER BY Bookmarks.id DESC")
    List<Bookmarks> findAll();

}
