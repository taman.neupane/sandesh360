package com.sandesh360.websystem.mapper_dao;

import com.sandesh360.websystem.entity.User;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface UserRepository {

    @Insert("INSERT INTO User (id, user_name, password, name, phone, email, address, " +
            "is_active, is_delete, created_at, updated_at, profile_pic) " +
            "VALUES (null, #{userName}, #{password}, #{name}, #{phone}, #{email}, #{address}, " +
            "#{isActive}, #{isDelete}, #{createdAt}, #{updatedAt}, #{profilePic})")
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    int insert(User user);

    @Update("UPDATE User SET name = #{name}, phone = #{phone}, email = #{email}, address = #{address}, " +
            "is_active = #{isActive}, is_delete = #{isDelete}, WHERE id = #{id}")
    int update(User user);

    @Update("UPDATE User SET profile_pic = #{profilePic} WHERE id = #{id}")
    int updateProfilePic(User user);

    @Select("SELECT * FROM User WHERE id = #{id}")
    User findById(@Param("id") final long id);

    @Delete("DELETE FROM User WHERE id = #{id}")
    int deleteById(@Param("id") final long id);


    @Select("SELECT * FROM User u ORDER BY u.name ASC")
    List<User> findAll();

    @Select("SELECT * FROM User WHERE email = #{email} AND password = #{password}")
    User login(User user);

}
