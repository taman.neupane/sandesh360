package com.sandesh360.websystem.mapper_dao;

import com.sandesh360.websystem.entity.Language;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface LanguageRepository {

    @Insert("INSERT INTO Language (id, name, locale) VALUES (null, #{name}, #{locale})")
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    int insert(Language language);

    @Update("UPDATE Language SET name = #{name},locale = #{locale} WHERE id = #{id}")
    int update(Language language);

    @Select("SELECT * FROM Language WHERE id = #{id}")
    Language findById(@Param("id") final long id);

    @Delete("DELETE FROM Language WHERE id = #{id}")
    int deleteById(@Param("id") final long id);

    @Select("SELECT * FROM Language ORDER BY Language.name ASC")
    List<Language> findAll(@Param("orderByName") String orderByName);
}
