package com.sandesh360.websystem.mapper_dao;

import com.sandesh360.websystem.entity.Questions;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface QuestionRepository {

    @Insert("INSERT INTO Questions (id, question_text, content_id, language_id, is_active," +
            " is_delete, created_by, updated_by, created_at, updated_at)" +
            " VALUES (null, #{questionText}, #{contentId}, #{languageId}, #{isActive}," +
            " #{isDelete}, #{createdBy}, #{updatedBy}, #{createdAt}, #{updatedAt})")
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    int insert(Questions question);

    @Update("UPDATE Questions SET question_text = #{questionText}, content_id = #{contentId}," +
            " language_id = #{languageId}, is_active = #{isActive}, is_delete = #{isDelete}," +
            " updated_by = #{updatedBy}, updated_at = #{updatedAt} WHERE id = #{id}")
    int update(Questions question);

    @Delete("DELETE FROM Questions WHERE id = #{id}")
    int deleteById(@Param("id") final long id);

    @Select("SELECT * FROM Questions WHERE id = #{id}")
    Questions findById(@Param("id") final long id);

    @Select("SELECT * FROM Questions q ORDER BY q.id DESC")
    List<Questions> findAll();

    @Select("SELECT * FROM Questions q WHERE q.is_active = 1 AND q.is_delete = 0 ORDER BY q.id DESC")
    List<Questions> findActiveQuestions();
}
