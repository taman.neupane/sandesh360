package com.sandesh360.websystem.mapper_dao;

import com.sandesh360.websystem.entity.Preference;
import com.sandesh360.websystem.model.Test;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.jdbc.SQL;

import java.util.List;

@Mapper
public interface PreferenceRepository {

    @Insert("INSERT INTO Preference (id, preference_name, icon," +
            " language_id, is_active, is_delete, created_by," +
            " updated_by, created_at, updated_at)" +
            " VALUES (null, #{preferenceName}, #{iconUrl}, #{languageId}," +
            " #{isActive}, #{isDelete}, #{createdBy}," +
            " #{updatedBy}, #{createdAt}, #{updatedAt})")
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    int insert(Preference preference);

    @Update("UPDATE Preference SET preference_name = #{preferenceName}, language_id = #{languageId}," +
            " is_active = #{isActive}, is_delete = #{isDelete}," +
            " updated_by = #{updatedBy}, updated_at = #{updatedAt} WHERE id = #{id}")
    @Options(keyProperty = "id", keyColumn = "id")
    int update(Preference preference);

    @Update("UPDATE Preference SET icon = #{iconUrl} WHERE id = #{id}")
    int updateIconUrl(Preference preference);

    @Update("UPDATE Preference SET is_active = #{isActive}, updated_by = #{updatedBy}," +
            " updated_at = #{updatedAt} WHERE id = #{id}")
    @Options(keyProperty = "id", keyColumn = "id")
    int updateActiveStatus(Preference preference);

    @Update("UPDATE Preference SET is_delete = #{isDelete}, updated_by = #{updatedBy}," +
            " updated_at = #{updatedAt} WHERE id = #{id}")
    @Options(keyProperty = "id", keyColumn = "id")
    int updateDeleteStatus(Preference preference);

    @Delete("DELETE FROM Preference WHERE id = #{id}")
    int deleteById(@Param("id") final long id);

    @Select("SELECT * FROM Preference where id = #{id}")
    Preference findById(@Param("id") final long id);

    @Select("SELECT * FROM Preference p ORDER BY p.preference_name ASC")
    List<Preference> findAll();

    @Select("SELECT * FROM Preference p WHERE p.is_active = 1 AND p.is_delete = 0 ORDER BY p.preference_name ASC")
    List<Preference> findActivePreferences();

    @Select("SELECT p.id, p.preference_name, l.locale " +
            "FROM `Preference` p " +
            "INNER JOIN Language l " +
            "ON p.language_id = l. id " +
            "WHERE l.id = #{id}")
    List<Test> findAllByLanguageId(@Param("id") final int id);

}
