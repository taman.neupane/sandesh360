package com.sandesh360.websystem.mapper_dao;

import com.sandesh360.websystem.entity.Content;
import com.sandesh360.websystem.entity.Image;
import com.sandesh360.websystem.model.ContentExtendedInfo;
import org.apache.ibatis.annotations.*;

import java.math.BigInteger;
import java.util.List;

@Mapper
public interface ContentRepository {

    @Insert("INSERT INTO Content (id, title, short_description, description, content_url," +
            " site_url, language_id, is_active, is_delete, created_by, updated_by, created_at," +
            " updated_at) VALUES (null, #{title}, #{shortDescription}, #{description}, #{contentUrl}," +
            " #{siteUrl}, #{languageId}, #{isActive}, #{isDelete}, #{createdBy}, #{updatedBy}," +
            " #{createdAt}, #{updatedAt})")
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    int insert(Content Content);

    @Update("UPDATE Content SET title = #{title}, short_description = #{shortDescription}," +
            " description = #{description}, content_url = #{contentUrl}, site_url = #{siteUrl}," +
            " language_id = #{languageId}, is_active = #{isActive}, is_delete = #{isDelete}," +
            " updated_by = #{updatedBy}, updated_at = #{updatedAt} WHERE id = #{id}")
    int update(Content Content);

    @Delete("DELETE FROM Content WHERE id = #{id}")
    int deleteById(@Param("id") final long id);

    @Select("SELECT * FROM Content WHERE id = #{id}")
    Content findById(@Param("id") final long id);

    @Select("SELECT * FROM Content c ORDER BY c.title ASC")
    List<Content> findAll();


    @Select("SELECT * FROM Content WHERE id = #{id}")
    @Results(value = {
            @Result(property = "images",
                    javaType = List.class,
                    column = "id",
                    many = @Many(select = "findImagesByContentId")
            )
    })
    ContentExtendedInfo findContentById(BigInteger id);

    @Select("SELECT * FROM Content c " +
            "LEFT JOIN ContentPreference cp " +
            "ON c.id = cp.content_id " +
            "WHERE c.is_active = 1 AND c.language_id = #{languageId} AND cp.preference_id = #{preferenceId} " +
            "ORDER BY c.updated_at DESC")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "title", column = "title"),
            @Result(property = "shortDescription", column = "short_description"),
            @Result(property = "description", column = "description"),
            @Result(property = "contentUrl", column = "content_url"),
            @Result(property = "siteUrl", column = "site_url"),
            @Result(property = "languageId", column = "language_id"),
            @Result(property = "isActive", column = "is_active"),
            @Result(property = "isDelete", column = "is_delete"),
            @Result(property = "createdBy", column = "created_by"),
            @Result(property = "updatedBy", column = "updated_by"),
            @Result(property = "createdAt", column = "created_at"),
            @Result(property = "updatedAt", column = "updated_at"),
            @Result(property = "images",
                    javaType = List.class,
                    column = "id",
                    many = @Many(select = "findImagesByContentId")
            )
    })
    List<ContentExtendedInfo> findActiveContentByPreferenceId(ContentExtendedInfo content);

    @Select("SELECT * FROM Content c " +
            "LEFT JOIN ContentCategory cc " +
            "ON c.id = cc.content_id " +
            "WHERE c.is_active = 1 AND c.language_id = #{languageId} AND cc.category_id = #{categoryId} " +
            "ORDER BY c.updated_at DESC")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "title", column = "title"),
            @Result(property = "shortDescription", column = "short_description"),
            @Result(property = "description", column = "description"),
            @Result(property = "contentUrl", column = "content_url"),
            @Result(property = "siteUrl", column = "site_url"),
            @Result(property = "languageId", column = "language_id"),
            @Result(property = "isActive", column = "is_active"),
            @Result(property = "isDelete", column = "is_delete"),
            @Result(property = "createdBy", column = "created_by"),
            @Result(property = "updatedBy", column = "updated_by"),
            @Result(property = "createdAt", column = "created_at"),
            @Result(property = "updatedAt", column = "updated_at"),
            @Result(property = "images",
                    javaType = List.class,
                    column = "id",
                    many = @Many(select = "findImagesByContentId")
            )
    })
    List<ContentExtendedInfo> findActiveContentByCategoryId(ContentExtendedInfo content);

    @Select("SELECT * FROM Content c " +
            "LEFT JOIN Bookmarks b " +
            "ON c.id = b.content_id " +
            "WHERE c.is_active = 1 AND c.language_id = #{languageId} AND b.user_id = #{userId} " +
            "ORDER BY c.updated_at DESC")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "title", column = "title"),
            @Result(property = "shortDescription", column = "short_description"),
            @Result(property = "description", column = "description"),
            @Result(property = "contentUrl", column = "content_url"),
            @Result(property = "siteUrl", column = "site_url"),
            @Result(property = "languageId", column = "language_id"),
            @Result(property = "isActive", column = "is_active"),
            @Result(property = "isDelete", column = "is_delete"),
            @Result(property = "createdBy", column = "created_by"),
            @Result(property = "updatedBy", column = "updated_by"),
            @Result(property = "createdAt", column = "created_at"),
            @Result(property = "updatedAt", column = "updated_at"),
            @Result(property = "images",
                    javaType = List.class,
                    column = "id",
                    many = @Many(select = "findImagesByContentId")
            )
    })
    List<ContentExtendedInfo> findBookmarkContentByUserId(ContentExtendedInfo content);


    @Select("SELECT * FROM Image WHERE content_id = #{contentId}")
    List<Image> findImagesByContentId(BigInteger contentId);

}
