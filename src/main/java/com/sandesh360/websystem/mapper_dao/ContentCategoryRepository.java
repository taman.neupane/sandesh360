package com.sandesh360.websystem.mapper_dao;

import com.sandesh360.websystem.entity.ContentCategory;
import org.apache.ibatis.annotations.*;

import java.math.BigInteger;
import java.util.List;

@Mapper
public interface ContentCategoryRepository {

    @Insert("INSERT INTO ContentCategory (id, category_id, content_id," +
            " created_by, updated_by, created_at, updated_at)" +
            " VALUES (null, #{categoryId}, #{contentId}," +
            " #{createdBy}, #{updatedBy}, #{createdAt}, #{updatedAt})")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    int insert(ContentCategory contentCategory);

    @Update("UPDATE ContentCategory SET content_id = #{contentId}," +
            " updated_by = #{updatedBy}, updated_at = #{updatedAt} WHERE id = #{id}")
    @Options(keyProperty = "id", keyColumn = "id")
    int updateContentId(ContentCategory contentCategory);

    @Update("UPDATE ContentCategory SET category_id = #{categoryId}," +
            " updated_by = #{updatedBy}, updated_at = #{updatedAt} WHERE id = #{id}")
    @Options(keyProperty = "id", keyColumn = "id")
    int updateCategoryId(ContentCategory contentCategory);

    @Delete("DELETE FROM ContentCategory WHERE id = #{id}")
    int deleteById(@Param("id") final long id);

    @Delete("DELETE FROM ContentCategory WHERE content_id = #{contentId}")
    int deleteByContentId(@Param("contentId") final BigInteger contentId);

    @Select("SELECT * FROM ContentCategory WHERE id = #{id}")
    ContentCategory findById(@Param("id") final long id);

    @Select("SELECT * FROM ContentCategory cc ORDER BY cc.id ASC")
    List<ContentCategory> findAll();

}
