package com.sandesh360.websystem.mapper_dao;

import com.sandesh360.websystem.entity.Category;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface CategoryRepository {

    @Insert("INSERT INTO Category (id, category_name, description, icon, language_id," +
            " is_active, is_delete, created_by, updated_by, created_at, updated_at)" +
            " VALUES (null, #{categoryName}, #{description}, #{iconUrl}, #{languageId}," +
            " #{isActive}, #{isDelete}, #{createdBy}, #{updatedBy}, #{createdAt}, #{updatedAt})")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    int insert(Category category);

    @Update("UPDATE Category SET category_name = #{categoryName}," +
            " language_id = #{languageId}, is_active = #{isActive}, is_delete = #{isDelete}," +
            " updated_by = #{updatedBy}, updated_at = #{updatedAt} WHERE id = #{id}")
    int update(Category category);

    @Update("UPDATE Category SET icon = #{iconUrl} WHERE id = #{id}")
    int updateIconUrl(Category Category);

    @Update("UPDATE Category SET is_active = #{isActive}, updated_by = #{updatedBy}," +
            " updated_at = #{updatedAt} WHERE id = #{id}")
    @Options(keyProperty = "id", keyColumn = "id")
    int updateActiveStatus(Category category);

    @Update("UPDATE Category SET is_delete = #{isDelete}, updated_by = #{updatedBy}," +
            " updated_at = #{updatedAt} WHERE id = #{id}")
    @Options(keyProperty = "id", keyColumn = "id")
    int updateDeleteStatus(Category category);

    @Delete("DELETE FROM Category WHERE id = #{id}")
    int deleteById(@Param("id") final long id);

    @Select("SELECT * FROM Category where id = #{id}")
    Category findById(@Param("id") final long id);

    @Select("SELECT * FROM Category c WHERE c.is_active = 1 AND c.is_delete = 0 ORDER BY c.category_name ASC")
    List<Category> findActiveCategories();

    @Select("SELECT * FROM Category c ORDER BY c.category_name ASC")
    List<Category> findAll();

}
