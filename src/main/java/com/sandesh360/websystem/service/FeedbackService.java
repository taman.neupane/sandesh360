package com.sandesh360.websystem.service;

import com.sandesh360.websystem.common.Utility;
import com.sandesh360.websystem.entity.Feedback;
import com.sandesh360.websystem.mapper_dao.FeedbackRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FeedbackService implements IBaseService<Feedback> {

    @Autowired
    private FeedbackRepository repository;

    @Override
    public Feedback insert(Feedback entity) {
        entity.setCreatedAt(Utility.getCurrentDateTime());
        entity.setDelete(false);
        repository.insert(entity);
        return entity;
    }

    @Override
    public Feedback update(Feedback entity) {
        repository.update(entity);
        return entity;
    }

    @Override
    public int deleteById(long id) {
        return repository.deleteById(id);
    }

    @Override
    public List<Feedback> findAll() {
        return repository.findAll();
    }

    @Override
    public Feedback findById(long id) {
        return repository.findById(id);
    }
}
