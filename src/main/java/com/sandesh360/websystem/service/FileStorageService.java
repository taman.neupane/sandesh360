package com.sandesh360.websystem.service;

import com.sandesh360.websystem.model.FileStorageException;
import com.sandesh360.websystem.model.FileNotFoundException;
import com.sandesh360.websystem.property.FileStorageProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Service
public class FileStorageService {

    private final Path fileStorageLocation;

    @Autowired
    public FileStorageService(FileStorageProperties fileStorageProperties) {
        this.fileStorageLocation = Paths.get(fileStorageProperties.getUploadDir())
                .toAbsolutePath().normalize();

        try {
            Files.createDirectories(this.fileStorageLocation);
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new FileStorageException("Could not create the directory where the uploaded files will be stored.", ex);
        }
    }

    public String storeFile(MultipartFile file, String fileName) {
//        // Normalize file name
        //String fileName1 = StringUtils.cleanPath(file.getOriginalFilename());

        try {
            // Copy file to the target location (Replacing existing file with the same name)
            Path targetLocation = this.fileStorageLocation.resolve(fileName);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

            return fileName;
        } catch (IOException ex) {
            ex.printStackTrace();
            throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
        }
    }

    public Resource loadFileAsResource(String fileName) {
        try {
            Path filePath = this.fileStorageLocation.resolve(fileName).normalize();
            Resource resource = new UrlResource(filePath.toUri());
            if (resource.exists()) {
                return resource;
            } else {
                throw new FileNotFoundException("File not found " + fileName);
            }
        } catch (MalformedURLException ex) {
            ex.printStackTrace();
            throw new FileNotFoundException("File not found " + fileName, ex);
        }
    }

    public void fileUploadService(MultipartFile file, String fileName) throws Exception {

        Path targetLocation = this.fileStorageLocation.resolve(fileName);

        File uploadFile = targetLocation.toFile();
        if(!uploadFile.exists()){
            uploadFile.createNewFile();
        }
        // Stream to write data to file in server.
        BufferedOutputStream boStream = null;
        try{
            boStream = new BufferedOutputStream(new FileOutputStream(uploadFile));
            boStream.write(file.getBytes());
        }finally{
            boStream.flush();
            boStream.close();
        }
    }

}