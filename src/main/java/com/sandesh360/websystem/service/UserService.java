package com.sandesh360.websystem.service;

import com.sandesh360.websystem.common.Utility;
import com.sandesh360.websystem.entity.User;
import com.sandesh360.websystem.mapper_dao.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService implements IBaseService<User> {

    @Autowired
    private UserRepository repository;

    @Override
    public User insert(User entity) {
        entity.setCreatedAt(Utility.getCurrentDateTime());
        entity.setUpdatedAt(Utility.getCurrentDateTime());
        repository.insert(entity);
        return entity;
    }

    @Override
    public User update(User entity) {
        repository.update(entity);
        return entity;
    }

    @Override
    public int deleteById(long id) {
        return repository.deleteById(id);
    }

    @Override
    public List<User> findAll() {
        return repository.findAll();
    }

    @Override
    public User findById(long id) {
        return repository.findById(id);
    }

    public User login(User user) {
        return repository.login(user);
    }

    public int updateProfilePic(User user) {
        return repository.updateProfilePic(user);
    }
}
