package com.sandesh360.websystem.service;

import java.util.List;


public interface IBaseService<T> {

    T insert(T entity);

    T update(T entity);

    int deleteById(long id);

    List<T> findAll();

    T findById(long id);

}
