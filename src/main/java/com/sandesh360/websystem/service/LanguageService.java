package com.sandesh360.websystem.service;

import com.sandesh360.websystem.entity.Language;
import com.sandesh360.websystem.mapper_dao.LanguageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LanguageService implements IBaseService<Language> {

    @Autowired
    private LanguageRepository repository;

    @Override
    public Language insert(Language entity) {
        repository.insert(entity);
        return entity;
    }

    @Override
    public Language update(Language entity) {
        repository.update(entity);
        return entity;
    }

    @Override
    public int deleteById(long id) {
        return repository.deleteById(id);
    }

    @Override
    public List<Language> findAll() {
        return null;
    }

    @Override
    public Language findById(long id) {
        return repository.findById(id);
    }

    public List<Language> findAllOrderByName(String orderByName) {
        return repository.findAll(orderByName);
    }
}
