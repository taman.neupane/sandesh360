package com.sandesh360.websystem.service;

import com.sandesh360.websystem.common.Utility;
import com.sandesh360.websystem.entity.Poll;
import com.sandesh360.websystem.mapper_dao.PollRepository;
import com.sandesh360.websystem.model.PollPercentage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.List;

@Service
public class PollService implements IBaseService<Poll> {

    @Autowired
    private PollRepository repository;

    @Override
    public Poll insert(Poll entity) {
        entity.setVotedAt(Utility.getCurrentDateTime());
        repository.insert(entity);
        return entity;
    }

    @Override
    public Poll update(Poll entity) {
        repository.update(entity);
        return null;
    }

    @Override
    public int deleteById(long id) {
        return repository.deleteById(id);
    }

    @Override
    public List<Poll> findAll() {
        return repository.findAll();
    }

    @Override
    public Poll findById(long id) {
        return repository.findById(id);
    }

    public PollPercentage getPollPercentageByQId(BigInteger qid) {
        return repository.getPercentageByQuestionId(qid);
    }
}
