package com.sandesh360.websystem.service;

import com.sandesh360.websystem.common.Utility;
import com.sandesh360.websystem.entity.ContentPreference;
import com.sandesh360.websystem.mapper_dao.ContentPreferenceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.List;

@Service
public class ContentPreferenceService implements IBaseService<ContentPreference> {

    @Autowired
    private ContentPreferenceRepository repository;

    @Override
    public ContentPreference insert(ContentPreference entity) {
        entity.setCreatedAt(Utility.getCurrentDateTime());
        repository.insert(entity);
        return entity;
    }

    @Override
    public ContentPreference update(ContentPreference entity) {
        return null;
    }

    @Override
    public int deleteById(long id) {
        return repository.deleteById(id);
    }

    @Override
    public List<ContentPreference> findAll() {
        return repository.findAll();
    }

    @Override
    public ContentPreference findById(long id) {
        return repository.findById(id);
    }

    public ContentPreference updateContentId(ContentPreference contentPreference) {
        repository.updateContentId(contentPreference);
        return contentPreference;

    }

    public ContentPreference updatePreferenceId(ContentPreference contentPreference) {
        repository.updatePreferenceId(contentPreference);
        return contentPreference;

    }

    public int deleteByContentId(BigInteger contentId) {
        return repository.deleteByContentId(contentId);
    }
}
