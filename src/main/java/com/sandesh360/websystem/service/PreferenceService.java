package com.sandesh360.websystem.service;


import com.sandesh360.websystem.common.Utility;
import com.sandesh360.websystem.entity.Preference;
import com.sandesh360.websystem.mapper_dao.PreferenceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PreferenceService implements IBaseService<Preference> {

    @Autowired
    private PreferenceRepository repository;


    @Override
    public Preference insert(Preference entity) {
        entity.setActive(true);
        entity.setDelete(false);
        entity.setCreatedAt(Utility.getCurrentDateTime());
        entity.setUpdatedAt(Utility.getCurrentDateTime());
        repository.insert(entity);
        return entity;
    }

    @Override
    public Preference update(Preference entity) {
        entity.setUpdatedAt(Utility.getCurrentDateTime());
        repository.update(entity);
        return entity;
    }

    @Override
    public int deleteById(long id) {
        return repository.deleteById(id);
    }

    @Override
    public List<Preference> findAll() {
        return repository.findAll();
    }

    @Override
    public Preference findById(long id) {
        return repository.findById(id);
    }

    public int updateIconUrl(Preference preference) {
        return repository.updateIconUrl(preference);
    }

    public Preference updateActiveStatus(Preference preference) {
        preference.setUpdatedAt(Utility.getCurrentDateTime());
        repository.updateActiveStatus(preference);
        return preference;
    }

    public Preference updateDeleteStatus(Preference preference) {
        preference.setUpdatedAt(Utility.getCurrentDateTime());
        repository.updateDeleteStatus(preference);
        return preference;
    }

    public List<Preference> findActivePreferences() {
        return repository.findActivePreferences();
    }
}
