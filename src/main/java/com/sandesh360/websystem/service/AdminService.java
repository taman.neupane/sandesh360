package com.sandesh360.websystem.service;

import com.sandesh360.websystem.common.Utility;
import com.sandesh360.websystem.entity.Admin;
import com.sandesh360.websystem.mapper_dao.AdminRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdminService implements IBaseService<Admin> {

    @Autowired
    private AdminRepository repository;

    @Override
    public Admin insert(Admin entity) {
        entity.setActive(true);
        entity.setDelete(false);
        entity.setCreatedAt(Utility.getCurrentDateTime());
        entity.setUpdatedAt(Utility.getCurrentDateTime());
        repository.insert(entity);
        return entity;
    }

    @Override
    public Admin update(Admin entity) {
        entity.setUpdatedBy(entity.getName());
        entity.setUpdatedAt(Utility.getCurrentDateTime());
        repository.update(entity);
        return entity;
    }

    @Override
    public int deleteById(long id) {
        return repository.deleteById(id);

    }

    @Override
    public List<Admin> findAll() {
        return repository.findAll();
    }

    @Override
    public Admin findById(long id) {
        return repository.findById(id);
    }

    public Admin login(String userName, String password) {
        return repository.login(userName, password);
    }

    public long changePassword(long id, String oldPass, String newPass) {
        return repository.updatePassword(id, oldPass, newPass);
    }

    public long resetPasswordByEmail(String email) {
        return repository.resetPassword(email);
    }
}
