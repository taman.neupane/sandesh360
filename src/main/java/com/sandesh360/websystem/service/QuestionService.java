package com.sandesh360.websystem.service;

import com.sandesh360.websystem.common.Utility;
import com.sandesh360.websystem.entity.Questions;
import com.sandesh360.websystem.mapper_dao.QuestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QuestionService implements IBaseService<Questions> {

    @Autowired
    private QuestionRepository repository;

    @Override
    public Questions insert(Questions entity) {
        entity.setCreatedAt(Utility.getCurrentDateTime());
        repository.insert(entity);
        return entity;
    }

    @Override
    public Questions update(Questions entity) {
        entity.setUpdatedAt(Utility.getCurrentDateTime());
        int index = repository.update(entity);
        return index > 0 ? entity : null;
    }

    @Override
    public int deleteById(long id) {
        return repository.deleteById(id);
    }

    @Override
    public List<Questions> findAll() {
        return repository.findAll();
    }

    @Override
    public Questions findById(long id) {
        return repository.findById(id);
    }

    public List<Questions> findActiveQuestions() {
        return repository.findActiveQuestions();
    }
}
