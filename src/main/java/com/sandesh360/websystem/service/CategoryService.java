package com.sandesh360.websystem.service;


import com.sandesh360.websystem.common.Utility;
import com.sandesh360.websystem.entity.Category;
import com.sandesh360.websystem.mapper_dao.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryService implements IBaseService<Category> {

    @Autowired
    private CategoryRepository repository;


    @Override
    public Category insert(Category entity) {
        entity.setActive(true);
        entity.setDelete(false);
        entity.setCreatedAt(Utility.getCurrentDateTime());
        entity.setUpdatedAt(Utility.getCurrentDateTime());
        repository.insert(entity);
        return entity;
    }

    @Override
    public Category update(Category entity) {
        entity.setUpdatedAt(Utility.getCurrentDateTime());
        repository.update(entity);
        return entity;
    }

    @Override
    public int deleteById(long id) {
        return repository.deleteById(id);
    }

    @Override
    public List<Category> findAll() {
        return repository.findAll();
    }

    @Override
    public Category findById(long id) {
        return repository.findById(id);
    }

    public int updateIconUrl(Category category) {
        return repository.updateIconUrl(category);
    }

    public Category updateActiveStatus(Category category) {
        category.setUpdatedAt(Utility.getCurrentDateTime());
        repository.updateActiveStatus(category);
        return category;
    }

    public Category updateDeleteStatus(Category category) {
        category.setUpdatedAt(Utility.getCurrentDateTime());
        repository.updateDeleteStatus(category);
        return category;
    }

    public List<Category> findActiveCategories() {
        return repository.findActiveCategories();
    }
}
