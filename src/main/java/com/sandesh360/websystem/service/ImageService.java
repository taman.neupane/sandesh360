package com.sandesh360.websystem.service;

import com.sandesh360.websystem.common.Utility;
import com.sandesh360.websystem.entity.Image;
import com.sandesh360.websystem.mapper_dao.ImageRepository;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.List;

@Service
public class ImageService implements IBaseService<Image> {

    @Autowired
    private ImageRepository repository;

    @Override
    public Image insert(Image entity) {
        entity.setUploadedAt(Utility.getCurrentDateTime());
        repository.insert(entity);
        return entity;
    }

    @Override
    public Image update(Image entity) {
        repository.update(entity);
        return entity;
    }

    @Override
    public int deleteById(long id) {
        return repository.deleteById(id);
    }

    @Override
    public List<Image> findAll() {
        return repository.findAll();
    }

    @Override
    public Image findById(long id) {
        return repository.findById(id);
    }

    public Image updateImageUrl(Image image) {
        repository.updateImageUrl(image);
        return image;
    }

    public Image updateVideoUrl(Image image) {
        repository.updateVideoUrl(image);
        return image;
    }

    public int deleteByContentId(BigInteger contentId) {
        return repository.deleteByContentId(contentId);
    }
}
