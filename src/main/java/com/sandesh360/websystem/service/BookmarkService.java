package com.sandesh360.websystem.service;

import com.sandesh360.websystem.entity.Bookmarks;
import com.sandesh360.websystem.mapper_dao.BookmarkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.List;

@Service
public class BookmarkService implements IBaseService<Bookmarks> {

    @Autowired
    private BookmarkRepository repository;


    @Override
    public Bookmarks insert(Bookmarks entity) {
        repository.insert(entity);
        return entity;
    }

    @Override
    public Bookmarks update(Bookmarks entity) {
        repository.update(entity);
        return entity;
    }

    @Override
    public int deleteById(long id) {
        return repository.deleteById(id);
    }

    @Override
    public List<Bookmarks> findAll() {
        return repository.findAll();
    }

    @Override
    public Bookmarks findById(long id) {
        return repository.findById(id);
    }

    public int removeById(final BigInteger id) {
        return repository.removeById(id);
    }
}
