package com.sandesh360.websystem.service;

import com.sandesh360.websystem.common.Utility;
import com.sandesh360.websystem.entity.Content;
import com.sandesh360.websystem.mapper_dao.ContentRepository;
import com.sandesh360.websystem.model.ContentExtendedInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.List;

@Service
public class ContentService implements IBaseService<Content> {

    @Autowired
    private ContentRepository repository;

    @Override
    public Content insert(Content entity) {
        entity.setActive(true);
        entity.setDelete(false);
        entity.setCreatedAt(Utility.getCurrentDateTime());
        entity.setUpdatedAt(Utility.getCurrentDateTime());
        repository.insert(entity);
        return entity;
    }

    @Override
    public Content update(Content entity) {
        entity.setUpdatedAt(Utility.getCurrentDateTime());
        repository.update(entity);
        return entity;
    }

    @Override
    public int deleteById(long id) {
        return repository.deleteById(id);
    }

    @Override
    public List<Content> findAll() {
        return repository.findAll();
    }

    @Override
    public Content findById(long id) {
        return repository.findById(id);
    }

    public List<ContentExtendedInfo> findActiveContentByPreferenceId(ContentExtendedInfo content) {
        return repository.findActiveContentByPreferenceId(content);
    }

    public List<ContentExtendedInfo> findActiveContentByCategoryId(ContentExtendedInfo content) {
        return repository.findActiveContentByCategoryId(content);
    }

    public List<ContentExtendedInfo> findBookmarkContentByUserId(ContentExtendedInfo content) {
        return repository.findBookmarkContentByUserId(content);
    }

    public ContentExtendedInfo findContentById(BigInteger id) {
        return repository.findContentById(id);
    }
}
