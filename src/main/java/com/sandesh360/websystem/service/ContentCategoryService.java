package com.sandesh360.websystem.service;

import com.sandesh360.websystem.common.Utility;
import com.sandesh360.websystem.entity.ContentCategory;
import com.sandesh360.websystem.mapper_dao.ContentCategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.List;

@Service
public class ContentCategoryService implements IBaseService<ContentCategory> {

    @Autowired
    private ContentCategoryRepository repository;

    @Override
    public ContentCategory insert(ContentCategory entity) {
        entity.setCreatedAt(Utility.getCurrentDateTime());
        repository.insert(entity);
        return entity;
    }

    @Override
    public ContentCategory update(ContentCategory entity) {
        return null;
    }

    @Override
    public int deleteById(long id) {
        return repository.deleteById(id);
    }

    @Override
    public List<ContentCategory> findAll() {
        return repository.findAll();
    }

    @Override
    public ContentCategory findById(long id) {
        return repository.findById(id);
    }

    public ContentCategory updateContentId(ContentCategory contentCategory) {
        repository.updateContentId(contentCategory);
        return contentCategory;

    }

    public ContentCategory updateCategoryId(ContentCategory contentCategory) {
        repository.updateCategoryId(contentCategory);
        return contentCategory;

    }

    public int deleteByContentId(BigInteger contentId) {
        return repository.deleteByContentId(contentId);
    }
}
