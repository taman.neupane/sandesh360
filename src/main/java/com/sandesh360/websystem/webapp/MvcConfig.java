package com.sandesh360.websystem.webapp;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MvcConfig implements WebMvcConfigurer {

    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/login").setViewName("login");
        registry.addViewController("/home").setViewName("home");
        registry.addViewController("/category").setViewName("category");
        registry.addViewController("/content").setViewName("content");
        registry.addViewController("/preference").setViewName("preference");
        registry.addViewController("/registration").setViewName("registration");
        registry.addViewController("/forgot-password").setViewName("forgot-password");

    }
}