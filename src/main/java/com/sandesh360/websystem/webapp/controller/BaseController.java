//package com.sandesh360.websystem.webapp.controller;
//
//import com.sandesh360.websystem.common.ApplicationPropertiesUtility;
//import com.sandesh360.websystem.entity.Admin;
//import com.sandesh360.websystem.entity.BaseEntity;
//import eu.bitwalker.useragentutils.UserAgent;
//import org.apache.commons.lang3.StringUtils;
//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.ApplicationContext;
//import org.springframework.context.MessageSource;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.validation.BindingResult;
//import org.springframework.validation.ObjectError;
//import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
//
//import javax.servlet.http.Cookie;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//import java.util.List;
//
//
//@Configuration
//public class BaseController implements WebMvcConfigurer {
//    /**
//     * Logger Initialize
//     */
//    protected final Log log = LogFactory.getLog(BaseController.class);
//
//    @Autowired
//    MessageSource messageSource;
//
//    @Autowired
//    private ApplicationContext serviceApp;
//
//    @Autowired
//    public HttpServletRequest request;
//
//    @Autowired
//    public HttpServletResponse response;
//
//    BaseEntity e;
//
////	public String projectUrl = req.getServerName() + ":" + req.getServerPort() + req.getContextPath();
////	private static BaseController mInstance;
//
//    public BaseController() {
//
//    }
//
//
//    protected void getErrorMessageForProperties(BindingResult bindingResult) {
//        if (bindingResult != null && bindingResult.hasErrors()) {
//            List<ObjectError> errorObject = bindingResult.getAllErrors();
//
//            for (int i = 0; i < errorObject.size(); i++) {
//
//            }
//        }
//    }
//
//    public LoginForm validateLogin() {
//        log.info("BaseController: Inside validate login method");
//        try {
//            if (getClientIp() == Boolean.FALSE) {
//                return null;
//            }
//        } catch (Exception ex) {
//
//        }
//        Cookie[] cookies = request.getCookies();
//        LoginForm loginForm = null;
//        try {
//            log.info("BaseController: Getting cookie to validate: Cookie ");
//            if (cookies == null || cookies.length < 1) {
//                log.error("BaseController:: Cookie Not Found ");
//                return null;
//            }
//            for (Cookie cookie : cookies) {
//                if (StringUtils.equals(cookie.getName(), "adminId")) {
//                    loginForm = (LoginForm) request.getSession().getAttribute(getSessionKey(cookie.getValue()));
//                    return loginForm;
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return loginForm;
//    }
//
//    public Admin validateAdminLogin() {
//        log.info("BaseController: Inside validate login method");
//        try {
//            if (getClientIp() == Boolean.FALSE) {
//                return null;
//            }
//        } catch (Exception ex) {
//
//        }
//        Cookie[] cookies = request.getCookies();
//        Admin loginForm = null;
//        try {
//            log.info("BaseController: Getting cookie to validate: Cookie ");
//            if (cookies == null || cookies.length < 1) {
//                log.error("BaseController:: Cookie Not Found ");
//                return null;
//            }
////            loginForm = (Admin) request.getSession().getAttribute(getSessionKey(cookies[0].getValue()));
//            for (Cookie cookie : cookies) {
//                if (StringUtils.equals(cookie.getName(), "adminId") || StringUtils.equals(cookie.getName(), "JSESSIONID")) {
//                    loginForm = (Admin) request.getSession().getAttribute(getSessionKey(cookie.getValue()));
//                    return loginForm;
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return loginForm;
//    }
//
//    public String getSessionKey(String id) {
//        UserAgent userAgent = UserAgent.parseUserAgentString(request.getHeader("User-Agent"));
//        return id + userAgent.getBrowser() + userAgent.getBrowserVersion();
//    }
//
//    public String getResourceMessage(String messageCode) {
//        //String message = messageSource.getMessage(messageCode, null, "Message Not Found", response.getLocale());
//        String message = messageSource.getMessage(messageCode, null, messageCode, response.getLocale());
//        return message;
//    }
//
//    /**
//     * This function validate the accepted IP addess of the client
//     *
//     * @throws IOException
//     */
//    public Boolean checkIPPermission() {
//
//        String remoteAddr = "";
//
//        if (request != null) {
//            remoteAddr = request.getHeader("X-FORWARDED-FOR");
//            if (remoteAddr == null || "".equals(remoteAddr)) {
//                remoteAddr = request.getRemoteAddr();
//            }
//        }
//
//        log.info("Client IP Address: " + remoteAddr);
//
//        Boolean isAuthenticated = Boolean.FALSE;
//        String permittedClientIp = ApplicationPropertiesUtility.getInstance().getStringValue("client-ip-addess-config.properties", "client_ip_adress");
//
//        if (StringUtils.isBlank(permittedClientIp)) {
//            log.info(String.format("Client IP Address [%s] is Permitted to access this system ", remoteAddr));
//            isAuthenticated = Boolean.TRUE;
//        } else {
//
//            String[] permittedClientIpList = StringUtils.split(permittedClientIp, ",", -1);
//
//            for (String aClient : permittedClientIpList) {
//                if (StringUtils.equals(aClient, remoteAddr)) {
//                    log.info(String.format("Client IP Address [%s] is Permitted to access this system ", remoteAddr));
//                    isAuthenticated = Boolean.TRUE;
//                    break;
//                }
//
//            }
//        }
//
//        return isAuthenticated;
//    }
//
//    /**
//     * This function validate the accepted IP addess of the client
//     *
//     * @throws IOException
//     */
//    public Object getClientIp() throws IOException {
//        String remoteAddr = "";
//
//        if (request != null) {
//            remoteAddr = request.getHeader("X-FORWARDED-FOR");
//            if (remoteAddr == null || "".equals(remoteAddr)) {
//                remoteAddr = request.getRemoteAddr();
//            }
//        }
//
//        log.info("Client IP Address: " + remoteAddr);
//
//        Boolean isAuthenticated = Boolean.FALSE;
//        String permittedClientIp = ApplicationPropertiesUtility.getInstance().getStringValue("client-ip-addess-config.properties", "client_ip_adress");
//
//        if (StringUtils.isBlank(permittedClientIp)) {
//            log.info(String.format("Client IP Address [%s] is Permitted to access this system ", remoteAddr));
//            isAuthenticated = Boolean.TRUE;
//        } else {
//
//            String[] permittedClientIpList = StringUtils.split(permittedClientIp, ",", -1);
//
//            for (String aClient : permittedClientIpList) {
//                if (StringUtils.equals(aClient, remoteAddr)) {
//                    log.info(String.format("Client IP Address [%s] is Permitted to access this system ", remoteAddr));
//                    isAuthenticated = Boolean.TRUE;
//                    break;
//                }
//
//            }
//        }
//
//        if (!isAuthenticated) {
//            log.info(String.format("Client IP Address [%s] is not Permitted to access this system ", remoteAddr));
//            response.sendRedirect("/foodllee/access_permission_denied.html");
//            //return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
//        }
//
//        return isAuthenticated;
//    }
//
//    public String getApplicationPath() {
//        String imagePath = request.getScheme() + "://" +   // "http" + "://
//                request.getServerName() +       // "localhost"
//                ":" + request.getServerPort() + // ":" + "8080"
//                request.getContextPath();      // "/people"
//        return imagePath;
//    }
//
//}
