package com.sandesh360.websystem.webapp.controller;

import com.sandesh360.websystem.entity.BaseEntity;
import org.hibernate.validator.constraints.NotBlank;

@SuppressWarnings("serial")
public class LoginForm extends BaseEntity {

    /**
     * User name
     */
    @NotBlank
    private String userName;

    /**
     * User password
     */
    @NotBlank
    private String userPassword;

    /**
     * Admin ID
     */
    private Integer adminId;

    /**
     * Admin Name
     */
    private String adminName;

    /**
     * Admin Email
     */
    private String adminEmail;

    /**
     * Admin Phone
     */
    private String adminPhone;

    /**
     * Admin Address
     */
    private String adminAddress;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getAdminName() {
        return adminName;
    }

    public void setAdminName(String adminName) {
        this.adminName = adminName;
    }

    public String getAdminEmail() {
        return adminEmail;
    }

    public void setAdminEmail(String adminEmail) {
        this.adminEmail = adminEmail;
    }

    public String getAdminAddress() {
        return adminAddress;
    }

    public void setAdminAddress(String adminAddress) {
        this.adminAddress = adminAddress;
    }

    public Integer getAdminId() {
        return adminId;
    }

    public void setAdminId(Integer adminId) {
        this.adminId = adminId;
    }

    public String getAdminPhone() {
        return adminPhone;
    }

    public void setAdminPhone(String adminPhone) {
        this.adminPhone = adminPhone;
    }

    public AdminForm getAdminForm() {
        AdminForm adminForm = new AdminForm();
        adminForm.setId(this.getAdminId());
        adminForm.setName(this.getAdminName());
        adminForm.setUserName(this.getUserName());
        adminForm.setPassword(this.getUserPassword());
        adminForm.setPhone(this.getAdminPhone());
        adminForm.setEmail(this.getAdminEmail());
        adminForm.setAddress(this.getAdminAddress());
        return adminForm;
    }

}
