//package com.sandesh360.websystem.webapp.controller;
//
//import com.sandesh360.websystem.common.*;
//import com.sandesh360.websystem.entity.Admin;
//import com.sandesh360.websystem.response.AdminRequestBody;
//import com.sandesh360.websystem.response.CategoryRequestBody;
//import com.sandesh360.websystem.response.ContentRequestBody;
//import com.sandesh360.websystem.response.PreferenceRequestBody;
//import com.sandesh360.websystem.service.AdminService;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.util.StringUtils;
//import org.springframework.validation.BindingResult;
//import org.springframework.web.bind.annotation.ModelAttribute;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.servlet.ModelAndView;
//import org.springframework.web.servlet.config.annotation.EnableWebMvc;
//
//import javax.servlet.http.Cookie;
//import javax.servlet.http.HttpServletResponse;
//import javax.validation.Valid;
//
//
///**
// * Handle admin web page requests
// *
// * @author Sandesh360 Ltd.
// */
//@Controller
//public class AdminWebController extends BaseController {
//
//    private static final Logger log = LoggerFactory.getLogger(AdminWebController.class);
//
//    @Autowired
//    private ErrorPageController errorPageController;
//
//    //	ModelAndView class declaration for whole class
//    ModelAndView aModelAndView;
//
//    @Autowired
//    AdminService adminService;
//
//
//    /**
//     * Landing page
//     *
//     * @return
//     */
//    @RequestMapping(value = JspPageUtilities.ADMIN_LOGIN_PAGE, method = RequestMethod.GET)
//    public ModelAndView indexPage() {
//        log.info("AdminController: indexPage invoked");
//        Boolean isPermitted;
////        isPermitted = checkIPPermission();
////        if (!isPermitted) {
////            log.info(String.format("Client IP Address is not Permitted to access this system "));
////            return errorPageController.openErrorPage_Permission_denied();
////        }
//
//        log.info("AdminController: indexPage Load the login page");
//        ModelAndView aModelAndView = new ModelAndView(JspPageUtilities.ADMIN_LOGIN_PAGE);
//        return aModelAndView;
//    }
//
//    /**
//     * Registration page
//     *
//     * @return
//     */
//    @RequestMapping(value = JspPageUtilities.ADMIN_REGISTRATION_PAGE, method = RequestMethod.GET)
//    public ModelAndView loadRegistrationPage() {
//        log.info("AdminController: registration invoked");
//        Boolean isPermitted;
////        isPermitted = checkIPPermission();
////        if (!isPermitted) {
////            log.info(String.format("Client IP Address is not Permitted to access this system "));
////            return errorPageController.openErrorPage_Permission_denied();
////        }
//
//        log.info("AdminController: indexPage Load the registration page");
//        ModelAndView aModelAndView = new ModelAndView(JspPageUtilities.ADMIN_REGISTRATION_PAGE);
//        return aModelAndView;
//    }
//
//
//    /**
//     * Registration page
//     *
//     * @return
//     */
//    @RequestMapping(value = JspPageUtilities.ADMIN_REGISTRATION_PAGE, method = RequestMethod.POST)
//    public ModelAndView registration(@ModelAttribute("admin") @Valid Admin admin,
//                                     BindingResult bindingResult, HttpServletResponse response) {
//        log.info("AdminController: registration invoked");
////        Boolean isPermitted;
////        isPermitted = checkIPPermission();
////        if (!isPermitted) {
////            log.info(String.format("Client IP Address is not Permitted to access this system "));
////            return errorPageController.openErrorPage_Permission_denied();
////        }
//
//        try {
//            if (bindingResult.hasErrors()) {
//                log.error("AdminController: adminLoginController -> Login form binding result error occurs");
//                aModelAndView = new ModelAndView(JspPageUtilities.ADMIN_REGISTRATION_PAGE);
//                return aModelAndView;
//            }
//
//            if (admin == null) {
//                log.error("AdminController: admin registration -> no content found");
//                aModelAndView = new ModelAndView(JspPageUtilities.ADMIN_REGISTRATION_PAGE);
//                return aModelAndView;
//            }
//
//            Admin responseAdmin = adminService.insert(admin);
//            if (responseAdmin == null) {
//                log.error("AdminController: admin registration ->  registration failed");
//                aModelAndView = new ModelAndView(JspPageUtilities.ADMIN_REGISTRATION_PAGE);
//                return aModelAndView;
//            }
//
//            log.info("AdminController: indexPage Load the registration page");
//            ModelAndView aModelAndView = new ModelAndView(JspPageUtilities.ADMIN_LOGIN_PAGE);
//            return aModelAndView;
//        } catch (Exception e1) {
//            e1.printStackTrace();
//            ModelAndView aModelAndView = new ModelAndView(JspPageUtilities.ADMIN_REGISTRATION_PAGE);
//            return aModelAndView;
//        }
//    }
//
//    /**
//     * Landing page
//     *
//     * @return
//     */
//    @RequestMapping(value = JspPageUtilities.FORGET_PASSWORD_PAGE, method = RequestMethod.GET)
//    public ModelAndView loadForgetPassword() {
//        log.info("AdminController: indexPage invoked");
//        Boolean isPermitted;
////        isPermitted = checkIPPermission();
////        if (!isPermitted) {
////            log.info(String.format("Client IP Address is not Permitted to access this system "));
////            return errorPageController.openErrorPage_Permission_denied();
////        }
//
//        log.info("AdminController: indexPage Load the login page");
//        ModelAndView aModelAndView = new ModelAndView(JspPageUtilities.FORGET_PASSWORD_PAGE);
//        return aModelAndView;
//    }
//
//    /**
//     * Login Method for admin users
//     *
//     * @return
//     */
//    @RequestMapping(value = JspPageUtilities.ADMIN_LOGIN_PAGE, method = RequestMethod.POST)
//    public ModelAndView adminLoginController(@ModelAttribute("loginForm") @Valid LoginForm loginForm,
//                                             BindingResult bindingResult, HttpServletResponse response) {
//        log.info("AdminController: indexPage invoked");
//        Boolean isPermitted;
//        isPermitted = checkIPPermission();
//        if (!isPermitted) {
//            log.info(String.format("AdminController: adminLoginController ->Client IP Address is not Permitted to access this system "));
//            return errorPageController.openErrorPage_Permission_denied();
//        }
//
//        try {
//
//            if (bindingResult.hasErrors()) {
//                log.error("AdminController: adminLoginController -> Login form binding result error occurs");
//                aModelAndView = new ModelAndView(JspPageUtilities.ADMIN_LOGIN_PAGE);
//                return aModelAndView;
//            }
//
//            Admin responseAdmin = adminService.login(loginForm.getUserName(), loginForm.getUserPassword());
//
//            if (responseAdmin == null) {
//                log.error("AdminController: adminLoginController -> Login failed");
//                aModelAndView = new ModelAndView(JspPageUtilities.ADMIN_LOGIN_PAGE);
//                aModelAndView.addObject(ErrorStatus.USER_NAME_OR_PASSWORD_WRONG.name());
//                return aModelAndView;
//            }
//
//            // Setting staff information in session
//            request.getSession().setAttribute(getSessionKey(responseAdmin.getId().toString()), responseAdmin);
//            //staffForm = (StaffForm) request.getSession().getAttribute(getSessionKey(staffDto.getStaffKey().getStaffID()));
//
//            log.info("LoginController: authentication -> Generating cookie value");
//            // Setting the cookie attribute
//            Cookie adminCookie = new Cookie("adminId", responseAdmin.getId().toString());
//            // Development
//            {
//                // Penetration test Need to set this always secured.
//                adminCookie.setSecure(request.isSecure());
//                // It Cookie Expired Time in Second
//                adminCookie.setMaxAge(60 * ApplicationPropertiesUtility.getInstance().getIntegerValue(PropertyConstants.PROPERTY_ADMIN_COOKIE_EXPIRE_INTERVAL));
//                response.addCookie(adminCookie);
//            }
//
//            log.info("AdminController: adminLoginController -> indexPage Load the login page");
//            ModelAndView aModelAndView = new ModelAndView(JspPageUtilities.HOME_PAGE);
//            aModelAndView.addObject("loginForm", responseAdmin);
//            return aModelAndView;
//
//        } catch (Exception e) {
//            e.printStackTrace();
//            aModelAndView = new ModelAndView(JspPageUtilities.ADMIN_LOGIN_PAGE);
//            aModelAndView.addObject("verificationErronMessage", getResourceMessage(e.getMessage()));
//            return aModelAndView;
//        }
//
//    }
//
//    /**
//     * Dashboard Method for admin users
//     *
//     * @return
//     */
//    @RequestMapping(value = JspPageUtilities.HOME_PAGE, method = RequestMethod.GET)
//    public ModelAndView adminDashboardController() {
//        log.info("AdminController: indexPage invoked");
//        Boolean isPermitted;
//        isPermitted = checkIPPermission();
//        if (!isPermitted) {
//            log.info(String.format("AdminController: adminDashboardController ->Client IP Address is not Permitted to access this system "));
//            return errorPageController.openErrorPage_Permission_denied();
//        }
//
//        try {
//
////            Admin loginForm = validateAdminLogin();
////            if (loginForm == null) {
////                ModelAndView aModelAndView = new ModelAndView(JspPageUtilities.ADMIN_LOGIN_PAGE);
////                return aModelAndView;
////            }
//
//            log.info("AdminController: adminDashboardController -> indexPage Load the login page");
//            ModelAndView aModelAndView = new ModelAndView(JspPageUtilities.HOME_PAGE);
//            //  aModelAndView.addObject("loginForm", loginForm);
//            return aModelAndView;
//
//        } catch (Exception e) {
//            e.printStackTrace();
//            aModelAndView = new ModelAndView(JspPageUtilities.ADMIN_LOGIN_PAGE);
//            aModelAndView.addObject("verificationErronMessage", getResourceMessage(e.getMessage()));
//            return aModelAndView;
//        }
//
//    }
//
//    /**
//     * Logout function web
//     *
//     * @return object of ModelAndView
//     */
//    @RequestMapping(value = JspPageUtilities.LOGOUT_PAGE, method = RequestMethod.GET)
//    public ModelAndView logoutControl() {
//        Cookie[] cookies = request.getCookies();
//        for (Cookie cookie : cookies) {
//            if ("adminId".equals(cookie.getName())) {
//                request.getSession().removeAttribute(getSessionKey(cookie.getValue()));
//            }
//        }
//        ModelAndView aModelAndView = new ModelAndView(JspPageUtilities.ADMIN_LOGIN_PAGE);
//        return aModelAndView;
//    }
//
//    /**
//     * Profile page
//     *
//     * @return
//     */
//    @RequestMapping(value = JspPageUtilities.ADMIN_PROFILE_PAGE, method = RequestMethod.GET)
//    public ModelAndView profileUpdatePage() {
//        log.info("AdminController: profilePage GET invoked");
//        Boolean isPermitted;
//        isPermitted = checkIPPermission();
//        if (!isPermitted) {
//            log.info(String.format("Client IP Address is not Permitted to access this system "));
//            return errorPageController.openErrorPage_Permission_denied();
//        }
//
//        LoginForm loginForm = validateLogin();
//        if (loginForm == null) {
//            ModelAndView aModelAndView = new ModelAndView(JspPageUtilities.ADMIN_LOGIN_PAGE);
//            return aModelAndView;
//        }
//
//        log.info("AdminController: profilePage Load the profile page");
//        ModelAndView aModelAndView = new ModelAndView(JspPageUtilities.ADMIN_PROFILE_PAGE);
//        aModelAndView.addObject("loginForm", loginForm);
//
//        AdminForm adminForm = loginForm.getAdminForm();
//        aModelAndView.addObject("adminForm", adminForm);
//
//        return aModelAndView;
//    }
//
//
//    /**
//     * Landing page
//     *
//     * @return
//     */
//    @RequestMapping(value = JspPageUtilities.ADMIN_PROFILE_PAGE, method = RequestMethod.POST)
//    public ModelAndView profileUpdatePage(@Valid AdminForm adminForm, BindingResult result) {
//        log.info("AdminController: profilePage POST invoked");
//        Boolean isPermitted;
//        isPermitted = checkIPPermission();
//        if (!isPermitted) {
//            log.info(String.format("Client IP Address is not Permitted to access this system "));
//            return errorPageController.openErrorPage_Permission_denied();
//        }
//
//        LoginForm loginForm = validateLogin();
//        if (loginForm == null) {
//            ModelAndView aModelAndView = new ModelAndView(JspPageUtilities.ADMIN_LOGIN_PAGE);
//            return aModelAndView;
//        }
//
//        log.info("AdminController: profilePage Load the profile page");
//        ModelAndView aModelAndView = new ModelAndView(JspPageUtilities.ADMIN_LOGIN_PAGE);
//        aModelAndView.addObject("loginForm", loginForm);
//        aModelAndView.addObject("adminForm", adminForm);
//
//        if (result.hasErrors()) {
//            log.error("TransactionController:: Fail to show the transaction history due to error");
//            return aModelAndView;
//        }
//
//        if (!adminForm.getPassword().equals(adminForm.getAdminRetypePassword())) {
//            aModelAndView.addObject("errMessage", getResourceMessage("err.changepassword.new.confirm.new.not.match"));
//            return aModelAndView;
//        }
//
//        try {
//            Admin adminDTO = adminForm.getAdminDto();
//            if (StringUtils.hasLength(adminDTO.getPassword()))
//                adminDTO.setPassword(Utility.encryptStringSha1(adminDTO.getPassword()));
//            adminDTO.setDelete(Constants.DELETE_FLAG_NO);
//            adminDTO.setUpdatedBy(adminDTO.getId().toString());
//            adminDTO.setUpdatedAt(Utility.getCurrentDateTime());
//
//            adminService.update(adminDTO);
//            //adminService.doModify(adminDTO);
//
//            aModelAndView.addObject("successMessage", "Successfully Updated");
//            return aModelAndView;
//        } catch (Exception e) {
//            log.error("AdminController:: Gettimg profile edit exception");
//            log.error("AdminController:: ErrorResponse Message ->" + e.getMessage());
//            aModelAndView.addObject("errMessage", "Updated Failed");
//            return aModelAndView;
//        }
//    }
//
//    /**
//     * Registration page
//     *
//     * @return
//     */
//    @RequestMapping(value = JspPageUtilities.CONTENT_PAGE, method = RequestMethod.GET)
//    public ModelAndView loadContentPage() {
//        log.info("AdminController: registration invoked");
//        Boolean isPermitted;
////        isPermitted = checkIPPermission();
////        if (!isPermitted) {
////            log.info(String.format("Client IP Address is not Permitted to access this system "));
////            return errorPageController.openErrorPage_Permission_denied();
////        }
//
//        log.info("AdminController: indexPage Load the registration page");
//        ModelAndView aModelAndView = new ModelAndView(JspPageUtilities.CONTENT_PAGE);
//        return aModelAndView;
//    }
//
//    /**
//     * Registration page
//     *
//     * @return
//     */
//    @RequestMapping(value = JspPageUtilities.CONTENT_PAGE, method = RequestMethod.POST)
//    public ModelAndView addContent(@ModelAttribute("admin") @Valid ContentRequestBody admin,
//                                   BindingResult bindingResult, HttpServletResponse response) {
//        log.info("AdminController: registration invoked");
////        Boolean isPermitted;
////        isPermitted = checkIPPermission();
////        if (!isPermitted) {
////            log.info(String.format("Client IP Address is not Permitted to access this system "));
////            return errorPageController.openErrorPage_Permission_denied();
////        }
//
//        log.info("AdminController: indexPage Load the registration page");
//        ModelAndView aModelAndView = new ModelAndView(JspPageUtilities.ADMIN_LOGIN_PAGE);
//        return aModelAndView;
//    }
//
//    /**
//     * Registration page
//     *
//     * @return
//     */
//    @RequestMapping(value = JspPageUtilities.CATEGORY_PAGE, method = RequestMethod.GET)
//    public ModelAndView loadCategoryPage() {
//        log.info("AdminController: registration invoked");
//        Boolean isPermitted;
////        isPermitted = checkIPPermission();
////        if (!isPermitted) {
////            log.info(String.format("Client IP Address is not Permitted to access this system "));
////            return errorPageController.openErrorPage_Permission_denied();
////        }
//
//        log.info("AdminController: indexPage Load the registration page");
//        ModelAndView aModelAndView = new ModelAndView(JspPageUtilities.CATEGORY_PAGE);
//        return aModelAndView;
//    }
//
//    /**
//     * Registration page
//     *
//     * @return
//     */
//    @RequestMapping(value = JspPageUtilities.CATEGORY_PAGE, method = RequestMethod.POST)
//    public ModelAndView addCategory(@ModelAttribute("category") @Valid CategoryRequestBody category,
//                                    BindingResult bindingResult, HttpServletResponse response) {
//        log.info("AdminController: registration invoked");
////        Boolean isPermitted;
////        isPermitted = checkIPPermission();
////        if (!isPermitted) {
////            log.info(String.format("Client IP Address is not Permitted to access this system "));
////            return errorPageController.openErrorPage_Permission_denied();
////        }
//
//        log.info("AdminController: indexPage Load the registration page");
//        ModelAndView aModelAndView = new ModelAndView(JspPageUtilities.ADMIN_LOGIN_PAGE);
//        return aModelAndView;
//    }
//
//    /**
//     * Registration page
//     *
//     * @return
//     */
//    @RequestMapping(value = JspPageUtilities.PREFERENCE_PAGE, method = RequestMethod.GET)
//    public ModelAndView loadPreferencePage() {
//        log.info("AdminController: registration invoked");
//        Boolean isPermitted;
////        isPermitted = checkIPPermission();
////        if (!isPermitted) {
////            log.info(String.format("Client IP Address is not Permitted to access this system "));
////            return errorPageController.openErrorPage_Permission_denied();
////        }
//
//        log.info("AdminController: indexPage Load the registration page");
//        ModelAndView aModelAndView = new ModelAndView(JspPageUtilities.PREFERENCE_PAGE);
//        return aModelAndView;
//    }
//
//    /**
//     * Registration page
//     *
//     * @return
//     */
//    @RequestMapping(value = JspPageUtilities.PREFERENCE_PAGE, method = RequestMethod.POST)
//    public ModelAndView addPreference(@ModelAttribute("preference") @Valid PreferenceRequestBody preference,
//                                      BindingResult bindingResult, HttpServletResponse response) {
//        log.info("AdminController: registration invoked");
////        Boolean isPermitted;
////        isPermitted = checkIPPermission();
////        if (!isPermitted) {
////            log.info(String.format("Client IP Address is not Permitted to access this system "));
////            return errorPageController.openErrorPage_Permission_denied();
////        }
//
//        log.info("AdminController: indexPage Load the registration page");
//        ModelAndView aModelAndView = new ModelAndView(JspPageUtilities.ADMIN_LOGIN_PAGE);
//        return aModelAndView;
//    }
//
//
//}
