package com.sandesh360.websystem.webapp.controller;

import com.sandesh360.websystem.entity.Admin;
import org.hibernate.validator.constraints.NotBlank;

import java.math.BigInteger;

public class AdminForm {

    /**
     * Admin ID
     */
    private Integer id;

    /**
     * Admin Password
     */
    private String password;

    /**
     * Admin Retype Password
     */
    private String adminRetypePassword;

    /**
     * Admin Username
     */
    @NotBlank
    private String userName;

    /**
     * Admin Name
     */
    @NotBlank
    private String name;

    /**
     * Admin phone
     */
    @NotBlank
    private String phone;

    /**
     * Admin Email
     */
    @NotBlank
    private String email;

    /**
     * Admin address
     */
    @NotBlank
    private String address;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAdminRetypePassword() {
        return adminRetypePassword;
    }

    public void setAdminRetypePassword(String adminRetypePassword) {
        this.adminRetypePassword = adminRetypePassword;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Admin getAdminDto() {
        Admin adminDTO = new Admin();
        adminDTO.setId(this.getId());
        adminDTO.setName(this.getName());
        adminDTO.setUserName(this.getUserName());
        adminDTO.setPassword(this.getPassword());
        adminDTO.setPhone(this.getPhone());
        adminDTO.setEmail(this.getEmail());
        adminDTO.setAddress(this.getAddress());
        return adminDTO;
    }

}
