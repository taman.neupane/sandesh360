//package com.sandesh360.websystem.webapp.controller;
//
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.servlet.ModelAndView;
//
//@Controller
//public class ErrorPageController extends BaseController {
//
//    @RequestMapping(value = "/general_error")
//    public ModelAndView openErrorPage() {
//        ModelAndView aModelAndView = new ModelAndView("");
//        aModelAndView.addObject("errMessage", "");
//        return aModelAndView;
//    }
//
//    @RequestMapping(value = "/general_error_500")
//    public ModelAndView openErrorPage_505() {
//        //ModelAndView aModelAndView = new ModelAndView(UrlUtilities.ERROR_PAGE);
//        //aModelAndView.addObject("errMessage", getResourceMessage("Server.general.error.505"));
//        ModelAndView aModelAndView = new ModelAndView("");
//        aModelAndView.addObject("errMessage", "");
//        return aModelAndView;
//    }
//
//    @RequestMapping(value = "/access_permission_denied")
//    public ModelAndView openErrorPage_Permission_denied() {
//        ModelAndView aModelAndView = new ModelAndView("");
//        aModelAndView.addObject("errMessage", "");
//        return aModelAndView;
//    }
//
//
//}
