package com.sandesh360.websystem;

import com.sandesh360.websystem.entity.*;
import com.sandesh360.websystem.property.FileStorageProperties;
import org.apache.ibatis.type.MappedTypes;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

@MappedTypes({
        Language.class,
        Preference.class,
        Category.class,
        Content.class,
        ContentPreference.class,
        ContentCategory.class,
        Image.class,
        Feedback.class,
        Questions.class,
        Poll.class,
        User.class,
        Bookmarks.class,
        Admin.class,
        AdminAuthToken.class


})
@MapperScan("com.sandesh360.websystem.mapper_dao")

@EnableConfigurationProperties({
        FileStorageProperties.class
})
@SpringBootApplication
@ComponentScan("com.sandesh360.websystem")
@EnableAutoConfiguration
public class Sandesh360Application extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(Sandesh360Application.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(Sandesh360Application.class, args);
    }
}
