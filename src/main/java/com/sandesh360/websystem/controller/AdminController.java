package com.sandesh360.websystem.controller;

import com.sandesh360.websystem.common.ErrorStatus;
import com.sandesh360.websystem.entity.Admin;
import com.sandesh360.websystem.entity.AdminAuthToken;
import com.sandesh360.websystem.response.*;
import com.sandesh360.websystem.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/admin")
public class AdminController {

    @Autowired
    private AdminService service;

    @Autowired
    private AdminAuthService authService;

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Response addNewAdmin(@RequestBody AdminRequestBody body) {

        try {
            if (body == null || body.getAdmin() == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.NO_CONTENT, HttpStatus.NO_CONTENT);
            }

            Admin admin = body.getAdmin();

            if (!StringUtils.hasLength(admin.getUserName())) {
                return ErrorResponse.getErrorResponse(ErrorStatus.USER_NAME_REQUIRED, HttpStatus.BAD_REQUEST);
            } else if (!StringUtils.hasLength(admin.getPassword())) {
                return ErrorResponse.getErrorResponse(ErrorStatus.PASSWORD_REQUIRED, HttpStatus.BAD_REQUEST);
            } else if (!StringUtils.hasLength(admin.getName())) {
                return ErrorResponse.getErrorResponse(ErrorStatus.NAME_REQUIRED, HttpStatus.BAD_REQUEST);
            } else if (!StringUtils.hasLength(admin.getEmail())) {
                return ErrorResponse.getErrorResponse(ErrorStatus.EMAIL_REQUIRED, HttpStatus.BAD_REQUEST);
            } else if (!StringUtils.hasLength(admin.getPhone())) {
                return ErrorResponse.getErrorResponse(ErrorStatus.PHONE_NO_REQUIRED, HttpStatus.BAD_REQUEST);
            } else if (!StringUtils.hasLength(admin.getAddress())) {
                return ErrorResponse.getErrorResponse(ErrorStatus.ADDRESS_REQUIRED, HttpStatus.BAD_REQUEST);

            }

            admin = service.insert(admin);
            AdminAuthToken token = null;
            if (admin != null) {
                token = new AdminAuthToken()
                        .setAdminId(admin.getId());
                token = authService.insert(token);
            }

            if (token == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.TOKEN_NOT_GENERATED, HttpStatus.INTERNAL_SERVER_ERROR);
            }

            return new AdminResponseBody()
                    .setAdmin(new Admin()
                            .setId(admin.getId())
                            .setName(admin.getName())
                            .setAuthToken(token.getToken())
                    ).setStatus(HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ErrorResponse.getErrorResponse(ErrorStatus.UNKNOWN_ERROR, HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/edit", method = RequestMethod.PUT)
    public Response editAdmin(@RequestBody AdminRequestBody body) {

        try {
            if (body == null || body.getAdmin() == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.NO_CONTENT, HttpStatus.NO_CONTENT);
            } else if (!StringUtils.hasLength(body.getToken())) {
                return ErrorResponse.getErrorResponse(ErrorStatus.TOKEN_REQUIRED, HttpStatus.BAD_REQUEST);
            }

            Admin admin = body.getAdmin();

            if (admin.getId() == null || admin.getId() <= 0) {
                return ErrorResponse.getErrorResponse(ErrorStatus.ID_REQUIRED, HttpStatus.BAD_REQUEST);
            } else if (!StringUtils.hasLength(admin.getName())) {
                return ErrorResponse.getErrorResponse(ErrorStatus.NAME_REQUIRED, HttpStatus.BAD_REQUEST);
            } else if (!StringUtils.hasLength(admin.getEmail())) {
                return ErrorResponse.getErrorResponse(ErrorStatus.EMAIL_REQUIRED, HttpStatus.BAD_REQUEST);
            } else if (!StringUtils.hasLength(admin.getPhone())) {
                return ErrorResponse.getErrorResponse(ErrorStatus.PHONE_NO_REQUIRED, HttpStatus.BAD_REQUEST);
            } else if (!StringUtils.hasLength(admin.getAddress())) {
                return ErrorResponse.getErrorResponse(ErrorStatus.ADDRESS_REQUIRED, HttpStatus.BAD_REQUEST);
            }


            AdminAuthToken token = authService.findById(admin.getId());
            if (token == null || token.getToken() == null
                    || !token.getToken().equals(body.getToken())) {
                return ErrorResponse.getErrorResponse(ErrorStatus.INVALID_TOKEN, HttpStatus.BAD_REQUEST);
            }


            admin = service.update(admin);
            return new AdminResponseBody()
                    .setAdmin(admin).setStatus(HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ErrorResponse.getErrorResponse(ErrorStatus.UNKNOWN_ERROR, HttpStatus.BAD_REQUEST);
    }

    //    @RequestMapping(value = "/details", method = RequestMethod.GET, headers = "Authorization")
    @RequestMapping(value = "/details", method = RequestMethod.POST)
    public Response getAdminDetails(@RequestBody AdminRequestBody body) {

        try {
            if (body == null || body.getAdmin() == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.NO_CONTENT, HttpStatus.NO_CONTENT);
            } else if (!StringUtils.hasLength(body.getToken())) {
                return ErrorResponse.getErrorResponse(ErrorStatus.TOKEN_REQUIRED, HttpStatus.BAD_REQUEST);
            }

            Admin admin = body.getAdmin();
            if (admin.getId() == null || admin.getId() <= 0) {
                return ErrorResponse.getErrorResponse(ErrorStatus.ID_REQUIRED, HttpStatus.BAD_REQUEST);
            }

            AdminAuthToken token = authService.findById(admin.getId());
            if (token == null || token.getToken() == null
                    || !token.getToken().equals(body.getToken())) {
                return ErrorResponse.getErrorResponse(ErrorStatus.INVALID_TOKEN, HttpStatus.BAD_REQUEST);
            }

            admin = service.findById(admin.getId());
            return new AdminResponseBody().setAdmin(admin).setStatus(HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return ErrorResponse.getErrorResponse(ErrorStatus.UNKNOWN_ERROR, HttpStatus.BAD_REQUEST);

        }

    }


    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public Response doLogin(@RequestBody AdminRequestBody body) {

        try {
            if (body == null || body.getAdmin() == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.NO_CONTENT, HttpStatus.NO_CONTENT);
            }
            Admin admin = body.getAdmin();
            if (!StringUtils.hasLength(admin.getUserName())) {
                return ErrorResponse.getErrorResponse(ErrorStatus.USER_NAME_REQUIRED, HttpStatus.BAD_REQUEST);

            } else if (!StringUtils.hasLength(admin.getPassword())) {
                return ErrorResponse.getErrorResponse(ErrorStatus.PASSWORD_REQUIRED, HttpStatus.BAD_REQUEST);
            }

            admin = service.login(admin.getUserName(), admin.getPassword());
            if (admin == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.USER_NAME_OR_PASSWORD_WRONG, HttpStatus.BAD_REQUEST);
            }

            AdminAuthToken token = authService.findById(admin.getId());
            if (token == null || token.getToken() == null) {
                token = authService.insert(new AdminAuthToken().setAdminId(admin.getId()));
            }

            admin.setAuthToken(token.getToken());
            return new AdminResponseBody()
                    .setAdmin(new Admin()
                            .setId(admin.getId())
                            .setName(admin.getName())
                            .setAuthToken(token.getToken())
                    ).setStatus(HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return ErrorResponse.getErrorResponse(ErrorStatus.UNKNOWN_ERROR, HttpStatus.BAD_REQUEST);
        }

    }

    @RequestMapping(value = "/change/password", method = RequestMethod.POST)
    public Response changePassword(@RequestBody AdminRequestBody body) {

        try {
            if (body == null || body.getAdmin() == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.NO_CONTENT, HttpStatus.NO_CONTENT);
            } else if (!StringUtils.hasLength(body.getToken())) {
                return ErrorResponse.getErrorResponse(ErrorStatus.TOKEN_REQUIRED, HttpStatus.BAD_REQUEST);
            }

            Admin admin = body.getAdmin();
            if (admin.getId() == null || admin.getId() <= 0) {
                return ErrorResponse.getErrorResponse(ErrorStatus.ID_REQUIRED, HttpStatus.BAD_REQUEST);
            } else if (!StringUtils.hasLength(admin.getPassword())) {
                return ErrorResponse.getErrorResponse(ErrorStatus.PASSWORD_REQUIRED, HttpStatus.BAD_REQUEST);
            } else if (!StringUtils.hasLength(admin.getNewPassword())) {
                return ErrorResponse.getErrorResponse(ErrorStatus.NEW_PASSWORD_REQUIRED, HttpStatus.BAD_REQUEST);

            } else if (!admin.isConfirm()) {
                return ErrorResponse.getErrorResponse(ErrorStatus.NEW_AND_CONFIRM_PASSWORD_DOES_NOT_MATCH, HttpStatus.BAD_REQUEST);
            }

            AdminAuthToken token = authService.findById(admin.getId());
            if (token == null || token.getToken() == null
                    || !token.getToken().equals(body.getToken())) {
                return ErrorResponse.getErrorResponse(ErrorStatus.INVALID_TOKEN, HttpStatus.BAD_REQUEST);
            }

            long index = service.changePassword(admin.getId(), admin.getPassword(), admin.getNewPassword());
            if (index > 0)
                return new AdminResponseBody().setStatus(HttpStatus.OK);
            else
                return ErrorResponse.getErrorResponse(ErrorStatus.PASSWORD_CHANGE_FAILED, HttpStatus.BAD_REQUEST);

        } catch (Exception e) {
            return ErrorResponse.getErrorResponse(ErrorStatus.UNKNOWN_ERROR, HttpStatus.BAD_REQUEST);
        }

    }


}
