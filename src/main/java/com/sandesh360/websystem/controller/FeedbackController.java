package com.sandesh360.websystem.controller;

import com.sandesh360.websystem.common.ErrorStatus;
import com.sandesh360.websystem.entity.AdminAuthToken;
import com.sandesh360.websystem.entity.Feedback;
import com.sandesh360.websystem.response.*;
import com.sandesh360.websystem.service.FeedbackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/feedback")
public class FeedbackController {

    @Autowired
    private FeedbackService service;

    @Autowired
    private AdminAuthService authService;

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Response addFeedback(@RequestBody FeedbackRequestBody body) {
        try {
            if (body == null || body.getFeedback() == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.NO_CONTENT, HttpStatus.NO_CONTENT);
            }

            Feedback feedback = body.getFeedback();
            if (!StringUtils.hasLength(feedback.getFrom())) {
                return ErrorResponse.getErrorResponse(ErrorStatus.EMAIL_REQUIRED, HttpStatus.BAD_REQUEST);
            } else if (!StringUtils.hasLength(feedback.getComment())) {
                return ErrorResponse.getErrorResponse(ErrorStatus.COMMENT_REQUIRED, HttpStatus.BAD_REQUEST);
            }

            service.insert(feedback);
            return new SuccessResponse<SuccessResponse>().setStatus(HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return ErrorResponse.getErrorResponse(ErrorStatus.UNKNOWN_ERROR, HttpStatus.BAD_REQUEST);
        }
    }


    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public Response getAllFeedback(@RequestHeader("Authorization") String token) {
        try {

            if (!StringUtils.hasLength(token)) {
                return ErrorResponse.getErrorResponse(ErrorStatus.TOKEN_REQUIRED, HttpStatus.BAD_REQUEST);
            }

            AdminAuthToken authToken = authService.findByToken(token);
            if (authToken == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.INVALID_TOKEN, HttpStatus.BAD_REQUEST);
            }

            List<Feedback> feedbacks = service.findAll();
            return new FeedbackResponseBody().setFeedbacks(feedbacks).setStatus(HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return ErrorResponse.getErrorResponse(ErrorStatus.UNKNOWN_ERROR, HttpStatus.BAD_REQUEST);
        }

    }

    @RequestMapping(value = "/details/{id}", method = RequestMethod.GET)
    public Response getDetails(@RequestHeader("Authorization") String token, @PathVariable("id") long id) {
        try {

            if (!StringUtils.hasLength(token)) {
                return ErrorResponse.getErrorResponse(ErrorStatus.TOKEN_REQUIRED, HttpStatus.BAD_REQUEST);
            }

            AdminAuthToken authToken = authService.findByToken(token);
            if (authToken == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.INVALID_TOKEN, HttpStatus.BAD_REQUEST);
            }

            if (id < 1) {
                return ErrorResponse.getErrorResponse(ErrorStatus.NOT_FOUND, HttpStatus.NOT_FOUND);
            }

            Feedback feedback = service.findById(id);
            if (feedback == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.NOT_FOUND, HttpStatus.NOT_FOUND);
            }
            return new FeedbackResponseBody().setFeedback(feedback).setStatus(HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return ErrorResponse.getErrorResponse(ErrorStatus.UNKNOWN_ERROR, HttpStatus.BAD_REQUEST);
        }

    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public Response deleteFeedback(@RequestHeader("Authorization") String token, @PathVariable("id") long id) {
        try {

            if (!StringUtils.hasLength(token)) {
                return ErrorResponse.getErrorResponse(ErrorStatus.TOKEN_REQUIRED, HttpStatus.BAD_REQUEST);
            }

            AdminAuthToken authToken = authService.findByToken(token);
            if (authToken == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.INVALID_TOKEN, HttpStatus.BAD_REQUEST);
            }

            if (id <= 0) {
                return ErrorResponse.getErrorResponse(ErrorStatus.NOT_FOUND, HttpStatus.NOT_FOUND);
            }

            service.deleteById(id);
            return new SuccessResponse<SuccessResponse>().setStatus(HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return ErrorResponse.getErrorResponse(ErrorStatus.UNKNOWN_ERROR, HttpStatus.BAD_REQUEST);
        }

    }


}
