package com.sandesh360.websystem.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sandesh360.websystem.common.Constants;
import com.sandesh360.websystem.common.ErrorStatus;
import com.sandesh360.websystem.common.Utility;
import com.sandesh360.websystem.entity.AdminAuthToken;
import com.sandesh360.websystem.entity.Preference;
import com.sandesh360.websystem.response.*;
import com.sandesh360.websystem.service.FileStorageService;
import com.sandesh360.websystem.service.PreferenceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;


@RestController   // This means that this class is a Controller
public class PreferenceController {

    private static final Logger logger = LoggerFactory.getLogger(PreferenceController.class);

    @Autowired
    private PreferenceService service;

    @Autowired
    private FileStorageService fileStorageService;

    @Autowired
    private AdminAuthService authService;

    @RequestMapping(value = "/api/preference/add", method = RequestMethod.POST)
    public Response addNewPreference(@RequestParam("body") String body,
                                     @RequestParam(value = "image", required = false) MultipartFile image) {

        try {
            if (!StringUtils.hasLength(body)) {
                return ErrorResponse.getErrorResponse(ErrorStatus.NO_CONTENT, HttpStatus.NO_CONTENT);
            }


            ObjectMapper objectMapper = new ObjectMapper();
            PreferenceRequestBody reqBody = objectMapper.readValue(body, PreferenceRequestBody.class);

            if (reqBody == null || reqBody.getPreference() == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.DATA_NOT_PARSED, HttpStatus.BAD_REQUEST);
            }

//            if (!StringUtils.hasLength(reqBody.getToken())) {
//                return ErrorResponse.getErrorResponse(ErrorStatus.TOKEN_REQUIRED, HttpStatus.BAD_REQUEST);
//            }
//
//            AdminAuthToken token = authService.findByToken(reqBody.getToken());
//            if (token == null) {
//                return ErrorResponse.getErrorResponse(ErrorStatus.INVALID_TOKEN, HttpStatus.BAD_REQUEST);
//            }

            Preference preference = reqBody.getPreference();
            if (!StringUtils.hasLength(preference.getPreferenceName())) {
                return ErrorResponse.getErrorResponse(ErrorStatus.PREFERENCE_NAME_REQUIRED, HttpStatus.BAD_REQUEST);
            } else if (preference.getLanguageId() <= 0) {
                return ErrorResponse.getErrorResponse(ErrorStatus.LANGUAGE_ID_REQUIRED, HttpStatus.BAD_REQUEST);
            }
            preference = service.insert(preference);
            if (preference == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.INSERT_FAILED, HttpStatus.BAD_REQUEST);
            }

            if (image != null) {
                String fileName = "pref_" + preference.getId() + "." + Utility.getFileExtension(image);
                fileStorageService.storeFile(image, fileName);
                preference.setIconUrl(Constants.PREFERENCE + fileName);
                service.updateIconUrl(preference);
            }

//            String uploadDirPath = ServletUriComponentsBuilder.fromCurrentContextPath()
//                    .path(Preference.class.getSimpleName())
//                    .path(fileName)
//                    .toUriString();
            return new SuccessResponse<SuccessResponse>().setStatus(HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return ErrorResponse.getErrorResponse(ErrorStatus.UNKNOWN_ERROR, HttpStatus.BAD_REQUEST);
        }

    }

    @RequestMapping(value = "/api/preference/edit", method = RequestMethod.PUT)
    public Response editPreference(@RequestParam("body") String body,
                                   @RequestParam(value = "image", required = false) MultipartFile image) {

        try {
            if (!StringUtils.hasLength(body)) {
                return ErrorResponse.getErrorResponse(ErrorStatus.NO_CONTENT, HttpStatus.NO_CONTENT);
            }


            ObjectMapper objectMapper = new ObjectMapper();
            PreferenceRequestBody reqBody = objectMapper.readValue(body, PreferenceRequestBody.class);

            if (reqBody == null || reqBody.getPreference() == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.DATA_NOT_PARSED, HttpStatus.BAD_REQUEST);
            }

            if (!StringUtils.hasLength(reqBody.getToken())) {
                return ErrorResponse.getErrorResponse(ErrorStatus.TOKEN_REQUIRED, HttpStatus.BAD_REQUEST);
            }

            AdminAuthToken token = authService.findByToken(reqBody.getToken());
            if (token == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.INVALID_TOKEN, HttpStatus.BAD_REQUEST);
            }

            Preference preference = reqBody.getPreference();
            if (preference.getId() <= 0) {
                return ErrorResponse.getErrorResponse(ErrorStatus.ID_REQUIRED, HttpStatus.BAD_REQUEST);
            } else if (!StringUtils.hasLength(preference.getPreferenceName())) {
                return ErrorResponse.getErrorResponse(ErrorStatus.PREFERENCE_NAME_REQUIRED, HttpStatus.BAD_REQUEST);
            } else if (preference.getLanguageId() <= 0) {
                return ErrorResponse.getErrorResponse(ErrorStatus.LANGUAGE_ID_REQUIRED, HttpStatus.BAD_REQUEST);
            } else if (preference.isActive() == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.ACTIVE_FLAG_REQUIRED, HttpStatus.BAD_REQUEST);
            } else if (preference.isDelete() == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.DELETE_FLAG_REQUIRED, HttpStatus.BAD_REQUEST);
            }

            preference = service.update(preference);
            if (preference == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.UPDATE_FAILED, HttpStatus.BAD_REQUEST);
            }

            if (image != null) {
                String fileName = "pref_" + preference.getId() + "." + Utility.getFileExtension(image);
                fileStorageService.storeFile(image, fileName);
                preference.setIconUrl(Constants.PREFERENCE + fileName);
                service.updateIconUrl(preference);
            }

            return new SuccessResponse<SuccessResponse>().setStatus(HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return ErrorResponse.getErrorResponse(ErrorStatus.UNKNOWN_ERROR, HttpStatus.BAD_REQUEST);
        }

    }

    @RequestMapping(value = "/api/preference/active/status", method = RequestMethod.PUT)
    public Response updateActiveStatus(@RequestBody PreferenceRequestBody body) {
        try {
            if (body == null || body.getPreference() == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.NO_CONTENT, HttpStatus.NO_CONTENT);
            }
            if (!StringUtils.hasLength(body.getToken())) {
                return ErrorResponse.getErrorResponse(ErrorStatus.TOKEN_REQUIRED, HttpStatus.BAD_REQUEST);
            }

            AdminAuthToken token = authService.findByToken(body.getToken());
            if (token == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.INVALID_TOKEN, HttpStatus.BAD_REQUEST);
            }

            Preference preference = body.getPreference();
            if (preference.getId() <= 0) {
                return ErrorResponse.getErrorResponse(ErrorStatus.ID_REQUIRED, HttpStatus.BAD_REQUEST);
            } else if (preference.isActive() == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.ACTIVE_FLAG_REQUIRED, HttpStatus.BAD_REQUEST);
            }

            service.updateActiveStatus(preference);
            return new SuccessResponse<SuccessResponse>().setStatus(HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return ErrorResponse.getErrorResponse(ErrorStatus.UNKNOWN_ERROR, HttpStatus.BAD_REQUEST);
        }

    }

    @RequestMapping(value = "/api/preference/delete/status", method = RequestMethod.PUT)
    public Response updateDeleteStatus(@RequestBody PreferenceRequestBody body) {
        try {
            if (body == null || body.getPreference() == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.NO_CONTENT, HttpStatus.NO_CONTENT);
            }
            if (!StringUtils.hasLength(body.getToken())) {
                return ErrorResponse.getErrorResponse(ErrorStatus.TOKEN_REQUIRED, HttpStatus.BAD_REQUEST);
            }

            AdminAuthToken token = authService.findByToken(body.getToken());
            if (token == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.INVALID_TOKEN, HttpStatus.BAD_REQUEST);
            }

            Preference preference = body.getPreference();
            if (preference.getId() <= 0) {
                return ErrorResponse.getErrorResponse(ErrorStatus.ID_REQUIRED, HttpStatus.BAD_REQUEST);
            } else if (preference.isDelete() == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.DELETE_FLAG_REQUIRED, HttpStatus.BAD_REQUEST);
            }

            service.updateDeleteStatus(preference);
            return new SuccessResponse<SuccessResponse>().setStatus(HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return ErrorResponse.getErrorResponse(ErrorStatus.UNKNOWN_ERROR, HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/api/preference/details", method = RequestMethod.POST)
    public Response getCategoryDetails(@RequestBody PreferenceRequestBody body) {
        try {
            if (body == null || body.getPreference() == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.NO_CONTENT, HttpStatus.BAD_REQUEST);
            }
            if (!StringUtils.hasLength(body.getToken())) {
                return ErrorResponse.getErrorResponse(ErrorStatus.TOKEN_REQUIRED, HttpStatus.BAD_REQUEST);
            }

            AdminAuthToken token = authService.findByToken(body.getToken());
            if (token == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.INVALID_TOKEN, HttpStatus.BAD_REQUEST);
            }

            Preference preference = body.getPreference();
            if (preference.getId() <= 0) {
                return ErrorResponse.getErrorResponse(ErrorStatus.ID_REQUIRED, HttpStatus.BAD_REQUEST);
            }

            preference = service.findById(preference.getId());
            return new PreferenceResponseBody().setPreference(preference).setStatus(HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();

            return ErrorResponse.getErrorResponse(ErrorStatus.UNKNOWN_ERROR, HttpStatus.BAD_REQUEST);
        }
    }


    @RequestMapping(value = "/api/preferences/{path}")
    public Response getPreferences(@PathVariable("path") String path) {

        try {
            List<Preference> list = null;
            if (StringUtils.hasLength(path) && path.equals(Constants.ACTIVE)) {
                list = service.findActivePreferences();
            } else if (StringUtils.hasLength(path) && path.equals(Constants.ALL)) {
                list = service.findAll();
            }

            return new PreferenceResponseBody().setPreferences(list).setStatus(HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return ErrorResponse.getErrorResponse(ErrorStatus.UNKNOWN_ERROR, HttpStatus.BAD_REQUEST);
        }

    }


    @GetMapping(Constants.PREFERENCE + "{fileName:.+}")
    public ResponseEntity<Resource> getPreferenceIcon(@PathVariable String fileName, HttpServletRequest request) {
        // Load file as Resource
        Resource resource = fileStorageService.loadFileAsResource(fileName);

        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
//            logger.info("Could not determine file type.");
        }

        // Fallback to the default content type if type could not be determined
        if (contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }
}