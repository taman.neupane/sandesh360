package com.sandesh360.websystem.controller;

import com.sandesh360.websystem.common.ErrorStatus;
import com.sandesh360.websystem.entity.Poll;
import com.sandesh360.websystem.model.PollPercentage;
import com.sandesh360.websystem.response.*;
import com.sandesh360.websystem.response.Error;
import com.sandesh360.websystem.service.PollService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.math.BigInteger;

@RestController
@RequestMapping("/api/poll")
public class PollController {

    @Autowired
    private PollService service;

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Response addPoll(@RequestBody PollRequestBody body) {

        try {
            if (body == null || body.getPoll() == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.NO_CONTENT, HttpStatus.NO_CONTENT);
            }

            Poll poll = body.getPoll();

            if (poll.getQuestionId() == null || poll.getQuestionId().compareTo(BigInteger.ZERO) != 1) {
                return ErrorResponse.getErrorResponse(ErrorStatus.QUESTION_ID_REQUIRED, HttpStatus.BAD_REQUEST);
            } else if (!StringUtils.hasLength(poll.getVoter())) {
                return ErrorResponse.getErrorResponse(ErrorStatus.VOTER_REQUIRED, HttpStatus.BAD_REQUEST);
            } else if (poll.isYes() == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.VOTE_REQUIRED, HttpStatus.BAD_REQUEST);
            }

            poll = service.insert(poll);
            PollPercentage percentage = service.getPollPercentageByQId(poll.getQuestionId());
            return new PollResponseBody().setPercentage(percentage).setStatus(HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return ErrorResponse.getErrorResponse(ErrorStatus.UNKNOWN_ERROR, HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/{qid}", method = RequestMethod.GET)
    public Response getPollPercentage(@PathVariable("qid") BigInteger qid) {
        try {
            if (qid == null || qid.compareTo(BigInteger.ZERO) != 1) {
                return ErrorResponse.getErrorResponse(ErrorStatus.NOT_FOUND, HttpStatus.BAD_REQUEST);
            }

            PollPercentage percentage = service.getPollPercentageByQId(qid);

            if (percentage == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.NOT_FOUND, HttpStatus.BAD_REQUEST);
            }

            return new PollResponseBody().setPercentage(percentage).setStatus(HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return ErrorResponse.getErrorResponse(ErrorStatus.UNKNOWN_ERROR, HttpStatus.BAD_REQUEST);
        }

    }
}
