package com.sandesh360.websystem.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sandesh360.websystem.common.Constants;
import com.sandesh360.websystem.common.ErrorStatus;
import com.sandesh360.websystem.common.Utility;
import com.sandesh360.websystem.entity.User;
import com.sandesh360.websystem.response.ErrorResponse;
import com.sandesh360.websystem.response.Response;
import com.sandesh360.websystem.response.UserRequestBody;
import com.sandesh360.websystem.response.UserResponseBody;
import com.sandesh360.websystem.service.FileStorageService;
import com.sandesh360.websystem.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@RestController
public class UserController {

    @Autowired
    private UserService service;

    @Autowired
    private FileStorageService storageService;

    @RequestMapping(value = "/api/user/add", method = RequestMethod.POST)
    public Response addUser(@RequestParam("body") String body,
                            @RequestParam(value = "image", required = false) MultipartFile image) {

        try {
            if (!StringUtils.hasLength(body)) {
                return ErrorResponse.getErrorResponse(ErrorStatus.NO_CONTENT, HttpStatus.NO_CONTENT);
            }

            ObjectMapper objectMapper = new ObjectMapper();
            UserRequestBody reqBody = objectMapper.readValue(body, UserRequestBody.class);

            if (reqBody == null && reqBody.getUser() == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.DATA_NOT_PARSED, HttpStatus.BAD_REQUEST);
            }

            User user = reqBody.getUser();

            if (!StringUtils.hasLength(user.getUserName())) {
                return ErrorResponse.getErrorResponse(ErrorStatus.USER_NAME_REQUIRED, HttpStatus.BAD_REQUEST);
            } else if (!StringUtils.hasLength(user.getPassword())) {
                return ErrorResponse.getErrorResponse(ErrorStatus.PASSWORD_REQUIRED, HttpStatus.BAD_REQUEST);
            } else if (!StringUtils.hasLength(user.getName())) {
                return ErrorResponse.getErrorResponse(ErrorStatus.NAME_REQUIRED, HttpStatus.BAD_REQUEST);
            } else if (!StringUtils.hasLength(user.getEmail())) {
                return ErrorResponse.getErrorResponse(ErrorStatus.EMAIL_REQUIRED, HttpStatus.BAD_REQUEST);
            } else if (!StringUtils.hasLength(user.getPhone())) {
                return ErrorResponse.getErrorResponse(ErrorStatus.PHONE_NO_REQUIRED, HttpStatus.BAD_REQUEST);
            } else if (!StringUtils.hasLength(user.getAddress())) {
                return ErrorResponse.getErrorResponse(ErrorStatus.ADDRESS_REQUIRED, HttpStatus.BAD_REQUEST);
            }

            User responseUser = service.insert(user);

            if (image != null) {
                String fileName = "user_" + responseUser.getId() + "." + Utility.getFileExtension(image);
                storageService.storeFile(image, fileName);
                responseUser.setProfilePic(Constants.USER + fileName);
                service.updateProfilePic(responseUser);
            }

            return new UserResponseBody()
                    .setUser(new User()
                            .setId(responseUser.getId())
                            .setName(responseUser.getName())
                            .setProfilePic(responseUser.getProfilePic())
                    ).setStatus(HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ErrorResponse.getErrorResponse(ErrorStatus.UNKNOWN_ERROR, HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/api/user/login", method = RequestMethod.POST)
    public Response userLogin(@RequestBody UserRequestBody body) {

        try {
            if (body == null || body.getUser() == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.NO_CONTENT, HttpStatus.NO_CONTENT);
            }
            User user = body.getUser();
            if (!StringUtils.hasLength(user.getEmail())) {
                return ErrorResponse.getErrorResponse(ErrorStatus.USER_NAME_REQUIRED, HttpStatus.BAD_REQUEST);
            } else if (!StringUtils.hasLength(user.getPassword())) {
                return ErrorResponse.getErrorResponse(ErrorStatus.PASSWORD_REQUIRED, HttpStatus.BAD_REQUEST);
            }

            user = service.login(user);
            if (user == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.USER_NAME_OR_PASSWORD_WRONG, HttpStatus.BAD_REQUEST);

            }

            return new UserResponseBody()
                    .setUser(new User()
                            .setId(user.getId())
                            .setName(user.getName())
                            .setProfilePic(user.getProfilePic())
                    ).setStatus(HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return ErrorResponse.getErrorResponse(ErrorStatus.UNKNOWN_ERROR, HttpStatus.BAD_REQUEST);
        }

    }

    @GetMapping(Constants.USER + "{fileName:.+}")
    public ResponseEntity<Resource> getProfilePic(@PathVariable String fileName, HttpServletRequest request) {
        // Load file as Resource
        Resource resource = storageService.loadFileAsResource(fileName);

        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
//            logger.info("Could not determine file type.");
        }

        // Fallback to the default content type if type could not be determined
        if (contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }
}
