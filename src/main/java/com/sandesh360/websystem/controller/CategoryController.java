package com.sandesh360.websystem.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sandesh360.websystem.common.Constants;
import com.sandesh360.websystem.common.ErrorStatus;
import com.sandesh360.websystem.common.Utility;
import com.sandesh360.websystem.entity.AdminAuthToken;
import com.sandesh360.websystem.entity.Category;
import com.sandesh360.websystem.response.*;
import com.sandesh360.websystem.service.CategoryService;
import com.sandesh360.websystem.service.FileStorageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;


@RestController   // This means that this class is a Controller
public class CategoryController {

    private static final Logger logger = LoggerFactory.getLogger(CategoryController.class);

    @Autowired
    private CategoryService service;

    @Autowired
    private FileStorageService fileStorageService;

    @Autowired
    private AdminAuthService authService;

    @RequestMapping(value = "/api/category/add", method = RequestMethod.POST)
    public Response addCategory(@RequestParam("body") String body,
                                @RequestParam(value = "image", required = false) MultipartFile image) {

        try {
            if (!StringUtils.hasLength(body)) {
                return ErrorResponse.getErrorResponse(ErrorStatus.NO_CONTENT, HttpStatus.NO_CONTENT);
            }


            ObjectMapper objectMapper = new ObjectMapper();
            CategoryRequestBody reqBody = objectMapper.readValue(body, CategoryRequestBody.class);

            if (reqBody == null || reqBody.getCategory() == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.DATA_NOT_PARSED, HttpStatus.BAD_REQUEST);
            }

//            if (!StringUtils.hasLength(reqBody.getToken())) {
//                return ErrorResponse.getErrorResponse(ErrorStatus.TOKEN_REQUIRED, HttpStatus.BAD_REQUEST);
//            }
//
//            AdminAuthToken token = authService.findByToken(reqBody.getToken());
//            if (token == null) {
//                return ErrorResponse.getErrorResponse(ErrorStatus.INVALID_TOKEN, HttpStatus.BAD_REQUEST);
//            }

            Category category = reqBody.getCategory();
            if (!StringUtils.hasLength(category.getCategoryName())) {
                return ErrorResponse.getErrorResponse(ErrorStatus.CATEGORY_NAME_REQUIRED, HttpStatus.BAD_REQUEST);
            } else if (category.getLanguageId() <= 0) {
                return ErrorResponse.getErrorResponse(ErrorStatus.LANGUAGE_ID_REQUIRED, HttpStatus.BAD_REQUEST);
            }

            category = service.insert(category);
            if (category == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.DELETE_FAILED, HttpStatus.BAD_REQUEST);
            }

            if (image != null) {
                String fileName = "cate_" + category.getId() + "." + Utility.getFileExtension(image);
                fileStorageService.storeFile(image, fileName);
                category.setIconUrl(Constants.CATEGORY + fileName);
                service.updateIconUrl(category);
            }

            return new SuccessResponse<SuccessResponse>().setStatus(HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return ErrorResponse.getErrorResponse(ErrorStatus.UNKNOWN_ERROR, HttpStatus.BAD_REQUEST);
        }

    }

    @RequestMapping(value = "/api/category/edit", method = RequestMethod.PUT)
    public Response editCategory(@RequestParam("body") String body,
                                 @RequestParam(value = "image", required = false) MultipartFile image) {

        try {
            if (!StringUtils.hasLength(body)) {
                return ErrorResponse.getErrorResponse(ErrorStatus.NO_CONTENT, HttpStatus.NO_CONTENT);
            }


            ObjectMapper objectMapper = new ObjectMapper();
            CategoryRequestBody reqBody = objectMapper.readValue(body, CategoryRequestBody.class);

            if (reqBody == null || reqBody.getCategory() == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.DATA_NOT_PARSED, HttpStatus.BAD_REQUEST);
            }

            if (!StringUtils.hasLength(reqBody.getToken())) {
                return ErrorResponse.getErrorResponse(ErrorStatus.TOKEN_REQUIRED, HttpStatus.BAD_REQUEST);
            }

            AdminAuthToken token = authService.findByToken(reqBody.getToken());
            if (token == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.INVALID_TOKEN, HttpStatus.BAD_REQUEST);
            }

            Category category = reqBody.getCategory();
            if (category.getId() <= 0) {
                return ErrorResponse.getErrorResponse(ErrorStatus.ID_REQUIRED, HttpStatus.BAD_REQUEST);
            } else if (!StringUtils.hasLength(category.getCategoryName())) {
                return ErrorResponse.getErrorResponse(ErrorStatus.CATEGORY_NAME_REQUIRED, HttpStatus.BAD_REQUEST);
            } else if (category.getLanguageId() <= 0) {
                return ErrorResponse.getErrorResponse(ErrorStatus.LANGUAGE_ID_REQUIRED, HttpStatus.BAD_REQUEST);
            } else if (category.isActive() == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.ACTIVE_FLAG_REQUIRED, HttpStatus.BAD_REQUEST);
            } else if (category.isDelete() == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.DELETE_FLAG_REQUIRED, HttpStatus.BAD_REQUEST);
            }

            category = service.update(category);
            if (category == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.UPDATE_FAILED, HttpStatus.BAD_REQUEST);
            }

            if (image != null) {
                String fileName = "cate_" + category.getId() + "." + Utility.getFileExtension(image);
                fileStorageService.storeFile(image, fileName);
                category.setIconUrl(Constants.CATEGORY + fileName);
                service.updateIconUrl(category);
            }

            return new SuccessResponse<SuccessResponse>().setStatus(HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return ErrorResponse.getErrorResponse(ErrorStatus.UNKNOWN_ERROR, HttpStatus.BAD_REQUEST);
        }

    }

    @RequestMapping(value = "/api/category/active/status", method = RequestMethod.PUT)
    public Response updateActiveStatus(@RequestBody CategoryRequestBody body) {
        try {
            if (body == null || body.getCategory() == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.NO_CONTENT, HttpStatus.NO_CONTENT);
            }
            if (!StringUtils.hasLength(body.getToken())) {
                return ErrorResponse.getErrorResponse(ErrorStatus.TOKEN_REQUIRED, HttpStatus.BAD_REQUEST);
            }

            AdminAuthToken token = authService.findByToken(body.getToken());
            if (token == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.INVALID_TOKEN, HttpStatus.BAD_REQUEST);
            }

            Category category = body.getCategory();
            if (category.getId() <= 0) {
                return ErrorResponse.getErrorResponse(ErrorStatus.ID_REQUIRED, HttpStatus.BAD_REQUEST);
            } else if (category.isActive() == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.ACTIVE_FLAG_REQUIRED, HttpStatus.BAD_REQUEST);
            }

            service.updateActiveStatus(category);
            return new SuccessResponse<SuccessResponse>().setStatus(HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return ErrorResponse.getErrorResponse(ErrorStatus.UNKNOWN_ERROR, HttpStatus.BAD_REQUEST);
        }

    }

    @RequestMapping(value = "/api/category/delete/status", method = RequestMethod.PUT)
    public Response updateDeleteStatus(@RequestBody CategoryRequestBody body) {
        try {
            if (body == null || body.getCategory() == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.NO_CONTENT, HttpStatus.NO_CONTENT);
            }
            if (!StringUtils.hasLength(body.getToken())) {
                return ErrorResponse.getErrorResponse(ErrorStatus.TOKEN_REQUIRED, HttpStatus.BAD_REQUEST);
            }

            AdminAuthToken token = authService.findByToken(body.getToken());
            if (token == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.INVALID_TOKEN, HttpStatus.BAD_REQUEST);
            }

            Category category = body.getCategory();
            if (category.getId() <= 0) {
                return ErrorResponse.getErrorResponse(ErrorStatus.ID_REQUIRED, HttpStatus.BAD_REQUEST);
            } else if (category.isDelete() == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.DELETE_FLAG_REQUIRED, HttpStatus.BAD_REQUEST);
            }

            service.updateDeleteStatus(category);
            return new SuccessResponse<SuccessResponse>().setStatus(HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return ErrorResponse.getErrorResponse(ErrorStatus.UNKNOWN_ERROR, HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/api/category/details", method = RequestMethod.POST)
    public Response getCategoryDetails(@RequestBody CategoryRequestBody body) {
        try {
            if (body == null || body.getCategory() == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.NO_CONTENT, HttpStatus.NO_CONTENT);
            }
            if (!StringUtils.hasLength(body.getToken())) {
                return ErrorResponse.getErrorResponse(ErrorStatus.TOKEN_REQUIRED, HttpStatus.BAD_REQUEST);
            }

            AdminAuthToken token = authService.findByToken(body.getToken());
            if (token == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.INVALID_TOKEN, HttpStatus.BAD_REQUEST);
            }

            Category category = body.getCategory();
            if (category.getId() <= 0) {
                return ErrorResponse.getErrorResponse(ErrorStatus.ID_REQUIRED, HttpStatus.BAD_REQUEST);
            }

            category = service.findById(category.getId());
            return new CategoryResponseBody().setCategory(category).setStatus(HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return ErrorResponse.getErrorResponse(ErrorStatus.UNKNOWN_ERROR, HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/api/categories/{path}", method = RequestMethod.GET)
    public Response getCategories(@PathVariable("path") String path) {

        try {
            List<Category> list = null;
            if (StringUtils.hasLength(path) && path.equals(Constants.ACTIVE)) {
                list = service.findActiveCategories();
            } else if (StringUtils.hasLength(path) && path.equals(Constants.ALL)) {
                list = service.findAll();
            }

            return new CategoryResponseBody().setCategories(list).setStatus(HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return ErrorResponse.getErrorResponse(ErrorStatus.UNKNOWN_ERROR, HttpStatus.BAD_REQUEST);
        }

    }


    @GetMapping(Constants.CATEGORY + "{fileName:.+}")
    public ResponseEntity<Resource> getCategoryIcon(@PathVariable String fileName, HttpServletRequest request) {
        // Load file as Resource
        Resource resource = fileStorageService.loadFileAsResource(fileName);

        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
//            logger.info("Could not determine file type.");
        }

        // Fallback to the default content type if type could not be determined
        if (contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }
}