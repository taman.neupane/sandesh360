package com.sandesh360.websystem.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sandesh360.websystem.common.Constants;
import com.sandesh360.websystem.common.ErrorStatus;
import com.sandesh360.websystem.common.FileType;
import com.sandesh360.websystem.common.Utility;
import com.sandesh360.websystem.entity.*;
import com.sandesh360.websystem.model.ContentExtendedInfo;
import com.sandesh360.websystem.response.*;
import com.sandesh360.websystem.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.math.BigInteger;
import java.util.List;

@RestController
public class ContentController {

    private static final Logger logger = LoggerFactory.getLogger(PreferenceController.class);

    @Autowired
    private ContentService contentService;

    @Autowired
    private ContentPreferenceService preferenceService;

    @Autowired
    private ContentCategoryService categoryService;

    @Autowired
    private FileStorageService storageService;

    @Autowired
    private AdminAuthService authService;

    @Autowired
    private ImageService imageService;


    @RequestMapping(value = "/api/content/add", method = RequestMethod.POST)
    public Response addNewContent(@RequestParam("body") String body,
                                  @RequestParam(value = "files", required = false) List<MultipartFile> files) {
        try {

            if (!StringUtils.hasLength(body)) {
                return ErrorResponse.getErrorResponse(ErrorStatus.NO_CONTENT, HttpStatus.NO_CONTENT);
            }


            ObjectMapper objectMapper = new ObjectMapper();
            ContentRequestBody reqBody = objectMapper.readValue(body, ContentRequestBody.class);

            if (reqBody == null || reqBody.getContent() == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.DATA_NOT_PARSED, HttpStatus.BAD_REQUEST);
            }

//            if (!StringUtils.hasLength(reqBody.getToken())) {
//                return ErrorResponse.getErrorResponse(ErrorStatus.TOKEN_REQUIRED, HttpStatus.BAD_REQUEST);
//            }
//
//            AdminAuthToken token = authService.findByToken(reqBody.getToken());
//            if (token == null) {
//                return ErrorResponse.getErrorResponse(ErrorStatus.INVALID_TOKEN, HttpStatus.BAD_REQUEST);
//            }

            ContentExtendedInfo content = reqBody.getContent();
            if (!StringUtils.hasLength(content.getTitle())) {
                return ErrorResponse.getErrorResponse(ErrorStatus.CONTENT_TITLE_REQUIRED, HttpStatus.BAD_REQUEST);
            } else if (!StringUtils.hasLength(content.getShortDescription())) {
                return ErrorResponse.getErrorResponse(ErrorStatus.CONTENT_SHORT_DESCRIPTION_REQUIRED, HttpStatus.BAD_REQUEST);
            } else if (!StringUtils.hasLength(content.getDescription())) {
                return ErrorResponse.getErrorResponse(ErrorStatus.CONTENT_DESCRIPTION_REQUIRED, HttpStatus.BAD_REQUEST);
            } else if (!StringUtils.hasLength(content.getSiteUrl())) {
                return ErrorResponse.getErrorResponse(ErrorStatus.CONTENT_SITE_URL_REQUIRED, HttpStatus.BAD_REQUEST);
            } else if (!StringUtils.hasLength(content.getContentUrl())) {
                return ErrorResponse.getErrorResponse(ErrorStatus.CONTENT_URL_REQUIRED, HttpStatus.BAD_REQUEST);
            } else if (content.getPreferences() == null || content.getPreferences().size() <= 0) {
                return ErrorResponse.getErrorResponse(ErrorStatus.CONTENT_PREFERENCES_REQUIRED, HttpStatus.BAD_REQUEST);
            } else if (content.getCategories() == null || content.getCategories().size() <= 0) {
                return ErrorResponse.getErrorResponse(ErrorStatus.CONTENT_CATEGORIES_REQUIRED, HttpStatus.BAD_REQUEST);
            }

            // insert database
            Content responseContent = new Content()
                    .setTitle(content.getTitle())
                    .setShortDescription(content.getShortDescription())
                    .setDescription(content.getDescription())
                    .setContentUrl(content.getContentUrl())
                    .setSiteUrl(content.getSiteUrl())
                    .setLanguageId(content.getLanguageId())
                    .setCreatedBy(content.getCreatedBy());

            responseContent = contentService.insert(responseContent);

            // insert content preference
            for (Preference preference : content.getPreferences()) {
                preferenceService.insert(new ContentPreference()
                        .setContentId(responseContent.getId())
                        .setPreferenceId(preference.getId()));
            }
            // insert content category
            for (Category category : content.getCategories()) {
                categoryService.insert(new ContentCategory()
                        .setContentId(responseContent.getId())
                        .setCategoryId(category.getId()));
            }

            if (files != null && files.size() > 0) {
                int index = 1;
                for (MultipartFile file : files) {

                    String fileName = "content_" + responseContent.getId() + "" + index++ + "." + Utility.getFileExtension(file);
                    storageService.storeFile(file, fileName);
                    Image image = new Image().setContentId(responseContent.getId());
                    FileType type = Utility.getFileType(file);
                    if (type == FileType.IMAGE) {
                        image.setImageUrl(fileName);
                    } else {
                        image.setVideoUrl(fileName);
                    }

                    imageService.insert(image);
                }

            }

            return new SuccessResponse<SuccessResponse>().setStatus(HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return ErrorResponse.getErrorResponse(ErrorStatus.UNKNOWN_ERROR, HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/api/content/edit", method = RequestMethod.PUT)
    public Response editContent(@RequestParam("body") String body,
                                @RequestParam(value = "files", required = false) List<MultipartFile> files) {
        try {

            if (!StringUtils.hasLength(body)) {
                return ErrorResponse.getErrorResponse(ErrorStatus.NO_CONTENT, HttpStatus.NO_CONTENT);
            }


            ObjectMapper objectMapper = new ObjectMapper();
            ContentRequestBody reqBody = objectMapper.readValue(body, ContentRequestBody.class);

            if (reqBody == null || reqBody.getContent() == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.DATA_NOT_PARSED, HttpStatus.BAD_REQUEST);
            }

            if (!StringUtils.hasLength(reqBody.getToken())) {
                return ErrorResponse.getErrorResponse(ErrorStatus.TOKEN_REQUIRED, HttpStatus.BAD_REQUEST);
            }

            AdminAuthToken token = authService.findByToken(reqBody.getToken());
            if (token == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.INVALID_TOKEN, HttpStatus.BAD_REQUEST);
            }

            ContentExtendedInfo content = reqBody.getContent();
            if (content.getId() == null || content.getId().compareTo(BigInteger.ZERO) != 1) {
                return ErrorResponse.getErrorResponse(ErrorStatus.ID_REQUIRED, HttpStatus.BAD_REQUEST);
            } else if (!StringUtils.hasLength(content.getTitle())) {
                return ErrorResponse.getErrorResponse(ErrorStatus.CONTENT_TITLE_REQUIRED, HttpStatus.BAD_REQUEST);
            } else if (!StringUtils.hasLength(content.getShortDescription())) {
                return ErrorResponse.getErrorResponse(ErrorStatus.CONTENT_SHORT_DESCRIPTION_REQUIRED, HttpStatus.BAD_REQUEST);
            } else if (!StringUtils.hasLength(content.getDescription())) {
                return ErrorResponse.getErrorResponse(ErrorStatus.CONTENT_DESCRIPTION_REQUIRED, HttpStatus.BAD_REQUEST);
            } else if (!StringUtils.hasLength(content.getSiteUrl())) {
                return ErrorResponse.getErrorResponse(ErrorStatus.CONTENT_SITE_URL_REQUIRED, HttpStatus.BAD_REQUEST);
            } else if (!StringUtils.hasLength(content.getContentUrl())) {
                return ErrorResponse.getErrorResponse(ErrorStatus.CONTENT_URL_REQUIRED, HttpStatus.BAD_REQUEST);
            } else if (content.isActive() == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.ACTIVE_FLAG_REQUIRED, HttpStatus.BAD_REQUEST);
            } else if (content.isDelete() == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.DELETE_FLAG_REQUIRED, HttpStatus.BAD_REQUEST);
            }

            // insert database
            Content responseContent = new Content()
                    .setId(content.getId())
                    .setTitle(content.getTitle())
                    .setShortDescription(content.getShortDescription())
                    .setDescription(content.getDescription())
                    .setContentUrl(content.getContentUrl())
                    .setSiteUrl(content.getSiteUrl())
                    .setLanguageId(content.getLanguageId())
                    .setActive(content.isActive())
                    .setDelete(content.isDelete())
                    .setUpdatedBy(content.getUpdatedBy());

            responseContent = contentService.update(responseContent);

            // insert content preference
            if (content.getPreferences() != null && content.getPreferences().size() > 0) {
                preferenceService.deleteByContentId(content.getId());
                for (Preference preference : content.getPreferences()) {
                    preferenceService.insert(new ContentPreference()
                            .setContentId(responseContent.getId())
                            .setPreferenceId(preference.getId()));
                }
            }

            // insert content category
            if (content.getCategories() != null && content.getCategories().size() > 0) {
                categoryService.deleteByContentId(content.getId());
                for (Category category : content.getCategories()) {
                    categoryService.insert(new ContentCategory()
                            .setContentId(responseContent.getId())
                            .setCategoryId(category.getId()));
                }
            }

            if (files != null && files.size() > 0) {
                imageService.deleteByContentId(content.getId());
                int index = 1;
                for (MultipartFile file : files) {

                    String fileName = "content_" + responseContent.getId() + "" + index++ + "." + Utility.getFileExtension(file);
                    storageService.storeFile(file, fileName);
                    Image image = new Image().setContentId(responseContent.getId());
                    FileType type = Utility.getFileType(file);
                    if (type == FileType.IMAGE) {
                        image.setImageUrl(fileName);
                    } else {
                        image.setVideoUrl(fileName);
                    }

                    imageService.insert(image);
                }

            }

            return new SuccessResponse<SuccessResponse>().setStatus(HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return ErrorResponse.getErrorResponse(ErrorStatus.UNKNOWN_ERROR, HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/api/content/by/{path}", method = RequestMethod.POST)
    public Response getActiveContents(@RequestBody ContentRequestBody body,
                                      @PathVariable("path") String path) {

        try {
            if (body == null || body.getContent() == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.NO_CONTENT, HttpStatus.NO_CONTENT);
            } else if (!StringUtils.hasLength(path)) {
                return ErrorResponse.getErrorResponse(ErrorStatus.NOT_FOUND, HttpStatus.NOT_FOUND);
            }


            ContentExtendedInfo content = body.getContent();

            if (content.getLanguageId() == null || content.getLanguageId() <= 0) {
                return ErrorResponse.getErrorResponse(ErrorStatus.LANGUAGE_ID_REQUIRED, HttpStatus.BAD_REQUEST);
            }

            List<ContentExtendedInfo> responseContent = null;

            if (path.equals(Constants.PREFERENCE_ID)) {

                if (content.getPreferenceId() == null || content.getPreferenceId() <= 0) {
                    return ErrorResponse.getErrorResponse(ErrorStatus.ID_REQUIRED, HttpStatus.BAD_REQUEST);
                }
                responseContent = contentService.findActiveContentByPreferenceId(content);


            } else if (path.equals(Constants.CATEGORY_ID)) {
                if (content.getCategoryId() == null || content.getCategoryId() <= 0) {
                    return ErrorResponse.getErrorResponse(ErrorStatus.ID_REQUIRED, HttpStatus.BAD_REQUEST);
                }
                responseContent = contentService.findActiveContentByCategoryId(content);

            } else if (path.equals(Constants.USER_ID)) {
                if (content.getUserId() == null || content.getUserId().compareTo(BigInteger.ZERO) != 1) {
                    return ErrorResponse.getErrorResponse(ErrorStatus.USER_ID_REQUIRED, HttpStatus.BAD_REQUEST);
                }
                responseContent = contentService.findBookmarkContentByUserId(content);

            } else {
                return ErrorResponse.getErrorResponse(ErrorStatus.NOT_FOUND, HttpStatus.NOT_FOUND);
            }

            return new ContentResponseBody().setContents(responseContent).setStatus(HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return ErrorResponse.getErrorResponse(ErrorStatus.UNKNOWN_ERROR, HttpStatus.BAD_REQUEST);
        }

    }

    @RequestMapping(value = "/api/content/{id}", method = RequestMethod.GET)
    public Response getContentDetailsById(@PathVariable("id") BigInteger id) {
        try {
            if (id == null || id.compareTo(BigInteger.ZERO) != 1) {
                return ErrorResponse.getErrorResponse(ErrorStatus.NOT_FOUND, HttpStatus.NOT_FOUND);
            }

            ContentExtendedInfo content = contentService.findContentById(id);
            if (content == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.CONTENT_NOT_EXIST, HttpStatus.BAD_REQUEST);
            }
            return new ContentResponseBody().setContent(content).setStatus(HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return ErrorResponse.getErrorResponse(ErrorStatus.UNKNOWN_ERROR, HttpStatus.BAD_REQUEST);
        }

    }


    @GetMapping(Constants.CONTENT + "{fileName:.+}")
    public ResponseEntity<Resource> getContentImage(@PathVariable String fileName, HttpServletRequest request) {
        // Load file as Resource
        Resource resource = storageService.loadFileAsResource(fileName);

        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
//            logger.info("Could not determine file type.");
        }

        // Fallback to the default content type if type could not be determined
        if (contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }
}
