package com.sandesh360.websystem.controller;

import com.sandesh360.websystem.common.ErrorStatus;
import com.sandesh360.websystem.entity.Bookmarks;
import com.sandesh360.websystem.response.BookmarkRequestBody;
import com.sandesh360.websystem.response.ErrorResponse;
import com.sandesh360.websystem.response.Response;
import com.sandesh360.websystem.response.SuccessResponse;
import com.sandesh360.websystem.service.BookmarkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigInteger;

@RestController
@RequestMapping(value = "/api/bookmark")
public class BookmarkController {

    @Autowired
    private BookmarkService service;

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Response addBookmark(@RequestBody BookmarkRequestBody body) {
        try {
            if (body == null || body.getBookmark() == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.NO_CONTENT, HttpStatus.NO_CONTENT);
            }
            Bookmarks bookmark = body.getBookmark();
            if (bookmark.getUserId().compareTo(BigInteger.ZERO) != 1) {
                return ErrorResponse.getErrorResponse(ErrorStatus.USER_ID_REQUIRED, HttpStatus.BAD_REQUEST);

            } else if (bookmark.getContentId().compareTo(BigInteger.ZERO) != 1) {
                return ErrorResponse.getErrorResponse(ErrorStatus.CONTENT_ID_REQUIRED, HttpStatus.BAD_REQUEST);
            }

            bookmark = service.insert(bookmark);
            if (bookmark != null) {
                return new SuccessResponse<SuccessResponse>().setStatus(HttpStatus.OK);
            } else {
                return ErrorResponse.getErrorResponse(ErrorStatus.INSERT_FAILED, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return ErrorResponse.getErrorResponse(ErrorStatus.UNKNOWN_ERROR, HttpStatus.NO_CONTENT);
        }

    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public Response removeBookmark(@RequestBody BookmarkRequestBody body) {
        try {
            if (body == null || body.getBookmark() == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.NO_CONTENT, HttpStatus.NO_CONTENT);
            }
            Bookmarks bookmark = body.getBookmark();
            if (bookmark.getId().compareTo(BigInteger.ZERO) != 1) {
                return ErrorResponse.getErrorResponse(ErrorStatus.ID_REQUIRED, HttpStatus.BAD_REQUEST);
            }

            int index = service.removeById(bookmark.getId());
            if (index > 0) {
                return new SuccessResponse<SuccessResponse>().setStatus(HttpStatus.OK);
            } else {
                return ErrorResponse.getErrorResponse(ErrorStatus.DELETE_FAILED, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return ErrorResponse.getErrorResponse(ErrorStatus.UNKNOWN_ERROR, HttpStatus.BAD_REQUEST);
        }

    }
}
