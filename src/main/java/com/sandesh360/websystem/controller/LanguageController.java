package com.sandesh360.websystem.controller;


import com.sandesh360.websystem.common.ErrorStatus;
import com.sandesh360.websystem.entity.Language;
import com.sandesh360.websystem.response.*;
import com.sandesh360.websystem.service.LanguageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/language")
public class LanguageController {

    @Autowired
    private LanguageService service;


    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Response addNewLanguage(@RequestBody LanguageRequestBody body) {
        try {
            if (body == null || body.getLanguage() == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.NO_CONTENT, HttpStatus.NO_CONTENT);
            }

            Language language = body.getLanguage();

            if (!StringUtils.hasLength(language.getName())) {
                return ErrorResponse.getErrorResponse(ErrorStatus.LANGUAGE_NAME_REQUIRED, HttpStatus.BAD_REQUEST);
            } else if (!StringUtils.hasLength(language.getLocale())) {
                return ErrorResponse.getErrorResponse(ErrorStatus.LANGUAGE_LOCALE_REQUIRED, HttpStatus.BAD_REQUEST);
            }

            language = service.insert(language);
            if (language != null) {
                return new SuccessResponse<SuccessResponse>()
                        .setStatus(HttpStatus.OK);
            } else {
                return ErrorResponse.getErrorResponse(ErrorStatus.ADD_NEW_LANGUAGE_FAILED, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return ErrorResponse.getErrorResponse(ErrorStatus.UNKNOWN_ERROR, HttpStatus.BAD_REQUEST);
        }

    }

    @RequestMapping(value = "/edit", method = RequestMethod.PUT)
    public Object updateLanguage(@RequestBody LanguageRequestBody body) {
        try {
            if (body == null || body.getLanguage() == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.NO_CONTENT, HttpStatus.NO_CONTENT);
            }

            Language language = body.getLanguage();
            if (language.getId() == null || language.getId() <= 0) {
                return ErrorResponse.getErrorResponse(ErrorStatus.ID_REQUIRED, HttpStatus.BAD_REQUEST);
            }
            if (!StringUtils.hasLength(language.getName())) {
                return ErrorResponse.getErrorResponse(ErrorStatus.LANGUAGE_NAME_REQUIRED, HttpStatus.BAD_REQUEST);
            } else if (!StringUtils.hasLength(language.getLocale())) {
                return ErrorResponse.getErrorResponse(ErrorStatus.LANGUAGE_LOCALE_REQUIRED, HttpStatus.BAD_REQUEST);
            }

            language = service.update(language);
            if (language != null) {
                return new SuccessResponse<SuccessResponse>()
                        .setStatus(HttpStatus.OK);
            } else {
                return ErrorResponse.getErrorResponse(ErrorStatus.UPDATE_LANGUAGE_FAILED, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            return ErrorResponse.getErrorResponse(ErrorStatus.UNKNOWN_ERROR, HttpStatus.BAD_REQUEST);
        }
    }


    @RequestMapping(value = "/all/{orderByName}", method = RequestMethod.GET)
    public Response getAllLanguageByOrders(@PathVariable("orderByName") String orderByName) {
        try {
            if (!StringUtils.hasLength(orderByName)) {
                return ErrorResponse.getErrorResponse(ErrorStatus.ORDER_BY_REQUIRED, HttpStatus.NO_CONTENT);
            }

            List<Language> list = service.findAllOrderByName(orderByName);
            if (list == null || list.size() < 0) {
                return ErrorResponse.getErrorResponse(ErrorStatus.NO_DATA_AVAILABLE, HttpStatus.BAD_REQUEST);
            }

            return new LanguageResponseBody().setLanguages(list).setStatus(HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return ErrorResponse.getErrorResponse(ErrorStatus.UNKNOWN_ERROR, HttpStatus.BAD_REQUEST);
        }

    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public Response deleteLanguage(@PathVariable("id") final int id) {
        try {
            if (id <= 0) {
                return ErrorResponse.getErrorResponse(ErrorStatus.ID_REQUIRED, HttpStatus.BAD_REQUEST);
            }

            long index = service.deleteById(id);
            if (index <= 0) {
                return ErrorResponse.getErrorResponse(ErrorStatus.DELETE_FAILED, HttpStatus.BAD_REQUEST);
            }

            return new SuccessResponse<SuccessResponse>().setStatus(HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return ErrorResponse.getErrorResponse(ErrorStatus.UNKNOWN_ERROR, HttpStatus.BAD_REQUEST);
        }

    }
}
