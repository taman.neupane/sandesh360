package com.sandesh360.websystem.controller;

import com.sandesh360.websystem.common.Constants;
import com.sandesh360.websystem.common.ErrorStatus;
import com.sandesh360.websystem.entity.AdminAuthToken;
import com.sandesh360.websystem.entity.Questions;
import com.sandesh360.websystem.response.*;
import com.sandesh360.websystem.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.math.BigInteger;
import java.util.List;

@RestController
@RequestMapping("/api/question")
public class QuestionController {

    @Autowired
    private QuestionService service;

    @Autowired
    private AdminAuthService authService;

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Response addQuestion(@RequestBody QuestionRequestBody body) {

        try {
            if (body == null || body.getQuestion() == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.NO_CONTENT, HttpStatus.NO_CONTENT);
            }

            if (!StringUtils.hasLength(body.getToken())) {
                return ErrorResponse.getErrorResponse(ErrorStatus.TOKEN_REQUIRED, HttpStatus.BAD_REQUEST);
            }

            AdminAuthToken token = authService.findByToken(body.getToken());
            if (token == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.INVALID_TOKEN, HttpStatus.BAD_REQUEST);
            }

            Questions question = body.getQuestion();

            if (!StringUtils.hasLength(question.getQuestionText())) {
                return ErrorResponse.getErrorResponse(ErrorStatus.QUESTION_TEXT_REQUIRED, HttpStatus.BAD_REQUEST);

            } else if (question.getContentId().compareTo(BigInteger.ZERO) != 1) {
                return ErrorResponse.getErrorResponse(ErrorStatus.CONTENT_ID_REQUIRED, HttpStatus.BAD_REQUEST);

            } else if (question.getLanguageId() == null || question.getLanguageId() < 1) {
                return ErrorResponse.getErrorResponse(ErrorStatus.LANGUAGE_ID_REQUIRED, HttpStatus.BAD_REQUEST);
            }

            service.insert(question);
            return new SuccessResponse<SuccessResponse>().setStatus(HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return ErrorResponse.getErrorResponse(ErrorStatus.UNKNOWN_ERROR, HttpStatus.BAD_REQUEST);
        }

    }

    @RequestMapping(value = "/edit", method = RequestMethod.PUT)
    public Response editQuestion(@RequestBody QuestionRequestBody body) {

        try {
            if (body == null || body.getQuestion() == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.NO_CONTENT, HttpStatus.NO_CONTENT);
            }

            if (!StringUtils.hasLength(body.getToken())) {
                return ErrorResponse.getErrorResponse(ErrorStatus.TOKEN_REQUIRED, HttpStatus.BAD_REQUEST);
            }

            AdminAuthToken token = authService.findByToken(body.getToken());
            if (token == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.INVALID_TOKEN, HttpStatus.BAD_REQUEST);
            }

            Questions question = body.getQuestion();

            if (question.getId() == null || question.getId().compareTo(BigInteger.ZERO) != 1) {
                return ErrorResponse.getErrorResponse(ErrorStatus.ID_REQUIRED, HttpStatus.BAD_REQUEST);
            } else if (!StringUtils.hasLength(question.getQuestionText())) {
                return ErrorResponse.getErrorResponse(ErrorStatus.QUESTION_TEXT_REQUIRED, HttpStatus.BAD_REQUEST);
            } else if (question.getContentId().compareTo(BigInteger.ZERO) != 1) {
                return ErrorResponse.getErrorResponse(ErrorStatus.CONTENT_ID_REQUIRED, HttpStatus.BAD_REQUEST);
            } else if (question.getLanguageId() == null || question.getLanguageId() < 1) {
                return ErrorResponse.getErrorResponse(ErrorStatus.LANGUAGE_ID_REQUIRED, HttpStatus.BAD_REQUEST);
            } else if (question.isActive() == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.ACTIVE_FLAG_REQUIRED, HttpStatus.BAD_REQUEST);
            } else if (question.isDelete() == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.DELETE_FLAG_REQUIRED, HttpStatus.BAD_REQUEST);
            }

            question = service.update(question);
            if (question == null) {
                return ErrorResponse.getErrorResponse(ErrorStatus.UPDATE_FAILED, HttpStatus.BAD_REQUEST);
            }
            return new SuccessResponse<SuccessResponse>().setStatus(HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return ErrorResponse.getErrorResponse(ErrorStatus.UNKNOWN_ERROR, HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/{path}", method = RequestMethod.GET)
    public Response getQuestions(@PathVariable("path") String path) {

        try {
            List<Questions> list = null;
            if (StringUtils.hasLength(path) && path.equals(Constants.ACTIVE)) {
                list = service.findActiveQuestions();
            } else if (StringUtils.hasLength(path) && path.equals(Constants.ALL)) {
                list = service.findAll();
            }

            return new QuestionResponseBody().setQuestions(list).setStatus(HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return ErrorResponse.getErrorResponse(ErrorStatus.UNKNOWN_ERROR, HttpStatus.BAD_REQUEST);
        }

    }
}
