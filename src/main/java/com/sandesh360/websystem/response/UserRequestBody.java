package com.sandesh360.websystem.response;

import com.sandesh360.websystem.entity.User;

public class UserRequestBody extends BaseRequestBody {

    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
