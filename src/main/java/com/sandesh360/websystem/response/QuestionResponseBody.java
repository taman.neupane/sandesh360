package com.sandesh360.websystem.response;

import com.sandesh360.websystem.entity.Questions;

import java.util.List;

public class QuestionResponseBody extends SuccessResponse<QuestionResponseBody> {

    private Questions question;

    private List<Questions> questions;

    public Questions getQuestion() {
        return question;
    }

    public QuestionResponseBody setQuestion(Questions question) {
        this.question = question;
        return this;
    }

    public List<Questions> getQuestions() {
        return questions;
    }

    public QuestionResponseBody setQuestions(List<Questions> questions) {
        this.questions = questions;
        return this;
    }
}
