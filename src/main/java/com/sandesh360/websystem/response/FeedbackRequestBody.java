package com.sandesh360.websystem.response;

import com.sandesh360.websystem.entity.Feedback;

public class FeedbackRequestBody extends BaseRequestBody {

    private Feedback feedback;

    public Feedback getFeedback() {
        return feedback;
    }

    public void setFeedback(Feedback feedback) {
        this.feedback = feedback;
    }
}
