package com.sandesh360.websystem.response;

import com.sandesh360.websystem.model.ContentExtendedInfo;

import java.util.List;

public class ContentResponseBody extends SuccessResponse<ContentResponseBody> {

    private ContentExtendedInfo content;

    private List<ContentExtendedInfo> contents;

    public ContentExtendedInfo getContent() {
        return content;
    }

    public ContentResponseBody setContent(ContentExtendedInfo content) {
        this.content = content;
        return this;
    }

    public List<ContentExtendedInfo> getContents() {
        return contents;
    }

    public ContentResponseBody setContents(List<ContentExtendedInfo> contents) {
        this.contents = contents;
        return this;
    }
}
