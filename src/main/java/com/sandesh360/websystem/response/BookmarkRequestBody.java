package com.sandesh360.websystem.response;

import com.sandesh360.websystem.entity.Bookmarks;

public class BookmarkRequestBody extends BaseRequestBody {

    private Bookmarks bookmark;

    public Bookmarks getBookmark() {
        return bookmark;
    }

    public void setBookmark(Bookmarks bookmark) {
        this.bookmark = bookmark;
    }
}
