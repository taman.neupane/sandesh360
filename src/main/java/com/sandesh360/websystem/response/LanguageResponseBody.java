package com.sandesh360.websystem.response;

import com.sandesh360.websystem.entity.Language;

import java.util.List;

public class LanguageResponseBody extends SuccessResponse<LanguageResponseBody> {

    private List<Language> languages;

    public List<Language> getLanguages() {
        return languages;
    }

    public LanguageResponseBody setLanguages(List<Language> languages) {
        this.languages = languages;
        return this;
    }
}
