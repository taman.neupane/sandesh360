package com.sandesh360.websystem.response;

import com.sandesh360.websystem.entity.Poll;

public class PollRequestBody extends BaseRequestBody {

    private Poll poll;

    public Poll getPoll() {
        return poll;
    }

    public void setPoll(Poll poll) {
        this.poll = poll;
    }
}
