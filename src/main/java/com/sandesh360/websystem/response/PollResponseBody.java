package com.sandesh360.websystem.response;

import com.sandesh360.websystem.entity.Poll;
import com.sandesh360.websystem.model.PollPercentage;

public class PollResponseBody extends SuccessResponse<PollResponseBody> {

    private Poll poll;

    private PollPercentage percentage;

    public Poll getPoll() {
        return poll;
    }

    public PollResponseBody setPoll(Poll poll) {
        this.poll = poll;
        return this;
    }

    public PollPercentage getPercentage() {
        return percentage;
    }

    public PollResponseBody setPercentage(PollPercentage percentage) {
        this.percentage = percentage;
        return this;
    }
}
