package com.sandesh360.websystem.response;

import com.sandesh360.websystem.entity.Admin;

public class AdminResponseBody extends SuccessResponse<AdminResponseBody> {

    private Admin admin;

    public Admin getAdmin() {
        return admin;
    }

    public AdminResponseBody setAdmin(Admin admin) {
        this.admin = admin;
        return this;
    }

}
