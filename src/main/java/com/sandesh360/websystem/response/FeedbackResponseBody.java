package com.sandesh360.websystem.response;

import com.sandesh360.websystem.entity.Feedback;

import java.util.List;

public class FeedbackResponseBody extends SuccessResponse<FeedbackResponseBody> {

    private Feedback feedback;

    private List<Feedback> feedbacks;

    public Feedback getFeedback() {
        return feedback;
    }

    public FeedbackResponseBody setFeedback(Feedback feedback) {
        this.feedback = feedback;
        return this;
    }

    public List<Feedback> getFeedbacks() {
        return feedbacks;
    }

    public FeedbackResponseBody setFeedbacks(List<Feedback> feedbacks) {
        this.feedbacks = feedbacks;
        return this;
    }
}
