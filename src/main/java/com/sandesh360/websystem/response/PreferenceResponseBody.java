package com.sandesh360.websystem.response;

import com.sandesh360.websystem.entity.Preference;

import java.util.List;

public class PreferenceResponseBody extends SuccessResponse<PreferenceResponseBody> {

    private Preference preference;

    private List<Preference> preferences;


    public Preference getPreference() {
        return preference;
    }

    public PreferenceResponseBody setPreference(Preference preference) {
        this.preference = preference;
        return this;
    }

    public List<Preference> getPreferences() {
        return preferences;
    }

    public PreferenceResponseBody setPreferences(List<Preference> preferences) {
        this.preferences = preferences;
        return this;
    }
}
