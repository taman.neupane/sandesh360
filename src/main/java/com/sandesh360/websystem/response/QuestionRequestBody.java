package com.sandesh360.websystem.response;

import com.sandesh360.websystem.entity.Questions;

public class QuestionRequestBody extends BaseRequestBody {

    private Questions question;

    public Questions getQuestion() {
        return question;
    }

    public void setQuestion(Questions question) {
        this.question = question;
    }
}
