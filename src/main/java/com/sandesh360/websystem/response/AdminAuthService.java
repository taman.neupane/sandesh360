package com.sandesh360.websystem.response;

import com.sandesh360.websystem.entity.AdminAuthToken;
import com.sandesh360.websystem.mapper_dao.AdminAuthRepository;
import com.sandesh360.websystem.service.IBaseService;
import org.apache.oltu.oauth2.as.issuer.MD5Generator;
import org.apache.oltu.oauth2.as.issuer.OAuthIssuer;
import org.apache.oltu.oauth2.as.issuer.OAuthIssuerImpl;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdminAuthService implements IBaseService<AdminAuthToken> {

    @Autowired
    private AdminAuthRepository repository;

    private OAuthIssuer aOAuthIssuer = new OAuthIssuerImpl(new MD5Generator());


    @Override
    public AdminAuthToken insert(AdminAuthToken entity) {
        try {
            entity.setToken(aOAuthIssuer.authorizationCode());
            repository.insert(entity);
            return entity;
        } catch (OAuthSystemException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public AdminAuthToken update(AdminAuthToken entity) {
        return null;
    }

    @Override
    public int deleteById(long id) {
        return repository.deleteById(id);
    }

    @Override
    public List<AdminAuthToken> findAll() {
        return null;
    }

    @Override
    public AdminAuthToken findById(long id) {
        return repository.findById(id);
    }


    public AdminAuthToken findByToken(String token) {
        return repository.findByToken(token);
    }
}
