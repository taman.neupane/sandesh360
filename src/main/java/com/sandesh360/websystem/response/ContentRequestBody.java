package com.sandesh360.websystem.response;

import com.sandesh360.websystem.model.ContentExtendedInfo;

public class ContentRequestBody extends BaseRequestBody {

    private ContentExtendedInfo content;

    public ContentExtendedInfo getContent() {
        return content;
    }

    public void setContent(ContentExtendedInfo content) {
        this.content = content;
    }
}
