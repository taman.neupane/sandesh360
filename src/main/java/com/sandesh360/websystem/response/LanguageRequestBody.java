package com.sandesh360.websystem.response;

import com.sandesh360.websystem.entity.Language;

public class LanguageRequestBody extends BaseRequestBody {

    private Language language;

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }
}
