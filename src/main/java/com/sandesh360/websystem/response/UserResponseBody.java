package com.sandesh360.websystem.response;

import com.sandesh360.websystem.entity.User;

public class UserResponseBody extends SuccessResponse<UserResponseBody> {

    private User user;

    public User getUser() {
        return user;
    }

    public UserResponseBody setUser(User user) {
        this.user = user;
        return this;
    }
}
