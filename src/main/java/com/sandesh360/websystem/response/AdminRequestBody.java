package com.sandesh360.websystem.response;

import com.sandesh360.websystem.entity.Admin;

public class AdminRequestBody extends BaseRequestBody {

    private Admin admin;

    public Admin getAdmin() {
        return admin;
    }

    public void setAdmin(Admin admin) {
        this.admin = admin;
    }
}
