package com.sandesh360.websystem.response;

import com.sandesh360.websystem.entity.Category;

public class CategoryRequestBody extends BaseRequestBody {

    private Category category;

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}
