package com.sandesh360.websystem.response;

import com.sandesh360.websystem.entity.Category;

import java.util.List;

public class CategoryResponseBody extends SuccessResponse<CategoryResponseBody> {

    private Category category;

    private List<Category> categories;


    public Category getCategory() {
        return category;
    }

    public CategoryResponseBody setCategory(Category category) {
        this.category = category;
        return this;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public CategoryResponseBody setCategories(List<Category> categories) {
        this.categories = categories;
        return this;
    }
}
