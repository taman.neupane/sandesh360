package com.sandesh360.websystem.response;

import com.sandesh360.websystem.entity.Preference;

public class PreferenceRequestBody extends BaseRequestBody {

    private Preference preference;

    public Preference getPreference() {
        return preference;
    }

    public void setPreference(Preference preference) {
        this.preference = preference;
    }
}
