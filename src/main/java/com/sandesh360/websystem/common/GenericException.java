package com.sandesh360.websystem.common;

public class GenericException extends Exception{
	
	/**
	 * Error Code
	 */
	private String errorCode;
	
	/**
	 * Error Message
	 */
	private String errorMessage;
	
	/**
	 * GenericException Constructor
	 * 
	 * @param errorCode Error Code
	 */
	public GenericException(String errorCode) {
		this.errorCode = errorCode;
	}	
	
	/**
	 * GenericException Constructor
	 * 
	 * @param errorCode Error Code
	 * @param errorMessage 
	 */
	public GenericException(String errorCode, String errorMessage) {
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	
	
	
	

}
