package com.sandesh360.websystem.common;

public class ErrorKeyUtils {

    public static final String ERROR_NUMBER_FORMAT_INVALID = "Invalid Number format";
    // Customer related error
    public static String ERROR_EMAIL_REQUIRED = "Email_Required";
    public static String ERROR_EMAIL_INVALID = "Email_Invalid";

    // api/System level error
    public static String ERROR_SERVER_CONNECTION_LOST = "Server_Connection_Lost";
    public static String ERROR_AUTHENTICATION_SERVER_ERROR = "Authentication_Server_Error";
    public static String ERROR_TOKEN_EXPIRED = "Error_Token_Expired";
    public static String ERROR_INTERNAL_SERVER = "Internal_Server_Error";


    // Http Exception Constant messages
    public static String MSG_INTERNAL_SERVER_ERROR = "500 Internal Server Error";
    public static String MSG_BAD_REQUEST = "400 Bad Request";
    public static String MSG_SERVER_REQUEST_SUCCEEDED = "OK";
    public static String MSG_Not_Found = "404 Not Found";
    public static String MSG_ERROR_TOKEN_EXPIRED = "401 Unauthorized";
    public static String MSG_ERROR_AUTHENTICATION_FAILED = "401 Unauthorized";
	public static final String INTERNAL_SERVER_ERROR = "internal.server.error";
}
