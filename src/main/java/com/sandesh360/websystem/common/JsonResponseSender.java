package com.sandesh360.websystem.common;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONObject;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

public class JsonResponseSender {
	
	private static final Log log = LogFactory.getLog(JsonResponseSender.class);
	
	public static final String KEY_STATUS = "status";
	
	public static final String KEY_CODE = "code";
	
	public static final String KEY_MESSAGE = "message";
	
	public static ResponseEntity<?> sendErrorResponse(String errorCode, String errorMessage, HttpStatus status)
	{		
			log.info("Sending Error Message Status: " + status.value() + " code: " + errorCode + " message:" + errorMessage);
		    
			JSONObject aJsonObject = new JSONObject();
		    
		    aJsonObject.put(KEY_STATUS, status);
		    aJsonObject.put(KEY_CODE, errorCode);
		    aJsonObject.put(KEY_MESSAGE, errorMessage);		
		
	        ResponseEntity<?> aResponse = new ResponseEntity<>(aJsonObject.toString(), status);
	        
	        return aResponse;
		
	}
	
	/**
	 * This function Send a Success response to the client
	 * @param aResponse response Json Object
	 * @return
	 */
	public static ResponseEntity<?> sendSuccessResponse(JSONObject aResponse) {
	    final HttpHeaders httpHeaders= new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
        return new ResponseEntity<>(aResponse.toString(), httpHeaders, HttpStatus.OK);

	}

}
