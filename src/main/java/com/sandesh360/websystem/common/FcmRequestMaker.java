package com.sandesh360.websystem.common;

import com.sandesh360.websystem.model.FcmModel;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONObject;

import javax.validation.constraints.NotNull;

public class FcmRequestMaker {
	
	private static final Log log = LogFactory.getLog(FcmRequestMaker.class);
	
	public static final String KEY_TO = "to";
	
	public static final String KEY_PRIORITY = "priority";
	
	public static final String KEY_NOTIFICATION = "notification";
	
	public static final String KEY_BODY = "body";
	
	public static final String KEY_TITLE = "title";
	
	public static final String KEY_CLICK_ACTION = "click_action";
	
	/**
	 * Prepare the FCM Request Json Body 
	 * 
	 * @param aFcmModel aFcmModel
	 * @return JSON Object
	 */
	public static JSONObject getFCMRequestJson(@NotNull FcmModel aFcmModel) {

		log.info("FCM Request maker: Starting...................");

		// Make the Response JSON Object
		JSONObject aResponseObject = new JSONObject();
		aResponseObject.put(KEY_TO, aFcmModel.getFcmToken());
		aResponseObject.put(KEY_PRIORITY, aFcmModel.getPriority());

		JSONObject aFCMNotification = new JSONObject();
		aFCMNotification.put(KEY_BODY, aFcmModel.getMessageBody());
		aFCMNotification.put(KEY_TITLE, aFcmModel.getMessageTitle());
		aFCMNotification.put(KEY_CLICK_ACTION, aFcmModel.getClickAction());
		
		aResponseObject.put(KEY_NOTIFICATION, aFCMNotification);

		log.info("FCM Request maker: Returning Response for FCM Token: " + aFcmModel.getFcmToken());

		return aResponseObject;

	}

}
