package com.sandesh360.websystem.common;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class ErrorCodeUtilities {
	
	private static ErrorCodeUtilities mInstance;
	
	private Map<String, String> errorCodeMap;
	
	private Map<String, String> errorKeyMap = null;
	
	
	
	private ErrorCodeUtilities() {
		this.constructErrorCodeMap();
	}
	
	public static ErrorCodeUtilities getInstance() {
		if(mInstance == null) {
			mInstance = new ErrorCodeUtilities();
		}
		
		return mInstance;
	}
	
	private void constructErrorCodeMap() {
		if(errorCodeMap == null) {
			errorCodeMap = new HashMap<>();
		}		


        // Common
        errorCodeMap.put(ErrorKeyUtils.ERROR_EMAIL_REQUIRED, "30018");
        errorCodeMap.put(ErrorKeyUtils.ERROR_EMAIL_INVALID, "30019");

        //System
        errorCodeMap.put(ErrorKeyUtils.ERROR_SERVER_CONNECTION_LOST, "40002");
        errorCodeMap.put(ErrorKeyUtils.ERROR_AUTHENTICATION_SERVER_ERROR, "40005");
        errorCodeMap.put(ErrorKeyUtils.ERROR_INTERNAL_SERVER, "40009");
        errorCodeMap.put(ErrorKeyUtils.ERROR_TOKEN_EXPIRED, "40003");

	}
	
	
	public  String getErrorCode(String errorName) {
		return errorCodeMap.get(errorName);
	}
	
	public String getErrorKey(String errorCode) {
	
		if(errorKeyMap == null) {
			errorKeyMap=new HashMap<>();
		
		    for(Entry<String, String> aItem : errorCodeMap.entrySet()) {
		    	errorKeyMap.put(aItem.getValue(), aItem.getKey());
		    }
		}
		
		return errorKeyMap.get(errorCode);
	}

}
