package com.sandesh360.websystem.common;


import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.multipart.MultipartFile;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utility {

    //private static Utility mInstance;


    /**
     * Log writer
     */
    protected final static Log log = LogFactory.getLog(Utility.class);

//    private Utility() {
//
//    }
//
//    public static Utility getInstance() {
//        if (mInstance == null) {
//            mInstance = new Utility();
//        }
//        return mInstance;
//    }

    /**
     * This Function Returns the Current DateTime of the System
     *
     * @return
     */
    public static Timestamp getCurrentDateTime() {
        try {
            SimpleDateFormat aSimpleDateFormat = new SimpleDateFormat("yyyyMMdd HH:mm:ss");

            String aDate = aSimpleDateFormat.format(new Date(Calendar.getInstance().getTimeInMillis()));

            return Timestamp.valueOf(aDate);
        } catch (Exception e) {
            return new Timestamp(Calendar.getInstance().getTimeInMillis());
        }
    }

    /**
     * This Function Returns the Current Date of the System
     *
     * @return
     */

    public static Date getCurrentDate() {
        try {
            SimpleDateFormat aSimpleDateFormat = new SimpleDateFormat("yyyyMMdd");

            String aDate = aSimpleDateFormat.format(new Date(Calendar.getInstance().getTimeInMillis()));

            return Date.valueOf(aDate);
        } catch (Exception e) {
            return new Date(Calendar.getInstance().getTimeInMillis());
        }
    }

    /**
     * Add time with current time
     *
     * @param hour
     * @param minute
     * @param second
     * @return Timestamp object
     */
    public static Timestamp addTimeWithCurrentTime(int hour, int minute, int second) {
        Timestamp currentTime = new Timestamp(System.currentTimeMillis());
        currentTime.setTime(currentTime.getTime() + ((hour * 60 * 60) + (minute * 60) + second) * 1000);

        return Timestamp.valueOf(getTimestampAsString_YYYYMMDDHHmmss(currentTime));
    }

    /**
     * Double Byte Character Change
     *
     * @param aString
     * @return true|false
     */
    public static boolean isContainDoubleByteCharacter(String aString) {
        try {

            //  for (int i=0; i<aString.length(); i++) {
            if (aString.getBytes("SJIS").length > aString.length()) {
                return true;
            }
            return false;
        } catch (Exception ex) {
            return true;
        }
    }

    /**
     * Email validation method
     *
     * @param aString
     * @return true|false
     */
    public static boolean isValidEmail(String aString) {
        try {

            if (null != aString) {
                String regex = "^([_a-zA-Z0-9-]+(\\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*(\\.[a-zA-Z]{1,6}))?$";
                Pattern pattern = Pattern.compile(regex);
                Matcher matcher = pattern.matcher(aString);
                if (!matcher.matches()) {
                    return false;
                }
            }
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    /**
     * Get Date from the String
     *
     * @param dateFormat Format of the Date
     * @param dateStr    Date String
     * @return
     */
    public static Timestamp getFormattedDate(String dateFormat, String dateStr) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        try {
            java.util.Date aDate = formatter.parse(dateStr);

            return Timestamp.valueOf(formatter.format(aDate));

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }

    }

    /**
     * Get Current Date As Formatted String
     *
     * @return YYYYMMDDHHmmss
     */
    public static String getCurrentDateAsString_YYYYMMDDHHmmss() {
        SimpleDateFormat writeFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        return writeFormat.format(new Date(Calendar.getInstance().getTimeInMillis()));
    }

    /**
     * Get Timestamp As Formatted String
     *
     * @return yyyy-MM-dd HH:mm:ss
     */
    public static String getTimestampAsString_YYYYMMDDHHmmss(Timestamp timestamp) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return simpleDateFormat.format(timestamp);
    }

    /**
     * Get Current Date As Formatted String
     *
     * @param format expected format of the DateTime
     * @return YYYYMMDDHHmmss
     */
    public static String getCurrentDateAsString(String format) {
        SimpleDateFormat writeFormat = new SimpleDateFormat(format);
        return writeFormat.format(new Date(Calendar.getInstance().getTimeInMillis()));
    }

    /**
     * Display  DateTime As Formatted String
     * <p>
     * dataValue Displaying data value
     *
     * @param dataValue expected format of the DateTime
     * @return yyyy-MM-dd HH:mm:ss
     */
    public static String getDisplayDateTimeAsString(Timestamp dataValue) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(dataValue);
    }

    /**
     * This function converts String to Integer value
     * If the format is wrong it will throw generic exception
     *
     * @param valueToConvert
     * @return
     */
    public static Integer getIntegerValueFromString(String valueToConvert) throws GenericException {
        Integer returnIntefger = 0;
        try {
            returnIntefger = Integer.parseInt(valueToConvert);
        } catch (NumberFormatException nfe) {
            // throw new exception
            throw new GenericException(ErrorCodeUtilities.getInstance().getErrorCode(ErrorKeyUtils.ERROR_NUMBER_FORMAT_INVALID), "Invalid Number format..");
        }
        return returnIntefger;
    }

    /**
     * This function converts String to Bigdecimal value
     * If the format is wrong it will throw generic exception
     *
     * @param valueToConvert
     * @return
     */
    public static BigDecimal getBigDecimalValueFromString(String valueToConvert) throws GenericException {
        BigDecimal returnBigdecimal = new BigDecimal("0");
        try {
            returnBigdecimal = BigDecimal.valueOf(Long.parseLong(valueToConvert));
        } catch (NumberFormatException nfe) {
            // throw new exception
            throw new GenericException(ErrorCodeUtilities.getInstance().getErrorCode(ErrorKeyUtils.ERROR_NUMBER_FORMAT_INVALID), "Invalid Number format..");
        }
        return returnBigdecimal;
    }

    /**
     * Common Encrypt string method(sha1)
     *
     * @param stringToConvert
     * @return
     */
	public static String encryptStringSha1(String stringToConvert){
		// Check the Plain Text is Blank
	    if( StringUtils.isBlank(stringToConvert)){
			return StringUtils.EMPTY;
		}

	    String encryptedString = DigestUtils.sha1Hex(stringToConvert);
		return encryptedString;
	}

    /**
     * Common Encrypt string method(Base64)
     *
     * @param stringToConvert
     * @return
     */
    public static String encryptString(String stringToConvert) {
        // Check the Plain Text is Blank
        if (StringUtils.isBlank(stringToConvert)) {
            return StringUtils.EMPTY;
        }

        byte[] encodedBytes = Base64.getEncoder().encode(stringToConvert.getBytes());
        return new String(encodedBytes);
    }

    /**
     * Decryption method using Base64
     *
     * @param encodedString
     * @return
     */
    public static String decryptString(String encodedString) {
        // Check the Cipher Text is Blank
        if (StringUtils.isBlank(encodedString)) {
            return StringUtils.EMPTY;
        }

        byte[] decodedBytes = Base64.getDecoder().decode(encodedString.getBytes());
        return new String(decodedBytes);
    }


    /**
     * Return the extension of the file
     *
     * @param file
     * @return
     */
    public static String getFileExtension(MultipartFile file) {
        // Get file extension
        String[] fileNameSplits = file.getOriginalFilename().split("\\.");
        // extension is assumed to be the last part
        int extensionIndex = fileNameSplits.length - 1;

        return fileNameSplits[extensionIndex];
    }

    /**
     * Return the extension of the file
     *
     * @param file
     * @return
     */
    public static FileType getFileType(MultipartFile file) {
        // Get file extension
        try {
            String extension = getFileExtension(file);

            if (extension.equalsIgnoreCase("png")
                    || extension.equalsIgnoreCase("jpg")
                    || extension.equalsIgnoreCase("jpeg")) {
                return FileType.IMAGE;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return FileType.VIDEO;
    }


}
