package com.sandesh360.websystem.common;

public enum ErrorStatus {
    // Common Error
    UNKNOWN_ERROR(1000, "Unknown Error"),
    NO_CONTENT(1001, "No content"),
    ID_REQUIRED(1002, "ID required"),
    TOKEN_NOT_GENERATED(1003, "Auth token generation failed"),
    INVALID_TOKEN(1004, "Invalid token"),
    TOKEN_REQUIRED(1005, "Token required"),
    NO_DATA_AVAILABLE(1006, "No data available"),
    INSERT_FAILED(1007, "Insert failed"),
    UPDATE_FAILED(1008, "Update failed"),
    DELETE_FAILED(1009, "Delete failed"),
    DATA_NOT_PARSED(1010, "Data not parsed"),
    ACTIVE_FLAG_REQUIRED(1011, "Active flag required"),
    DELETE_FLAG_REQUIRED(1012, "Delete flag required"),
    EMAIL_REQUIRED(1013, "Email is required"),
    PHONE_NO_REQUIRED(1014, "Phone number is required"),
    NOT_FOUND(1015, "Not Found"),

    // Admin Error
    USER_NAME_REQUIRED(2001, "User name is required"),
    PASSWORD_REQUIRED(2002, "Password is required"),
    NAME_REQUIRED(2003, "User name is required"),
    ADDRESS_REQUIRED(2004, "Address is required"),
    USER_NAME_OR_PASSWORD_WRONG(2005, "User name or password is wrong"),
    NEW_PASSWORD_REQUIRED(2006, "Password is required"),
    NEW_AND_CONFIRM_PASSWORD_DOES_NOT_MATCH(2007, "New and confirm password does not match"),
    PASSWORD_CHANGE_FAILED(2008, "Password change failed"),


    // Language Error
    LANGUAGE_NAME_REQUIRED(3001, "Language name is required"),
    LANGUAGE_LOCALE_REQUIRED(3002, "Language locale is required"),
    ADD_NEW_LANGUAGE_FAILED(3003, "Add new language is failed"),
    UPDATE_LANGUAGE_FAILED(3004, "Update language is failed"),
    ORDER_BY_REQUIRED(3005, "Sort order is required"),
    LANGUAGE_ID_REQUIRED(3006, "Language id is required"),

    // Preference Error
    PREFERENCE_NAME_REQUIRED(4001, "Preference name is required"),
    PREFERENCE_ADD_FAILED(4002, "Preference adding is failed"),

    // Preference Error
    CATEGORY_NAME_REQUIRED(5001, "Category name is required"),
    CATEGORY_ADD_FAILED(5002, "Category adding is failed"),

    // Content Error
    CONTENT_TITLE_REQUIRED(6001, "Content title is required"),
    CONTENT_SHORT_DESCRIPTION_REQUIRED(6002, "Content short description is required"),
    CONTENT_DESCRIPTION_REQUIRED(6003, "Content description is required"),
    CONTENT_URL_REQUIRED(6004, "Content url is required"),
    CONTENT_SITE_URL_REQUIRED(6005, "Content site url is required"),
    CONTENT_CATEGORIES_REQUIRED(6006, "Content categories is required"),
    CONTENT_PREFERENCES_REQUIRED(6007, "Content preferences is required"),
    CONTENT_NOT_EXIST(6008,"Content does not exist"),

    // Feedback errors
    COMMENT_REQUIRED(7001, "Comment can't be blank"),

    // Bookmark errors
    USER_ID_REQUIRED(8001, "User id is required"),
    CONTENT_ID_REQUIRED(8002, "Content id is required"),

    // Question & Poll errors
    QUESTION_TEXT_REQUIRED(9001, "Question text required"),
    QUESTION_ID_REQUIRED(9002, "Question id is required"),
    VOTER_REQUIRED(9003, "Voter is required"),
    VOTE_REQUIRED(9004, "Vote is required");


    private final int code;
    private final String message;

    private ErrorStatus(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int code() {
        return this.code;
    }

    public String getMessage() {
        return this.message;
    }
}
