package com.sandesh360.websystem.common;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Properties;


/**
 * 
 * Application Property Utility
 * 
 * @author Mobarak Hosen
 *
 */

public class ApplicationPropertiesUtility {
    
    /**
     * Logger
     */
    protected final Log log = LogFactory.getLog(ApplicationPropertiesUtility.class);
    
    
    
    private static ApplicationPropertiesUtility mInstance;
    
    private Properties prop;
    
    private ApplicationPropertiesUtility() {
    	prop = loadPropertyFile("application.properties");
    }

	/**
	 * This Function Load the Property of a Propery file and send it as Return Value
	 * 
	 * @param propertyFileName Property File Name
	 */
	private Properties loadPropertyFile(String propertyFileName) {
		Properties aProp = new Properties();
		try{
            
            // Loading the Property File
            log.info(String.format("Start Loading the Property File [%s]", propertyFileName) );
            ClassLoader classLoader = Thread.currentThread().getContextClassLoader();           
            InputStream input =  classLoader.getResourceAsStream("application.properties");
            aProp.load(input);
            log.info(String.format("Completed Loading the Property File [%s]", propertyFileName));
        } catch (Exception e) {
           log.error("Property File Loading Failed........................." + e.getMessage());
           e.printStackTrace();
        }
		
		return aProp;
	}
    
    
    
    
   
    
    public static ApplicationPropertiesUtility getInstance() {
        if(mInstance == null) {
            mInstance = new ApplicationPropertiesUtility();
        }        
        return mInstance;
    }
    
    /**
     * Get Boolean Value of the Property
     * @param propertyKey
     * @return
     */
    public Boolean getBooleanValue(String propertyKey) {
        try {
           String value = prop.getProperty(propertyKey);
           return Boolean.parseBoolean(value);           
        } catch (Exception e) {
           log.error("ApplicationPropertiesUtility: " + e.getMessage());
           e.printStackTrace();
           return Boolean.FALSE;
        }
        
    }
    
    /**
     * Get String Value of the Property
     * 
     * @param propertyKey
     * @return
     */
    public String getStringValue(String propertyKey) {
        try {
           String value = prop.getProperty(propertyKey);
           return value;         
        } catch (Exception e) {
           log.error("ApplicationPropertiesUtility: " + e.getMessage());
           e.printStackTrace();
           return "";
        }
        
    }
    
    
    /**
     * Get String Value of the Property Which Value
     * @param propertyFileName propertyFileName
     * @param propertyKey
     * @return
     */
    public String getStringValue( String propertyFileName,  String propertyKey) {
        try {
           Properties aProp = loadPropertyFile(propertyFileName);
           String value = aProp.getProperty(propertyKey);
           return value;         
        } catch (Exception e) {
           log.error("ApplicationPropertiesUtility: " + e.getMessage());
           e.printStackTrace();
           return "";
        }
        
    }
    
    
    
    /**
     * Get Integer Value of the Property
     * 
     * @param propertyKey
     * @return
     */
    public Integer getIntegerValue(String propertyKey) {
        try {
           String value = StringUtils.trim(prop.getProperty(propertyKey));
           log.info(String.format("Prperty [%s] = [%s]", propertyKey, value));
           return Integer.parseInt(value);         
        } catch (Exception e) {
           log.error("ApplicationPropertiesUtility: Invalid Format to Cast" );
           e.printStackTrace();
           return null;
        }
        
    }
    
    
    /**
     * Get Integer Value of the Property
     * 
     * @param propertyKey
     * @return
     */
    public BigDecimal getBigDecimalVal(String propertyKey) {
        try {
           String value = StringUtils.trim(prop.getProperty(propertyKey));
           log.info(String.format("Prperty [%s] = [%s]", propertyKey,  value));
           return new BigDecimal(value);         
        } catch (Exception e) {
           log.error("ApplicationPropertiesUtility: Invalid Format to Cast" );
           e.printStackTrace();
           return BigDecimal.ZERO;
        }
        
    }
  

}
