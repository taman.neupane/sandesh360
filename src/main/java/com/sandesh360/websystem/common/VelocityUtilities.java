package com.sandesh360.websystem.common;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.ui.velocity.VelocityEngineUtils;

import java.util.Map;

/**
 * Created for saving mail template information
 * @author Mobarak Hosen
 * Created At 2017-11-28
 */
public class VelocityUtilities {
	
	public static final String PASSWORD_RESET_TEMPLATE_EMAIL = "velocityemailtemplates/passwordResetLink.vm";
	public static final String VENDOR_PASSWORD_RESET_TEMPLATE_EMAIL = "velocityemailtemplates/vendorPasswordResetLink.vm";
	public static final String PASSWORD_RESET_SUBJECT_EMAIL = "Foodllee - Password reset link";
	
	/**
	 * Logger
	 */
	protected static Log log = LogFactory.getLog(VelocityUtilities.class);
	
	/**
	 * Get velocity template content as String
	 * 
	 * @param velocityEngine
	 * @param model
	 * @param templateLocation
	 * @return
	 */
	public static String geVelocityTemplateContent(VelocityEngine velocityEngine, Map<String, Object> model, String templateLocation){
        StringBuffer content = new StringBuffer();
        log.info("Creating Mail/SMS Content from the Template");
        try{
            content.append(VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, templateLocation, model));            
            return content.toString();
        }catch(Exception e){
        	log.error("Exception occured while processing velocity template:"+e.getMessage());
        	e.printStackTrace();
        	throw e;            
        }
    }

}
