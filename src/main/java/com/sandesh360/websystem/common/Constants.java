package com.sandesh360.websystem.common;

/**
 * @author Mobarak Hosen
 */
public class Constants {

    /**
     * Google API Key
     */
    public static final String API_KEY = "AIzaSyAeZ762rLNZjSwm90iHv1bWARdpV9IAJnk";
    public static final String IMAGE_FOLDER = "Image";
    public static final String TOKEN = "token";

    public static final String ALL = "all";
    public static final String ACTIVE = "active";

    public static final String PREFERENCE = "/preference/";
    public static final String CATEGORY = "/category/";
    public static final String CONTENT = "/content/";
    public static final String USER = "/user/";
    public static final String PREFERENCE_ID = "preferenceid";
    public static final String CATEGORY_ID = "categoryid";
    public static final String USER_ID = "userid";


    /**
     * Delete Flag [True]
     */
    public static Boolean DELETE_FLAG_YES = Boolean.TRUE;

    /**
     * Active Flag [False]
     */
    public static Boolean ACTIVE_FLAG_NO = Boolean.FALSE;

    /**
     * Active Flag [True]
     */
    public static Boolean ACTIVE_FLAG_YES = Boolean.TRUE;

    /**
     * Delete Flag [False]
     */
    public static Boolean DELETE_FLAG_NO = Boolean.FALSE;


    //////////////////////////// Date Format ///////////////////////////////////////////
    public static String DATE_FORMAT_YYYY_MM_DD = "yyyy/MM/dd";


}
