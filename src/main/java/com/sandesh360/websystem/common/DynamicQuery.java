package com.sandesh360.websystem.common;

import com.sandesh360.websystem.entity.Language;
import org.apache.ibatis.jdbc.SQL;

import java.util.ArrayList;
import java.util.List;

public class DynamicQuery {


    public static String getUpdateSql(Language language) {
        StringBuilder builder = new StringBuilder("UPDATE Language SET");
        if (language.getName() != null && !language.getName().isEmpty()) {
            builder.append(" ").append("name = #{name}");
        }
        if (language.getLocale() != null && !language.getLocale().isEmpty()) {
            builder.append(", ").append("locale = #{locale}");
        }

        builder.append(" ").append("WHERE id = #{id}");
        return builder.toString();


    }
}
