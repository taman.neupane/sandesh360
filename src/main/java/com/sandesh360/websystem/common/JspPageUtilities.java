package com.sandesh360.websystem.common;

public class JspPageUtilities {

    public static final String ADMIN_LOGIN_PAGE = "/login";

    public static final String ADMIN_REGISTRATION_PAGE = "/registration";

    public static final String FORGET_PASSWORD_PAGE = "/forgot-password";

    public static final String DASHBOARD_PAGE = "/dashboard";

    public static final String HOME_PAGE = "/home";

    public static final String ADMIN_PROFILE_PAGE = "/profile";

    public static final String CONTENT_PAGE = "/content";

    public static final String CATEGORY_PAGE = "/category";

    public static final String PREFERENCE_PAGE = "/preference";

    public static final String ERROR_PAGE = "/error";

    public static final String LOGOUT_PAGE = "/logout";
}
