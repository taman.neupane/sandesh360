package com.sandesh360.websystem.common;

public class PropertyConstants {


    /**
     * Property [Change Password Temporary Link Expire interval in Minutes]
     */
    public static final String PROPERTY_CHANGE_PASSWORD_LINK_EXPIRE_INTERVAL = "change_password_link_expire_interval";

    /**
     * Property [Vendor location based search radius in kilometers]
     */
    public static final String PROPERTY_VENDOR_SEARCH_LOCATION_RADIUS = "vendor_search_location_radius";

    /**
     * Property [Foodllee commission percentage on total Order Amount]
     */
    public static final String PROPERTY_FOODLLEE_COMMISSION_PERCENTAGE = "foodllee_commission_percentage_on_total_order_amount";

    /**
     * Property [Foodllee cookie expire interval in minutes]
     */
    public static final String PROPERTY_ADMIN_COOKIE_EXPIRE_INTERVAL = "admin_cookie_expire_interval";

    /**
     * Property [Foodllee Default Order Cancellation time in minutes]
     */
    public static final String ORDER_DEFAULT_CANCEL_TIME_IN_MINUTES = "order_default_cancel_time_in_minutes";
}
