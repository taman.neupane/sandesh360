package com.sandesh360.websystem.common;

public class JsonKeyConstants {
	
	public static final String KEY_ID = "Id";
	public static final String KEY_NAME = "Name";
	
	public static final String KEY_PHONE = "Phone";
	
	public static final String KEY_ADDRESS = "Address";
	
	public static final String KEY_ISACTIVE = "IsActive";
	
	public static final String KEY_EMAIL = "Email";
	
	public static final String KEY_PASSWORD = "Password";
	
	public static final String KEY_OLD_PASSWORD = "OldPassword";
	
	public static final String KEY_NEW_PASSWORD = "NewPassword";
	
	public static final String KEY_CONFIRM_NEW_PASSWORD = "ConfirmNewPassword";
	
	public static final String KEY_TYPE = "Type";
	
	public static final String KEY_STATUS = "Status";
	
	public static final String KEY_STATUS_OK = "Ok";
	
	public static final String KEY_TOKEN = "Token";
	
	public static final String KEY_TERMINAL_INFO = "TerminalId";
	
	public static final String KEY_FCM_TOKEN = "FCMToken";	
    
    public static final String KEY_CREATED_AT = "CreatedAt";


    
}
